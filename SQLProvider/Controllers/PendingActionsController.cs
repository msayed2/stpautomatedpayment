﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;
namespace SQLProvider.Controllers
{
    public class PendingActionsController : Access.PendingActionsController
    {
        private TribalEntities _objContext = null;
        public PendingActionsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override List<PendingAction> Get(Func<PendingAction, bool> expression, out bool? dbStatus)
        {
            List<PendingAction> pendingActionsList = null;
            dbStatus = null;
            try
            {
                pendingActionsList = _objContext.PendingActions.Where(expression).ToList();
                if (pendingActionsList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("60a29cb1-8e03-43be-9631-89999129a729 Error occur during Get all  pending Action in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return pendingActionsList;
        }

        public override PendingAction GetById(string id, out bool? dbStatus)
        {
            PendingAction objPendingAction = null;
            dbStatus = null;
            try
            {
                objPendingAction = _objContext.PendingActions.FirstOrDefault(obj => obj.Id == id);
                if (objPendingAction != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("39ed0176-c467-4847-89f5-b5cffd29a5cc Error occur during Get pending Action in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objPendingAction;
        }
        public override void Create(PendingAction objPendingAction, out bool? dbStatus,bool allowSaveChanges=true)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PendingActions.Add(objPendingAction);
                if (entity != null)
                {
                    if (allowSaveChanges)
                    {
                        _objContext.SaveChanges();
                    }
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8f8da95b-e22a-467e-b324-f64a13b7f93a Error occur during Create Pending Actions in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void SaveChanges()
        {
            try
            {
                _objContext.SaveChanges();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("cad00419-11a3-4780-930f-71d08ca8862b Error occur during Saving PendingAction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

    }
}
