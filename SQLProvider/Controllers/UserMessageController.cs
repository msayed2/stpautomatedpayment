﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class UserMessageController
    {
        private TribalEntities _objContext = null;
        public UserMessageController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public List<UserMessage> GetAll(Func<UserMessage, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<UserMessage> objResult = null;
            try
            {
                objResult = _objContext.UserMessages.ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("deec5e65-9841-49ab-bee0-fffacab7e376 Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }

        public List<UserMessage> GetUserMessages(Func<UserMessage, bool> expression, out bool? dbStatus)
        {
            List<UserMessage> objResult = null;
            dbStatus = null;
            try
            {

                objResult = _objContext.UserMessages.Where(expression).ToList();

                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("0cd74263-e4ea-44a2-bc42-a4efa9c87324 Error occur during Get UserMessages in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public UserMessage Get(Func<UserMessage, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            UserMessage objUserMessage = null;
            try
            {
                objUserMessage = _objContext.UserMessages.FirstOrDefault(expression);
                if (objUserMessage != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4fcd-8249-b752eacb269e Error occur when get company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objUserMessage;
        }
        public void Create(UserMessage objUserMessage, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.UserMessages.Add(objUserMessage);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("41c9605a-042c-4fe4-9525-3b4134f0f327 Error occur during Create Company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void Update(UserMessage objUserMessage, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.UserMessages.FirstOrDefault(I => I.Id == objUserMessage.Id);
                if (objEntity != null)
                {
                    objEntity.Message = (objUserMessage.Message != null) ? objUserMessage.Message : objEntity.Message;
                    objEntity.UpdatedAt = objUserMessage.UpdatedAt;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5f7d9e51-de1f-4da3-8735-8facb032dd01 Error occur when update Card in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
