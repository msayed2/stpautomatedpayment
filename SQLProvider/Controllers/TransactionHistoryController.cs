﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;
namespace SQLProvider.Controllers
{
    public class TransactionHistoryController : Access.TransactionHistoryController
    {
        private TribalEntities _objContext = null;
        public TransactionHistoryController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(TransactionsHistory objTransactionHistory, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
               var entity = _objContext.TransactionsHistories.Add(objTransactionHistory);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"9bf4c069-e99f-496d-b517-864f4985ebf5 Error occur during create transaction history in function {MethodBase.GetCurrentMethod().Name}", ex);

            }
        }

      
    }
}
