﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class DigitalWalletMessageController : Access.DigitalWalletMessageController
    {
        private TribalEntities _objContext = null;
        public DigitalWalletMessageController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(DigitalWalletMessage objDigitalWalletMessage, out bool? dbStatus, bool allowSaving = true)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.DigitalWalletMessages.Add(objDigitalWalletMessage);
                if (entity != null)
                {
                    if (allowSaving)
                    {
                        _objContext.SaveChanges();
                    }
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("006454d2-99a5-4022-9b50-d490798eddf8 Error occur during Create DigitalWalletMessage in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override List<DigitalWalletMessage> GetAll(Func<DigitalWalletMessage, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<DigitalWalletMessage> digitalWalletMessageList = null;
            try
            {
                digitalWalletMessageList = _objContext.DigitalWalletMessages.Where(expression).ToList();
                if (digitalWalletMessageList != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5480b96c-7d99-47d2-950c-28797b0f95be Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return digitalWalletMessageList;
        }

        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a876e445-89e9-44a4-8cc6-c3949e7bdd9c Error occur when saving DigitalWalletMessage in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

        public override void Update(string id, int stateId, out bool? dbStatus)
        {
            dbStatus = null;

            try
            {
                DigitalWalletMessage objDigitalWalletMessage = _objContext.DigitalWalletMessages.FirstOrDefault(I => I.Id == id);
                if (objDigitalWalletMessage != null)
                {
                    objDigitalWalletMessage.StatusId = stateId;
                    objDigitalWalletMessage.UpdateAt = DateTime.UtcNow;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ab4c7357-beb9-49ee-892e-ad0e524f638f Error occur when update DigitalWalletMessage in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
