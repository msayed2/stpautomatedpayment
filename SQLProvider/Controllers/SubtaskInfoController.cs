﻿using Spacegene.Logger;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class SubtaskInfoController
    {
        private TribalPaymentEntities _objContext = null;
        public SubtaskInfoController(TribalPaymentEntities objContext)
        {
            _objContext = objContext;
        }

        public SubTaskInfo Get(Expression<Func<SubTaskInfo, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            SubTaskInfo entity = null;
            try
            {
                entity = _objContext.SubTaskInfoes.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"b8da4222-b55e-4021-a11e-e46fb20b52b5 Error occur during get SubtaskInfo in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<SubTaskInfo> GetAll(Expression<Func<SubTaskInfo, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<SubTaskInfo> objSubtaskInfo = null;
            try
            {
                objSubtaskInfo = _objContext.SubTaskInfoes.Where(expression).ToList();
                if (objSubtaskInfo != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"f75b6a90-8f27-4ce4-ab3c-363e6dca864b Error occur during get all SubTaskInfo in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objSubtaskInfo;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c8dedaaf-bf4f-4db1-84e4-f75ae8c59248 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(SubTaskInfo subTaskInfo, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.SubTaskInfoes.Add(subTaskInfo);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"e4bbb83c-23cc-418f-abcd-af9e3a9c78a5 Error occur during Create SubTaskInfo in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(SubTaskInfo subTaskInfo, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.SubTaskInfoes.FirstOrDefault(I => I.Id == subTaskInfo.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("4f0a23e8-c5d4-45ce-bd5f-d131e957871e Error occur when Update SubTaskInfo Operation {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
