﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;


namespace SQLProvider.Controllers
{
    public class SignAgreemnetController : Access.SignAgreemnetController
    {
        private TribalEntities _objContext = null;
        public SignAgreemnetController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override SignAgreementPendingAction GetById(string id, out bool? dbStatus)
        {
            SignAgreementPendingAction objSignAgreementPendingAction = null;
            dbStatus = null;
            try
            {
                objSignAgreementPendingAction = _objContext.SignAgreementPendingActions.FirstOrDefault(obj => obj.Id == id);
                if (objSignAgreementPendingAction != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("fcbf319b-ec73-4516-a965-bb9de7b034bf Error occur during Get SignAgreementPendingAction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objSignAgreementPendingAction;
        }
        public AgreementUpdatePendingAction GetAgreementupdateAById(string id, out bool? dbStatus)
        {
            AgreementUpdatePendingAction objSignAgreementPendingAction = null;
            dbStatus = null;
            try
            {
                objSignAgreementPendingAction = _objContext.AgreementUpdatePendingActions.FirstOrDefault(obj => obj.Id == id);
                if (objSignAgreementPendingAction != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("fcbf319b-ec73-4516-a965-bb9de7b034bf Error occur during Get SignAgreementPendingAction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objSignAgreementPendingAction;
        }
        public override List<SignAgreementPendingAction> Get(Func<SignAgreementPendingAction, bool> expression, out bool? dbStatus)
        {
            List<SignAgreementPendingAction> signAgreementPendingActionList = null;
            dbStatus = null;
            try
            {
                signAgreementPendingActionList = _objContext.SignAgreementPendingActions.Where(expression).ToList();
                if (signAgreementPendingActionList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("f3c6f23b-2da9-4ca8-821f-76afd5d8d3ab Error occur during Get all SignAgreementPendingAction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return signAgreementPendingActionList;
        }
        public PendingAction GetPendingAction(Func<PendingAction, bool> expression, out bool? dbStatus)
        {
            PendingAction objSignAgreementPendingAction = null;
            dbStatus = null;
            try
            {
                objSignAgreementPendingAction = _objContext.PendingActions.FirstOrDefault(expression);
                if (objSignAgreementPendingAction != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a156f2bc-67ee-4875-872d-a7b7d554ef44 Error occur during Get PendingAction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objSignAgreementPendingAction;
        }
        public  List<ActionType> GetAllActionType(Func<ActionType, bool> expression, out bool? dbStatus)
        {
            List<ActionType> objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.ActionTypes.Where(expression).ToList();
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("35821b53-9b71-43fa-8db3-2853360ce9e8 Error occur during Get All Action Type in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;

        }
        public ActionType GetActionType(Func<ActionType, bool> expression, out bool? dbStatus)
        {
            ActionType objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.ActionTypes.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("b20832bf-f93f-4af6-ab5f-9a81bbdec3f2 Error occur during Get All Action Type in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;

        }
        public Document GetDocumnet(Func<Document, bool> expression, out bool? dbStatus)
        {
            Document objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.Documents.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5e084fb5-7a22-432a-9e09-009505bbfc77 Error occur during Get All Action Type in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;

        }

        public override List<Document> GetAllDocument(Func<Document, bool> expression, out bool? dbStatus)
        {
            List<Document> objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.Documents.Where(expression).ToList();
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d70e1178-f2e6-4574-a7bd-f31035ee9552 Error occur during Get All Document in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public void CreatePendingAction(PendingAction objPendingAction, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PendingActions.Add(objPendingAction);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("f1d69914-1333-4c06-8bb7-61a39b48b121 Error occur during Create Pending Actions in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override void Create(SignAgreementPendingAction objSignAgreementPendingAction, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.SignAgreementPendingActions.Add(objSignAgreementPendingAction);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                 new Logger().LogError(string.Format("e6717e00-d2c2-4fbf-a404-6264a2963f10 Error occur when Create SignAgreementPendingAction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public  void CreateAgreementupdate(AgreementUpdatePendingAction objAgreementupdateAction, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.AgreementUpdatePendingActions.Add(objAgreementupdateAction);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e6717e00-d2c2-4fbf-a404-6264a2963f10 Error occur when Create SignAgreementPendingAction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void SaveChanges()
        {
            try
            {
                _objContext.SaveChanges();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("f71ab1a2-4a73-490a-8b05-7c0c32e57e4e Error occur during Saving SignAgreemnet in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
          
        }
    }
}
