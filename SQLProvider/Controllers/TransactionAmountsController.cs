﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class TransactionAmountsController :Access.TransactionAmountsController
    {
        private TribalEntities _objContext = null;
        public TransactionAmountsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override List<TransactionAmount> GetALLAsNoTracking(Expression<Func<TransactionAmount, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<TransactionAmount> objResult = null;
            try
            {
                objResult = _objContext.TransactionAmounts
                                .AsNoTracking()
                                .Where(expression)
                               // .Select(I=>new TransactionAmount { BaseAmount = I.BaseAmount, CardAmount =I.CardAmount,UserId =I.UserId,CardId=I.CardId})
                                .ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"792d4b11-cd46-476c-8424-be799515c25b Exception when getting Transaction Amounts from database in", ex);
            }

            return objResult;
        }
        public  List<TransactionAmount> GetALL(Expression<Func<TransactionAmount, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<TransactionAmount> objResult = null;
            try
            {
                objResult = _objContext.TransactionAmounts
                                .Where(expression)
                                .ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"792d4b11-cd46-476c-8424-be799515c25b Exception when getting Transaction Amounts from database in", ex);
            }

            return objResult;
        }
        public void Create(TransactionAmount objTransactionAmount, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.TransactionAmounts.Add(objTransactionAmount);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"2e2d474e-c9b1-40c6-ba62-ef4aa96a96ba Error occur during Create transaction amount in function CreateTransactionAmount", ex);
            }
        }

        //public override TransactionAmount Get(Expression<Func<TransactionAmount, bool>> expression, out bool? dbStatus)
        //{
        //    dbStatus = null;
        //    TransactionAmount objTransactionAmount = null;
        //    try
        //    {
        //        objTransactionAmount = _objContext.TransactionAmounts.FirstOrDefault(expression);
        //        dbStatus = objTransactionAmount != null ? true : false;
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogError($"792d4b11-cd46-4sss76c-8424-be799515c25b Exception when getting Transaction Amount from database in", ex);
        //    }

        //    return objTransactionAmount;
        //}
        public override TransactionAmount Get(Func<TransactionAmount, bool>expression, out bool? dbStatus)
        {
            dbStatus = null;
            TransactionAmount objTransaction = null;
            try
            {
                objTransaction = _objContext.TransactionAmounts.FirstOrDefault(expression);
                if (objTransaction != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"792d4b11-cd46-4sss76c-8424-be799515c25b Exception when getting Transaction Amount from database in", ex);
            }
            return objTransaction;
        }
        public void SaveChanges(out bool? dbStatus)
        {
             dbStatus = null;
            try
            {

                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ssssxxxzz-ba62-ef4aa96a96ba Error occur during Save Changes amount in function SaveChanges", ex);

            }
        }
    }
}
