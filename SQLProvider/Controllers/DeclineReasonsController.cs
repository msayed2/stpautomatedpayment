﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
   public class DeclineReasonsController : Access.DeclineReasonsController
    {
        private TribalEntities _objContext = null;

        public DeclineReasonsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(DeclineReason objDeclineReason, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.DeclineReasons.Add(objDeclineReason);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"a8d462ab-c585-48fb-b477-0e9b44191556 Error occur during create Decline Reasons in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override DeclineReason Get(Expression<Func<DeclineReason, bool>> expression, out bool? dbStatus)
        {
            DeclineReason objDeclineReason = null;
            dbStatus = null;
            try
            {
                objDeclineReason = _objContext.DeclineReasons.FirstOrDefault(expression);
                if (objDeclineReason != null)
                {
                    dbStatus = true;
                } else
                {
                    dbStatus = false;
                 }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"81571637-40cf-410c-bfac-b8355bf2a8b2 Error occur during get Decline Reason in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objDeclineReason;
        }

        public override List<DeclineReason> GetALL(Expression<Func<DeclineReason, bool>> expression, out bool? dbStatus)
        {
            List<DeclineReason> declineReasonList = null;
            dbStatus = null;
            try
            {
                declineReasonList = _objContext.DeclineReasons.Where(expression).ToList();
                if (declineReasonList != null)
                {
                    dbStatus = declineReasonList.Count > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"2da5d995-88ab-4f09-80eb-bdf51326a062 Error occur during get all Decline Reason in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return declineReasonList;
        }
    }
}
