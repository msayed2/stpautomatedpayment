﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class CountriesController
    {
        private TribalEntities _objContext = null;
        public CountriesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public Country Get(Expression<Func<Country, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Country entity = null;
            try
            {
                entity = _objContext.Countries.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"8cd4d31c-0030-4921-b6a7-d624936d6975 Error occur during get Country in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<Country> GetAll(Expression<Func<Country, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Country> countries = null;
            try
            {
                countries = _objContext.Countries.Where(expression).ToList();
                if (countries != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"2b81366a-7170-42f7-bc10-e4f4b4229f08 Error occur during get all Countries in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return countries;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"9ae2f766-b1e4-402d-8661-771d04744f67 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(Country countries, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Countries.Add(countries);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3990bc61-37ad-4e8c-8317-ae66f59ba668 Error occur during Create Country in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(Country country, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Countries.FirstOrDefault(I => I.Id == country.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("b75b6f0e-c196-4796-a15b-d79d5bdee95c Error occur when Updating Country {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
