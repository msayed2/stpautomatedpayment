﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyCashBillingController :Access.CompanyCashBillingController
    {
        private TribalEntities _objContext = null;
        public CompanyCashBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Update(CompanyCashBilling objCompanyBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CompanyCashBillings.FirstOrDefault(I => I.Id == objCompanyBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = objCompanyBilling.Avaliable;
                    objEntity.Spent = objCompanyBilling.Spent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"1f6a8143-975c-485e-b828-5168a3a224b9 Error occur when update company Cash billing in function ", ex);
            }
        }
    }
}
