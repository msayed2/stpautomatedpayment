﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class PaymentController
    {
        private TribalEntities _objContext = null;
        public PaymentController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public Payment Get(Expression<Func<Payment, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Payment entity = null;
            try
            {
                entity = _objContext.Payments.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                  new Logger().LogError($"58d10bfb-05d4-4078-9486-52c93a71f689 Error occur during get payment in function Get", ex);
            }
            return entity;
        }

        public List<Payment> GetAll(Expression<Func<Payment, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Payment> payments = null;
            try
            {
                payments = _objContext.Payments.Where(expression).ToList();
                if (payments != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"06a3efb7-20ac-460a-8280-5323cc695f98 Error occur during get all payments in function GetAll", ex);
            }
            return payments;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                 new Logger().LogError($"45937721-5fc1-4c64-a9cf-5520c7ef0896 Error occur during save changes in function SaveChanges", ex);
            }
        }

        public void Create(Payment payment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Payments.Add(payment);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"35574ff9-20c2-4ead-a03f-1847dbcfa7f3 Error occur during Create payment in function Create", ex);
            }
        }

        public void Update(Payment payment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Payments.FirstOrDefault(I => I.Id == payment.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("b35c1aed-b398-422b-a947-556bc8bd7b12 Error occur when Updating payment in funciont Update"), ex);
            }
        }

        public void CreatePaymentHistory(PaymentTransactionsHistory payment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PaymentTransactionsHistories.Add(payment);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"af541696-941c-4d4e-b77c-e6f5c5358bf3 Exception occur during Create Payment History in function CreatePaymentHistory", ex);
            }
        }
    }
}
