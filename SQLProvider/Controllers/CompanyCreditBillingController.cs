﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyCreditBillingController :Access.CompanyCreditBillingController
    {
        private TribalEntities _objContext = null;
        public CompanyCreditBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Update(CompanyCreditBilling objCompanyBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CompanyCreditBillings.FirstOrDefault(I => I.Id == objCompanyBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = objCompanyBilling.Avaliable;
                    objEntity.Spent = objCompanyBilling.Spent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"c3006d8d-be2a-4d17-930d-bea1c5c0ff5e Error occur when update company credit billing in function ", ex);
            }
        }
    }
}
