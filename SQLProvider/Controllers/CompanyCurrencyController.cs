﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyCurrencyController :Access.CompanyCurrencyController
    {
        private TribalEntities _objContext = null;
        public CompanyCurrencyController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override CompanyCurrency Get(Func<CompanyCurrency, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyCurrency objCurrency = null;
            try
            {
                objCurrency = _objContext.CompanyCurrencies.FirstOrDefault(expression);
                if (objCurrency != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ea814148-0530-4c28-8c31-6cc0a9e468c5 Error occur when get Company Currency  in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objCurrency;
        }

        public override void Create(CompanyCurrency objCurrency, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyCurrencies.Add(objCurrency);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"64151369-90ba-4e40-b03e-15695cbfa8a3 Error occur during Create currency in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }


    }
}
