﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CurrencyController: Access.CurrencyController
    {

        private TribalEntities _objContext = null;
        public CurrencyController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override Currency Get(Expression<Func<Currency, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Currency entity = null;
            try
            {
                entity = _objContext.Currencies.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"33ee3779-0428-4a5c-94e0-fc48a9ecc8b7 Error occur during get Currency in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public override List<Currency> GetAll(Expression<Func<Currency, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Currency> currencies = null;
            try
            {
                currencies = _objContext.Currencies.Where(expression).ToList();
                if (currencies != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"986ea9c3-f168-4f5f-89be-651825e15469 Error occur during get all currencies in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return currencies;
        }

        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"f7d4824e-1251-4c25-9333-dc2da31a2551 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override void Create(Currency objCurrency, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Currencies.Add(objCurrency);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"64151369-90ba-4e40-b03e-15695cbfa8a3 Error occur during Create currency in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }
    }
}
