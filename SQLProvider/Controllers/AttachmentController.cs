﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class AttachmentController : Access.AttachmentController
    {
        private TribalEntities _objContext = null;

        public AttachmentController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void CreateBulk(List<Attachment> model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.Attachments.AddRange(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a01c7518-11c5-43e8-b414-00bac06921af Error occur during Add Attachement  Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

        public override void Create(Attachment model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.Attachments.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6aa29e68-dae4-4333-8c71-77415a90271a Error occur during Add Attachement  Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

        public override List<AttachmentType> GetAttachmentTypes(Func<AttachmentType, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<AttachmentType> objAttachmentType = null;
            try
            {
                objAttachmentType = _objContext.AttachmentTypes.Where(expression).ToList();
                if (objAttachmentType != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ssd346e26b-5071-4fcd-8249-b752eacb269e Error occur when get Attachement types in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objAttachmentType;
        }

        public override Attachment Get(Func<Attachment, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Attachment objAttachment = null;
            try
            {
                objAttachment = _objContext.Attachments.FirstOrDefault(expression);
                if (objAttachment != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ececa700-8e5e-416e-9ca4-1c713b5e5c09 Error occur when get Attachement in function {MethodBase.GetCurrentMethod().Name} ", ex);
            }
            return objAttachment;
        }

        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"14223be8-58ff-41f0-9215-b23393857d65 Error occur when save changes in function {MethodBase.GetCurrentMethod().Name} ", ex);
            }
        }
    }
}
