﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class MerchantController : Access.MerchantController
    {
        private TribalEntities _objContext = null;
        public MerchantController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override Merchant Get(Func<Merchant, bool> expression, out bool? dbStatus)
        {
            Merchant objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.Merchants.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e0006e0b-a5cc-4c01-b76b-94ddfc9fb469 Error occur during Get Merchant in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
    }
}
