﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
   public class InvoiceDetailTransactionsController : Access.InvoiceDetailTransactionsController
    {
        private TribalEntities _objContext = null;
        public InvoiceDetailTransactionsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(TransactionsInvoiceDetail objTransactionsInvoiceDetails, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.TransactionsInvoiceDetails.Add(objTransactionsInvoiceDetails);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9d7632e5-c9de-4f4b-94e5-99bae02e838f Error occur during TransactionsInvoiceDetail in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override void CreateBulk(List<TransactionsInvoiceDetail> transactionsInvoiceDetaislList, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.TransactionsInvoiceDetails.AddRange(transactionsInvoiceDetaislList);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("90c70d34-b58b-461f-b010-0507c8fb2906 Error occur during Create bulk TransactionsInvoiceDetail in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void CreateBulkPaymentInvoiceDetail(List<PaymentInvoiceDetail> paymentInvoiceDetailList, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PaymentInvoiceDetails.AddRange(paymentInvoiceDetailList);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("90c70d34-54sab-461f-b010-0507c8fsds06 Error occur during Create bulk paymentInvoiceDetail in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public List<TransactionsInvoiceDetail> GetAll(Func<TransactionsInvoiceDetail, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<TransactionsInvoiceDetail> entity = new List<TransactionsInvoiceDetail>();
            try
            {
                entity = _objContext.TransactionsInvoiceDetails.Where(expression).ToList();
                dbStatus = true;

            }
            catch (Exception ex)
            {
                new Logger().LogError($"e03415eb-0df5-4e40-af06ss-25ee8ef250d0 Error occur during get all MoneyIN Data in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }
    }
}
