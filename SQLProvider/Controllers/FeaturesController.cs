﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class FeaturesController
    {
        private TribalEntities _objContext = null;
        public FeaturesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public Feature Get(Expression<Func<Feature, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Feature entity = null;
            try
            {
                entity = _objContext.Features.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"92c111c8-b8da-4b09-91b0-e56ba89ee926 Error occur during get feature in function Get", ex);
            }
            return entity;
        }

        public List<Feature> GetAll(Expression<Func<Feature, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Feature> features = null;
            try
            {
                features = _objContext.Features.Where(expression).ToList();
                if (features != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"7bb291b6-9532-4f5d-a23c-104353722b4d Error occur during get all features in function GetAll", ex);
            }
            return features;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"a5e02079-2ad9-461d-a103-6fd7aaaff310 Error occur during save changes in function SaveChanges", ex);
            }
        }

        public void Create(Feature feature, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Features.Add(feature);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"32555479-dad7-4741-a12d-7cfb2bc2ffa1 Error occur during Creating feature in function Create", ex);
            }
        }

        public void Update(Feature feature, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Features.FirstOrDefault(I => I.Id == feature.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("209d27da-a367-4f8f-a592-8ec222d4da40 Error occur when Updating feature in function Update"), ex);
            }
        }
    }
}
