﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class MoneyINController
    {
        private TribalEntities _objContext = null;

        public MoneyINController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public void Create(MoneyIN model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.MoneyINs.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9a8543ac-ca9c-495a-b0f9-010290e5baca Error occur during Add Money Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public void CreateMoneyDetailRecord(MoneyInDetali model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.MoneyInDetalis.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("369249c7-63c3-4f70-9478-31c4566eaab3 Error occur during Add Money Detail Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public MoneyIN Get(Expression<Func<MoneyIN, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            MoneyIN entity = null;
            try
            {
                entity = _objContext.MoneyINs.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"9cb4bf4d-0223-4a8e-a60f-3f66d0048d54 Error occur during get MoneyIN in function Get", ex);
            }
            return entity;
        }
        public List<MoneyIN> GetAll(Func<MoneyIN, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<MoneyIN> entity = new List<MoneyIN>();
            try
            {
                entity = _objContext.MoneyINs.Where(expression).ToList();
                dbStatus = true;

            }
            catch (Exception ex)
            {
                new Logger().LogError($"e03415eb-0df5-4e40-af06ss-25ee8ef250d0 Error occur during get all MoneyIN Data in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<MoneyInDetali> GetAllMoneyInDetail(Func<MoneyInDetali, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<MoneyInDetali> entity = new List<MoneyInDetali>();
            try
            {
                entity = _objContext.MoneyInDetalis.Where(expression).ToList();
                dbStatus = true;

            }
            catch (Exception ex)
            {
                new Logger().LogError($"e03415eb-0df5-4e40-af06ss-25ee8ef250d0 Error occur during get all MoneyIN Data in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

    }
}
