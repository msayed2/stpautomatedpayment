﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class InvoiceCurrencyController : Access.InvoiceCurrencyController
    {
        private TribalEntities _objContext = null;
        public InvoiceCurrencyController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(InvoiceCurrency objInvoiceCurrency, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.InvoiceCurrencies.Add(objInvoiceCurrency);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("df3e373a-8ed1-4aa0-a770-0d8091ddd14c Error occur during Create Invoice Currency in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override InvoiceCurrency Get(Expression<Func<InvoiceCurrency, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            InvoiceCurrency objInvoiceCurrency = null;
            try
            {
                objInvoiceCurrency = _objContext.InvoiceCurrencies.FirstOrDefault(expression);
                if (objInvoiceCurrency != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("df3e373a-8ed1-4aa0-a770-0d8091ddd14c Error occur during Get Invoice Currency in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objInvoiceCurrency;
        }
        public List<InvoiceCurrency> GetAll(Func<InvoiceCurrency, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<InvoiceCurrency> entity = new List<InvoiceCurrency>();
            try
            {
                entity = _objContext.InvoiceCurrencies.Where(expression).ToList();


            }
            catch (Exception ex)
            {
                new Logger().LogError($"c73ee2f1-56c2-45d8-8d01-bb063f10294b Error occur during get all Invoices Data in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("df3e373a-8ed1-4aa0-a770-0d8091ddd14c Error occur during save changes in InvoiceCurrenciesController in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
