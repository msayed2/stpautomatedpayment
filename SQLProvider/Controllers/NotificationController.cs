﻿using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Access = SQLAccess.Controllers;
using Entity.Entities.TribalEntities;
using System.Reflection;
using Spacegene.Logger;

namespace SQLProvider.Controllers
{
    public class NotificationController : Access.NotificationController
    {
        private TribalEntities _objContext = null;
        public NotificationController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override List<Notification> GetAll(string user_id, out bool? dbStatus)
        {
            dbStatus = null;
            List<Notification> objNotifications = null;
            try
            {
                //objNotifications = (
                //                   from C in _objContext.CommunicationChannels
                //                   join N in _objContext.Notifications
                //                   //on C.Id equals N.ChannelId
                //                   where C.UserId == user_id
                //                   select N
                //                   ).ToList();
                if (objNotifications != null && objNotifications.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("12543fb2-6933-4e18-86a1-b093ee9641b3 Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objNotifications;
        }

        public override void MarkAsRead(string notification_id, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Notifications.FirstOrDefault(I => I.Id == notification_id);
                if (objEntity != null)
                {
                    //objEntity.IsSeen = true;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("7fbdd573-0db7-42bd-80b9-f7750edc55ff Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override void UpdateAction(string notification_id, int action_id, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Notifications.FirstOrDefault(I => I.Id == notification_id);
                if (objEntity != null)
                {
                    //objEntity.ActionId = action_id;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("63f80bee-d2f8-417a-8fc8-0983b2a7856a Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public void CreateNotification(Notification objNotification, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Notifications.Add(objNotification);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("15cc0c2f-782d-4f2d-8f7c-1f0406b77863 Error occur during Create notification in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void CreateNotificationBulk(List<Notification> objNotificationList,out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Notifications.AddRange(objNotificationList);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a35d5a3a-d83f-4f9b-9be2-fb34467924f5 Error occur during Create notification Bulk in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void CreateNotificationBadge(NotificationBadge objNotificationBadge, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.NotificationBadges.Add(objNotificationBadge);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("3dfb672f-1a4f-4da0-958c-6c2c0ee16bfd Error occur during Create NotificationBadge in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public NotificationBadge GetNotificationBadge(Func<NotificationBadge, bool> expression, out bool? dbStatus)
        {
            NotificationBadge objNotificationBadge = null;
            dbStatus = null;
            try
            {
                objNotificationBadge = _objContext.NotificationBadges.FirstOrDefault(expression);
                if (objNotificationBadge != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9875e39b-f902-4f32-8ab7-318f230b5f34 Error occur during get NotificationBadge in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objNotificationBadge;
        }
        public List<Notification> GetAllNotification(Func<Notification, bool> expression, out bool? dbStatus)
        {
            List<Notification> result = null;
            dbStatus = null;
            try
            {
                if (expression != null)
                {
                    result = _objContext.Notifications.Where(expression).ToList();
                }
                else
                {
                    result = new List<Notification>();
                }
                if (result != null)
                {
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("faf38cac-76f0-43ec-a8d0-9a201fb41d23 Error occur when Get all Notification in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public Notification GetNotification(Func<Notification, bool> expression, out bool? dbStatus)
        {
            Notification result = null;
            dbStatus = null;
            try
            {
                result = _objContext.Notifications.Where(expression).FirstOrDefault();

                if (result != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("4cb5d653-b024-467f-acff-749547426698 Error occur when Get Notification in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public UserSetting GetUserSetting(Func<UserSetting, bool> expression, out bool? dbStatus)
        {
            UserSetting result = null;
            dbStatus = null;
            try
            {

                result = _objContext.UserSettings.Where(expression).FirstOrDefault();

                if (result != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ef63a2da-697e-4c89-9148-a0a4a5fb6b85 Error occur when Get UserSetting in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public List<UserSetting> GetAllUserSetting(Func<UserSetting, bool> expression, out bool? dbStatus)
        {
            List<UserSetting> result = null;
            dbStatus = null;
            try
            {
                if (expression != null)
                {
                    result = _objContext.UserSettings.Where(expression).OrderBy(I=>I.NotificationCategoryId).ToList();
                }
                else
                {
                    result = _objContext.UserSettings.ToList();
                }
                if (result != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("1f3a6dab-8161-478b-802e-be30f3553349 Error occur when Get all UserSetting in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public override List<NotificationCategory> GetAllNotificationType(Func<NotificationCategory, bool> expression, out bool? dbStatus)
        {
            List<NotificationCategory> result = null;
            dbStatus = null;
            try
            {
                result = _objContext.NotificationCategories.Where(expression)?.OrderBy(I => I.Id).ToList();
                if (result != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3e4bf7c7-a617-4d92-8485-5ea09a32af9c Error occur when Get all Notification Type in function {MethodBase.GetCurrentMethod().Name} ", ex);
            }
            return result;
        }
        public override void CreateBulkSetting(List<UserSetting> userSettings, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var userDbSettings = _objContext.UserSettings.AddRange(userSettings);
                if (userDbSettings != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"e255d69c-378c-4838-836e-1635da898858 Error occur when Get create bulk user settings in function {MethodBase.GetCurrentMethod().Name} ", ex);
            }
        }

        public void SaveChanges()
        {
            try
            {
                _objContext.SaveChanges();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e45d4e02-b49a-4c15-b99e-1c4823835c7a Error occur during Saving cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

       
    }
}
