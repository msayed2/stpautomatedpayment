﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class InvoiceCurrenciesSettlementController : Access.InvoiceCurrenciesSettlementController
    {
        private TribalEntities _objContext = null;
        public InvoiceCurrenciesSettlementController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(InvoiceCurrenciesSettlement objInvoiceCurrenciesSettlement, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.InvoiceCurrenciesSettlements.Add(objInvoiceCurrenciesSettlement);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ac0d8356-b06d-4b25-96ac-18b4a6fa9a40 Error occur during create invoice currencies settlement  in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public InvoiceCurrenciesSettlement Get(Func<InvoiceCurrenciesSettlement, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            InvoiceCurrenciesSettlement objInvoiceCurrenciesSettlement = null;
            try
            {
                objInvoiceCurrenciesSettlement = _objContext.InvoiceCurrenciesSettlements.FirstOrDefault(expression);
                if (objInvoiceCurrenciesSettlement != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d4016bbb-6c9a-4c75-bf44-0sdsdsd0ad724d8ba Error occur during get Invoice Currencies Settlement in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objInvoiceCurrenciesSettlement;
        }
    }
}
