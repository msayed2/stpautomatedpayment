﻿using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Access = SQLAccess.Controllers;
using Entity.Entities.TribalEntities;
using Spacegene.Logger;
using System.Reflection;
using Newtonsoft.Json;

namespace SQLProvider.Controllers
{
    public class CompanySuspensionController : Access.CompanySuspensionController
    {
        private TribalEntities _objContext = null;
        public CompanySuspensionController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(CompanySuspension objCompanySuspension, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanySuspensions.Add(objCompanySuspension);
                if (entity != null)
                {
                    entity.CreatedAt = DateTime.UtcNow;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("322cd77f-4697-47fd-a91f-8cfd95fde555 Error occur during Create CompanySuspension in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override CompanySuspension Get(Func<CompanySuspension, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanySuspension objCompanySuspension = null;
            try
            {
                objCompanySuspension = _objContext.CompanySuspensions.FirstOrDefault(expression);
                if (objCompanySuspension != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"87eb8952-cece-4430-a737-abd8c5732f55 Error occur when get CompanySuspension in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objCompanySuspension;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("664a893b-e519-46ba-918a-1919cbbd83e3 Error occur during SaveChanges in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

    }
}
