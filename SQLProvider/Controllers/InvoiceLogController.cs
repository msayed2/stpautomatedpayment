﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;
namespace SQLProvider.Controllers
{
    public class InvoiceLogController:Access.InvoiceLogController
    {
        private TribalEntities _objContext = null;
        public InvoiceLogController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(InvoiceLog objInvoice, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.InvoiceLogs.Add(objInvoice);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9cb96c31-729d-4ff6-81db-321555c4b48c Error occur during create Invoice Log in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
