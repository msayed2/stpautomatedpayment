﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyAliasController : Access.CompanyAliasController
    {
        private TribalEntities _objContext = null;

        public CompanyAliasController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(CompanyAlias model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CompanyAliases.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("495bb4ff-8792-4ab1-b588-6a949955dfd1 Error occur during Add Company Alias Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

        public override CompanyAlias Get(Func<CompanyAlias, bool> expression, out bool? dbStatus)
        {
            CompanyAlias objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.CompanyAliases.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("0735c38a-5849-4688-aa2e-df193bead35f Error occur during Get Company Alias in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public List<CompanyAlias> GetAll(Func<CompanyAlias, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<CompanyAlias> CompanyAliasList = null;
            try
            {
                CompanyAliasList = _objContext.CompanyAliases.Where(expression).ToList();
                if (CompanyAliasList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"fd8145846a-7170-42f7-bc10-e4f4dsfs8 Error occur during get all CompanyAlias in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return CompanyAliasList;
        }

    }
}
