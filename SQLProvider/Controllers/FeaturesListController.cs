﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class FeaturesListController
    {
        private TribalEntities _objContext = null;
        public FeaturesListController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public FeaturesList Get(Expression<Func<FeaturesList, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            FeaturesList entity = null;
            try
            {
                entity = _objContext.FeaturesLists.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3637ec83-4ae3-4f91-81d2-fc36a3c891c0 Error occur during get feature list in function Get", ex);
            }
            return entity;
        }

        public List<FeaturesList> GetAll(Expression<Func<FeaturesList, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<FeaturesList> featuresList = null;
            try
            {
                featuresList = _objContext.FeaturesLists.Where(expression).ToList();
                if (featuresList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ca304c58-00e5-4213-b7a1-bc7c91e1e2cf Error occur during get all features list in function GetAll", ex);
            }
            return featuresList;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"57efa099-a78c-4e4a-b6d9-6b7684744bfa Error occur during save changes in function SaveChanges", ex);
            }
        }

        public void Create(FeaturesList featuresList, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.FeaturesLists.Add(featuresList);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"5419e8c7-7e99-45ca-a151-b72c02282dad Error occur during Create feature in features list in function Create", ex);
            }
        }

        public void Update(FeaturesList featuresList, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.FeaturesLists.FirstOrDefault(I => I.Id == featuresList.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("780ef2c1-1c6a-4788-9155-cda700fc7136 Error occur when Updating feature list in funciont Update"), ex);
            }
        }
    }
}
