﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class CurrencyCloudPaymentsController
    {
        private TribalEntities _objContext = null;
        public CurrencyCloudPaymentsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public CurrencyCloudPayment Get(Expression<Func<CurrencyCloudPayment, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CurrencyCloudPayment entity = null;
            try
            {
                entity = _objContext.CurrencyCloudPayments.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3237b838-7d3e-4c65-9096-4dffd0f17b3a Error occur during get currency cloud payment in function Get", ex);
            }
            return entity;
        }

        public List<CurrencyCloudPayment> GetAll(Expression<Func<CurrencyCloudPayment, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<CurrencyCloudPayment> payments = null;
            try
            {
                payments = _objContext.CurrencyCloudPayments.Where(expression).ToList();
                if (payments != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ca49bb8b-7968-4858-afe2-f9595369c1d8 Error occur during get all currency cloud payments in function GetAll", ex);
            }
            return payments;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"a9e5b6fd-3b9c-40c4-bc75-b7295a490ec2 Error occur during save changes in function SaveChanges", ex);
            }
        }

        public void Create(CurrencyCloudPayment currencyCloudPayment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CurrencyCloudPayments.Add(currencyCloudPayment);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"21f6e872-a5af-4ab3-aa63-dcc90d1c1282 Error occur during Create currency cloud payment in function Create", ex);
            }
        }

        public void Update(CurrencyCloudPayment currencyCloudPayment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CurrencyCloudPayments.FirstOrDefault(I => I.Id == currencyCloudPayment.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8b849941-dba4-4098-8306-104eed2b1515 Error occur when Updating currency cloud payment in funciont Update"), ex);
            }
        }
    }
}
