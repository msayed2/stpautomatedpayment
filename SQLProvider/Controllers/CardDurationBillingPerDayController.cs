﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;


namespace SQLProvider.Controllers
{
    public class CardDurationBillingPerDayController :Access.CardDurationBillingPerDayController
    {
        private TribalEntities _objContext = null;
        public CardDurationBillingPerDayController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Update(CardDurationBillingPerDay objCardDurationBillingPerDay, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CardDurationBillingPerDays.FirstOrDefault(I => I.Id == objCardDurationBillingPerDay.Id);
                if (objEntity != null)
                {
                    objEntity.IsActive = objCardDurationBillingPerDay.IsActive;
                    objEntity.Avaliable = objCardDurationBillingPerDay.Avaliable;
                    objEntity.Spent = objCardDurationBillingPerDay.Spent;
                    objEntity.HQAvaliable = objCardDurationBillingPerDay.HQAvaliable;
                    objEntity.HQSpent = objCardDurationBillingPerDay.HQSpent;
                    objEntity.Updated = objCardDurationBillingPerDay.Updated;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"a082c061-4205-413b-bfb0-6b5db2ead269 Error occur when update Card Duration Billing Per Day in function ", ex);
            }
        }


        public override void Create(CardDurationBillingPerDay objCardDurationBillingPerDay, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CardDurationBillingPerDays.Add(objCardDurationBillingPerDay);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"efa8b3af-2222-44f5-a00e-983ed6404300 Error occur during create card billing in function", ex);
            }
        }


    }
}
