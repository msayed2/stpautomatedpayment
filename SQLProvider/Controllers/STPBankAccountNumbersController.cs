﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class STPBankAccountNumbersController
    {
        private TribalEntities _objContext = null;
        public STPBankAccountNumbersController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public  STPBankAccountNumber Get(Func<STPBankAccountNumber, bool> expression, out bool? dbStatus)
        {
            STPBankAccountNumber objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.STPBankAccountNumbers.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9bf4c069-e99f-496d-b517-864f4985ebf5 Error occur during Get All Action Type in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public  void CreateCompanySTPNumbers(CompanySTPAccountNumber model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CompanySTPAccountNumbers.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6aa29e68-dae4-4333-8c71-77415a90271a Error occur during Add Attachement  Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"14223be8-58ff-41f0-9215-b23393857d65 Error occur when save changes in function {MethodBase.GetCurrentMethod().Name} ", ex);
            }
        }
    }
}
