﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;


namespace SQLProvider.Controllers
{
    public class ProfileController : Access.ProfileController
    {
        private TribalEntities _objContext = null;
        public ProfileController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(Profile objProfile, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Profiles.Add(objProfile);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8f6cb4ee-dd9d-4c53-8146-d0e9a78e126f Error occur during Create Profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override void Update(Func<Profile, bool> expression, Profile objProfile, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Profiles.FirstOrDefault(expression);
                if (entity != null)
                {
                    entity.FirstName = objProfile.FirstName;
                    entity.LastName = objProfile.LastName;
                    entity.AspNetUser.Email = objProfile.AspNetUser.Email;
                    entity.AspNetUser.PhoneNumber = objProfile.AspNetUser.PhoneNumber;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8924c177-299c-4af1-9e5d-77551683f132 Error occur during Update Profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public override DeviceLogin GetVerification(Func<DeviceLogin, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            DeviceLogin objVerificationModel = null;
            try
            {
                objVerificationModel = _objContext.DeviceLogins.FirstOrDefault(expression);
                if (objVerificationModel != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("7b6a9d26-09d3-45a8-b51f-7cb863491b87 Error occur when get Verification in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objVerificationModel;
        }
        public override void Update(DeviceLogin objDeviceLogin, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                //var entity = _objContext.Profiles.FirstOrDefault(expression);
                var entity = _objContext.DeviceLogins.Add(objDeviceLogin);
                if (entity != null)
                {
                    //entity.Code = smsCode;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8e0be13f-3791-4d74-88e2-d051da2ef0ad Error occur during Create Profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override Profile Get(Func<Profile, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Profile objProfile = null;
            try
            {
                objProfile = _objContext.Profiles.FirstOrDefault(expression);
                if (objProfile != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("931cdefc-6f09-4b21-acd9-3c653bb66de9 Error occur when get Profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objProfile;
        }
    
        public void UpdateToSuperAdmin(Func<Profile, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Profile objProfile = null;
            try
            {
                objProfile = _objContext.Profiles.FirstOrDefault(expression);
                if (objProfile != null)
                {
                    objProfile.InitialTypeId = objProfile.CurrentTypeId;
                    objProfile.CurrentTypeId = 0;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }

            }
            catch (Exception ex)
            {

                new Logger().LogError(string.Format("ec2f4731-192b-41e3-978d-107f4dbd7e7f Error occur when Update Profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public bool  updateMobileNumber(string userid,string mobilecode,string mobilenumber, out bool? dbStatus)
        {
            dbStatus = null;
            bool issuccess = false;
            try
            {
                var entity = _objContext.Profiles.Where(I=>I.UserId==userid).FirstOrDefault();
                if (entity != null)
                {
                    entity.MobileNumber = mobilenumber;
                    entity.MobileCode = mobilecode;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("334da0d8-44d1-4c52-904d-d2d5ca04dec7 Error occur during Create Profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return issuccess;

        }

        public bool isAgreementUpdated(string profileId)
        {
            bool updated = false;
            Profile objProfileResult = _objContext.Profiles.Where(objProfile => objProfile.Id == profileId).FirstOrDefault();
            if (objProfileResult != null)
            {
                objProfileResult.PendingAgreement = false;
                _objContext.SaveChanges();
                updated = true;
            }
            return updated;
        }

        public void SaveChanges()
        {
            try
            {
                _objContext.SaveChanges();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e300bdc2-4a82-4ed2-9b55-80e20e75a082 Error occur during Update Profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

        public override List<Profile> GetAll(Func<Profile, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Profile> profileList = null;
            try
            {
                profileList = _objContext.Profiles.Where(expression).OrderBy(I=>I.CreatedAt).ToList();
                if (profileList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a4098385-7d6b-40c3-a59c-e9267b85b5db Error occur when get all profile in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return profileList;
        }
    }
}