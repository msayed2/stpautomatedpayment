﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
namespace SQLProvider.Controllers
{
    public  class BankDetailsController
    {
        private TribalEntities _objContext = null;
        public BankDetailsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public BankDetail Get(Expression<Func<BankDetail, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            BankDetail entity = null;
            try
            {
                entity = _objContext.BankDetails.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"eee40a44-613e-4ddc-a90e-8e44ef5e486e Error occur during get Bank Details in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<BankDetail> GetAll(Expression<Func<BankDetail, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<BankDetail> bankDetails = null;
            try
            {
                bankDetails = _objContext.BankDetails.Where(expression).ToList();
                if (bankDetails != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c04c671f-c035-459d-8bab-59729532ad72 Error occur during get all bank details in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return bankDetails;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ab4ffd05-4d32-4213-b5db-563dcc4841c6 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(BankDetail bankDetail, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.BankDetails.Add(bankDetail);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"9039bb89-5942-4082-8b60-26105d0d8c68 Error occur during Create BankDetail in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(BankDetail bankDetail, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.BeneficiariesHistories.FirstOrDefault(I => I.Id == bankDetail.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("c4eaea6d-d6dc-495b-8576-3f477fa731cd Error occur when Updating BankDetail {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
