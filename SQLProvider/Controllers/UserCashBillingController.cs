﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class UserCashBillingController :Access.UserCashBillingController
    {
        private TribalEntities _objContext = null;
        public UserCashBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Update(UserCashBilling UserCashBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.UserCashBillings.FirstOrDefault(I => I.Id == UserCashBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = UserCashBilling.Avaliable;
                    objEntity.Spent = UserCashBilling.Spent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"78080e98-ecba-426f-85ca-5294025ca1bd Error occur when update user Cash billing in function ", ex);
            }
        }
    }
}
