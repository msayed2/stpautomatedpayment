﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class SystemMessageController
    {
        private TribalEntities _objContext = null;
        public SystemMessageController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public List<SystemMessage> GetAll(Func<SystemMessage, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<SystemMessage> objResult = null;
            try
            {
                objResult = _objContext.SystemMessages.ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("deec5e65-9841-49ab-bee0-fffacab7e376 Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }

        public SystemMessage Get(Func<SystemMessage, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            SystemMessage onjSystemMessage = null;
            try
            {
                onjSystemMessage = _objContext.SystemMessages.FirstOrDefault(expression);
                if (onjSystemMessage != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4fcd-8249-b752eacb269e Error occur when get company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return onjSystemMessage;
        }
        public void Create(SystemMessage onjSystemMessage, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.SystemMessages.Add(onjSystemMessage);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("41c9605a-042c-4fe4-9525-3b4134f0f327 Error occur during Create Company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void Update(SystemMessage onjSystemMessage, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.SystemMessages.FirstOrDefault(I => I.Id == onjSystemMessage.Id);
                if (objEntity != null)
                {
                    objEntity.Message = (onjSystemMessage.Message != null) ? onjSystemMessage.Message : objEntity.Message;
                    objEntity.UpdatedAt = onjSystemMessage.UpdatedAt;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5f7d9e51-de1f-4da3-8735-8facb032dd01 Error occur when update Card in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
    }
}
