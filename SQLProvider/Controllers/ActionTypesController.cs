﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Access = SQLAccess.Controllers;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class ActionTypesController : Access.ActionTypesController
    {
        private TribalEntities _objContext = null;
        public ActionTypesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override ActionType Get(Func<ActionType, bool> expression, out bool? dbStatus)
        {
            ActionType objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.ActionTypes.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9bf4c069-e99f-496d-b517-864f4985ebf5 Error occur during Get All Action Type in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public override List<ActionType> GetAll(Func<ActionType, bool> expression, out bool? dbStatus)
        {
            List<ActionType> objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.ActionTypes.Where(expression).ToList();
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d3a0b53d-a64a-4644-a342-8728af243912 Error occur during Get All Action Type in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
    }
}
