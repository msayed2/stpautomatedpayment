﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class DeclineTransactionsController : Access.DeclineTransactionsController
    {
        private TribalEntities _objContext = null;

        public DeclineTransactionsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(DeclinedTransaction objDeclinedTransaction, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.DeclinedTransactions.Add(objDeclinedTransaction);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"8ce47aa9-3232-426c-a5da-9c86af62da8d Error occur during create Declined Transaction in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override DeclinedTransaction Get(Expression<Func<DeclinedTransaction, bool>> expression, out bool? dbStatus)
        {
            DeclinedTransaction objDeclinedTransaction = null;
            dbStatus = null;
            try
            {
                objDeclinedTransaction = _objContext.DeclinedTransactions.FirstOrDefault(expression);
                if (objDeclinedTransaction != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"929db772-19bf-4d2e-9e21-382a7213eda0 Error occur during get Declined Transaction in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objDeclinedTransaction;
        }

        public override List<DeclinedTransaction> GetAll(Expression<Func<DeclinedTransaction, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<DeclinedTransaction> objDeclinedTransactions = null;
            try
            {
                if (expression != null)
                {
                    objDeclinedTransactions = _objContext.DeclinedTransactions.Where(expression).ToList();
                }
                else
                {
                    objDeclinedTransactions = _objContext.DeclinedTransactions.ToList();
                }
                if (objDeclinedTransactions != null && objDeclinedTransactions.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("edd71099-16dc-46ae-8be4-bca9f3340083 Error occur when get Declined Transactions data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objDeclinedTransactions;
        }

    }
}
