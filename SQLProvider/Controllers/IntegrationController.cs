﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;
namespace SQLProvider.Controllers
{
    public class IntegrationController : Access.IntegrationController
    {
        private TribalEntities _objContext = null;
        public IntegrationController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override List<Integration> GetALL(Func<Integration, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Integration> integrations = null;
            try
            {
                if (expression != null)
                {
                    integrations = _objContext.Integrations.Where(expression).ToList();
                }
                else
                {
                    integrations = _objContext.Integrations.ToList();
                }
                if (integrations != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"cb6dbac6-04aa2-47eb-8ss69b-6b734dd292432d Error occur when get integrations in function {MethodBase.GetCurrentMethod().Name } ", ex);
            }
            return integrations;
        }

        public override void CreateUserIntegration(IntegrationsPerUser objIntegration, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.IntegrationsPerUsers.Add(objIntegration);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"03e785ba-d7f3-4fa6-ae52-cf257b335951 Error occur during CreateUserIntegration in function ", ex);
            }
        }
    }
}
