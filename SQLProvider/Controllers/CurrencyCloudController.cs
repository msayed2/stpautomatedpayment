﻿using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Attribute = Entity.Entities.TribalEntities.Attribute;
using Entity.Entities.TribalEntities;
using Spacegene.Logger;
using System.Reflection;

namespace SQLProvider.Controllers
{
    public class CurrencyCloudController
    {
        private TribalEntities _objContext = null;

        public CurrencyCloudController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        //GetCity
        //GetState
        //GetCompanyDeposit
        public CompanyDeposit GetCompanyDeposit(Expression<Func<CompanyDeposit, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyDeposit objresult = null;
            try
            {
                objresult = _objContext.CompanyDeposits.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("852ba716-c1c7-4983-b9d7-68d881c03600 Error occur when get CompanyDeposit in function GetCompanyDeposit", ex);
            }
            return objresult;

        }
        public State GetState(Expression<Func<State, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            State objresult = null;
            try
            {
                objresult = _objContext.States.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("1fafb171-b3e5-4850-a550-96466bedc3b8 Error occur when GetState in function GetState", ex);
            }
            return objresult;

        }

        //GetAllDeposit
        public List<DepositsTransaction> GetAllDeposit(Func<DepositsTransaction, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            List<DepositsTransaction> objResult = null;
            try
            {

                objResult = _objContext.DepositsTransactions.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("9cddc77a-3e39-42d3-b639-2c17ccd97c50 Error occur when Get All Deposits in function GetAllDeposit", ex);
            }

            return objResult;
        }
        //   GetAllCitties
        public City GetCity(Expression<Func<City, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            City objresult = null;
            try
            {
                objresult = _objContext.Cities.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("aa5d5242-8d5b-488e-9725-f5a7d1f4b33f Error occur when Get City in function GetCity", ex);
            }
            return objresult;

        }
        public List<City> GetAllCities(Expression<Func<City, bool>> expression, out bool? dbStatus)
        {

            dbStatus = null;
            List<City> objResult = null;
            try
            {

                objResult = _objContext.Cities.AsNoTracking().Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("cf641fec-ea0c-44ee-82fb-0899c3007704 Error occur when Get All Cities in function GetAllCities", ex);
            }

            return objResult;
        }

        //
        //GetAllCountries
        public List<Country> GetAllCountries(Func<Country, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            List<Country> objResult = null;
            try
            {

                objResult = _objContext.Countries.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("e1426339-4448-485d-ae37-eed8472b60f5 Error occur when Get All Countries in function GetAllCountries", ex);
            }

            return objResult;
        }

        public List<State> GetAllStates(Func<State, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            List<State> objResult = null;
            try
            {

                objResult = _objContext.States.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("8c760dcf-187f-4512-872a-52197ce50c05 Error occur when Get All States in function GetAllStates", ex);
            }

            return objResult;
        }
        //GetCountry
        public Country GetCountry(Func<Country, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            Country objResult = null;
            try
            {

                objResult = _objContext.Countries.Where(expression).FirstOrDefault();
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("a6fc552e-2aa9-496a-ba61-7be38a933901 Error occur when Get Country in function GetCountry", ex);
            }

            return objResult;
        }
        public List<UserBeneficiary> GetAllBeneficiaries(Func<UserBeneficiary, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            List<UserBeneficiary> objResult = null;
            try
            {

                objResult = _objContext.UserBeneficiaries.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("d716df21-890d-452d-9018-3008d5ab8460 Error occur when Get All Beneficiaries in function GetAllBeneficiaries", ex);
            }

            return objResult;
        }
        public List<PaymentCurrency> GetAllCurrencies(Func<PaymentCurrency, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            List<PaymentCurrency> objResult = null;
            try
            {

                objResult = _objContext.PaymentCurrencies.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("cc22b49f-8b91-4cfe-b4cb-5d552a58f4b6 Error occur when Get All Currencies in function GetAllCurrencies", ex);
            }

            return objResult;
        }
        //public List<WalletTransactions> GetAllTransactions(Func<WalletTransactions, bool> expression, out bool? dbStatus)
        //{

        //    dbStatus = null;
        //    List<WalletTransactions> objResult = null;
        //    try
        //    {

        //        objResult = _objContext.WalletTransactions.Where(expression).ToList();
        //        if (objResult != null && objResult.Count > 0)
        //        {
        //            dbStatus = true;
        //        }
        //        else
        //        {
        //            dbStatus = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return objResult;
        //}
        ////public List<WalletStatus> GetAllTranasactionStatus(Func<WalletStatus, bool> expression, out bool? dbStatus)
        //{

        //    dbStatus = null;
        //    List<WalletStatus> objResult = null;
        //    try
        //    {

        //        objResult = _objContext.WalletStatus.Where(expression).ToList();
        //        if (objResult != null && objResult.Count > 0)
        //        {
        //            dbStatus = true;
        //        }
        //        else
        //        {
        //            dbStatus = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return objResult;
        //}
        public void CreatePayment(Payment objPayment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Payments.Add(objPayment);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("7ffd6a15-1af2-463c-8a0b-cb3ec1fad2fb Error occur during Create Payment in function CreatePayment", ex);
            }
        }

        public void CreateBeneficiaries(Beneficiary objBeneficiaries, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Beneficiaries.Add(objBeneficiaries);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("99726746-1587-4dae-b7c1-2dbb7b105f75 Error occur during Create Beneficiary in function CreateBeneficiaries", ex);
            }
        }
        public List<AddtionalAttribute> GetAddtionalAttributes(Func<AddtionalAttribute, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            List<AddtionalAttribute> objResult = null;
            try
            {

                objResult = _objContext.AddtionalAttributes.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("121950b3-9073-4f96-8c25-5d6c9572fd0b Error occur when Get Addtional Attributes in function GetAddtionalAttributes", ex);
            }

            return objResult;
        }

        public AddtionalAttribute GetAddtionalAttribute(Func<AddtionalAttribute, bool> expression, out bool? dbStatus)
        {

            dbStatus = null;
            AddtionalAttribute objResult = null;
            try
            {

                objResult = _objContext.AddtionalAttributes.Where(expression).FirstOrDefault();
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("0687bd64-e1e5-433e-9ca9-5c71821a205f Error occur when Get Addtional Attribute in function GetAddtionalAttribute", ex);
            }
            return objResult;
        }

        //public void CreateBeneficiaryBank(BeneficiaryBank objBeneficiaryBank, out bool? dbStatus)
        //{
        //    dbStatus = null;
        //    try
        //    {
        //        var entity = _objContext.BeneficiaryBank.Add(objBeneficiaryBank);
        //        if (entity != null)
        //        {
        //            _objContext.SaveChanges();
        //            dbStatus = true;
        //        }
        //        else
        //        {
        //            dbStatus = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // new Logger().LogError(string.Format("41c9605a-042c-4fe4-9525-3b4134f0f327 Error occur during Create Company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
        //    }
        //}
        public void CreateUserBeneficiaries(UserBeneficiary objUserBeneficiaries, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.UserBeneficiaries.Add(objUserBeneficiaries);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("933c887e-2b2f-4684-90be-b7c3caa9bb0a Error occur during Create User Beneficiaries in function CreateUserBeneficiaries", ex);
            }
        }
        //   //GetBeneficaryCountry

        public Country GetBeneficaryCountry(Expression<Func<Country, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Country objresult = null;
            try
            {
                objresult = _objContext.Countries.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("8c1e87d9-ca1d-4536-addd-baea8d71a73b Error occur when Get Beneficary Country in function GetBeneficaryCountry", ex);
            }
            return objresult;
        }

        public void CreateBankDetails(BankDetail objBankDetails, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.BankDetails.Add(objBankDetails);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("eedf1d34-350a-4b93-a1f9-327ef145ff78 Error occur during Create Bank Details in function CreateBankDetails", ex);
            }
        }

        public void CreateAddtionalAttributes(AddtionalAttribute objAddtionalAttributes, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.AddtionalAttributes.Add(objAddtionalAttributes);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("c05f65eb-47df-4745-b632-1c11d4ade195 Error occur during Create Addtional Attributes in CreateAddtionalAttributes", ex);
            }
        }
        public Beneficiary GetBeneficiary(Expression<Func<Beneficiary, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Beneficiary objresult = null;
            try
            {
                objresult = _objContext.Beneficiaries.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("e0319d5e-29da-45f6-9d87-ad595ab661d5 Error occur when Get Beneficiary in function GetBeneficiary", ex);
            }
            return objresult;
        }
        //public PaymentTransaction_status GetPaymentStatus(Expression<Func<PaymentTransaction_status, bool>> expression, out bool? dbStatus)
        //{
        //    dbStatus = null;
        //    PaymentTransaction_status objresult = null;
        //    try
        //    {
        //        objresult = _objContext.PaymentTransaction_status.FirstOrDefault(expression);

        //        if (objresult != null)
        //        {
        //            dbStatus = true;
        //        }
        //        else
        //        {
        //            dbStatus = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //new Logger().LogError(string.Format("4613afa2-81b0-4693-a339-45c02ad67056 Error occur when get all departments in function {0}", MethodBase.GetCurrentMethod().Name), ex);
        //    }
        //    return objresult;
        //}
        public Attribute GetAttribute(Expression<Func<Attribute, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Attribute objresult = null;
            try
            {
                objresult = _objContext.Attributes.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("5edfffea-1b5a-4907-ba59-3f940789bd21 Error occur when GetAttribute in function GetAttribute", ex);
            }
            return objresult;

        }
        public PaymentCurrency GetLocalCurrency(Expression<Func<PaymentCurrency, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            PaymentCurrency objresult = null;
            try
            {
                objresult = _objContext.PaymentCurrencies.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("42d41161-67d4-4c87-97fd-35eaf590ca6e Error occur when Get Local Currency in function GetLocalCurrency", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;

        }
        public BankDetail GetBankDetails(Expression<Func<BankDetail, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            BankDetail objresult = null;
            try
            {
                objresult = _objContext.BankDetails.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("e55bf02f-06ad-4f70-a3c7-e0585e0c5d78 Error occur when Get Bank Details in function GetBankDetails", ex);
            }
            return objresult;

        }
        //GetUserWallet
        //public CompanyWallet GetCompanyWallet(Expression<Func<CompanyWallet, bool>> expression, out bool? dbStatus)
        //{
        //    dbStatus = null;
        //    CompanyWallet objresult = null;
        //    try
        //    {
        //        objresult = _objContext.CompanyWallet.FirstOrDefault(expression);

        //        if (objresult != null)
        //        {
        //            dbStatus = true;
        //        }
        //        else
        //        {
        //            dbStatus = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //new Logger().LogError(string.Format("4613afa2-81b0-4693-a339-45c02ad67056 Error occur when get all departments in function {0}", MethodBase.GetCurrentMethod().Name), ex);
        //    }
        //    return objresult;

        //}
        public Beneficiary GetBankBeneficiaries(Expression<Func<Beneficiary, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Beneficiary objresult = null;
            try
            {
                objresult = _objContext.Beneficiaries.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1e130214-04d4-4152-af70-e713b72b40ab Error occur when get Bank Beneficiaries in function GetBankBeneficiaries", ex);
            }
            return objresult;

        }
        //CreateTransaction
        //public void CreateTransaction(WalletTransactions objWalletTransaction, out bool? dbStatus)
        //{
        //    dbStatus = null;
        //    try
        //    {
        //        var entity = _objContext.WalletTransactions.Add(objWalletTransaction);
        //        if (entity != null)
        //        {
        //            _objContext.SaveChanges();
        //            dbStatus = true;
        //        }
        //        else
        //        {
        //            dbStatus = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // new Logger().LogError(string.Format("41c9605a-042c-4fe4-9525-3b4134f0f327 Error occur during Create Company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
        //    }
        //}

        public BankDetail GetBeneficiaryBank(Expression<Func<BankDetail, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            BankDetail objresult = null;
            try
            {
                objresult = _objContext.BankDetails.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("2ca6a88b-c90b-4a41-83c0-4b6b4ae82e99 Error occur when get Get Beneficiary Bank in function GetBeneficiaryBank", ex);
            }
            return objresult;

        }
        //Wallet
        public List<Payment> GetAllPayments(Func<Payment, bool> expression, out bool? dbStatus)

        {
            dbStatus = null;
            List<Payment> objResult = null;

            try
            {

                objResult = _objContext.Payments.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("75b5a939-0f83-46c6-b56e-8eb9c85a718c Error occur when get Get All Payments in function GetAllPayments", ex);
            }

            return objResult;
        }

        public PaymentStatu GetPaymentStatus(Func<PaymentStatu, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            PaymentStatu objResult = null;
            try
            {
                objResult = _objContext.PaymentStatus.Where(expression).FirstOrDefault();
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("33c198a0-b319-407d-a717-624d4260313f Error occur during Get Payment Status in function GetPaymentStatus", ex);
            }
            return objResult;
        }


        public void UpdateBeneficiary(Beneficiary objBeneficiary, out bool? dbStatus, bool allowSaveChange = true)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Beneficiaries.FirstOrDefault(I => I.Id == objBeneficiary.Id);
                if (objEntity != null)
                {

                    //     objEntity.IntegrationId = objBeneficiary.IntegrationId;
                    objEntity.Name = objBeneficiary.Name;
                    objEntity.Address = objBeneficiary.Address;
                    objEntity.BankAccountHolderName = objBeneficiary.BankAccountHolderName;
                    objEntity.CityId = objBeneficiary.CityId;
                    objEntity.CountryId = objBeneficiary.CountryId;
                    objEntity.StateId = objBeneficiary.StateId;
                    objEntity.CurrencyId = objBeneficiary.CurrencyId;
                    objEntity.Zip = objBeneficiary.Zip;
                    objEntity.UpdatedAt = DateTime.Now;
                    objEntity.Email = objBeneficiary.Email;
                    if (allowSaveChange == true)
                    {
                        _objContext.SaveChanges();
                    }
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError("08121d21-367a-4c7d-8ed0-f564d9bd6ebb Error occur during Update Beneficiary in function UpdateBeneficiary", ex);
            }
        }

        public void UpdateBank(BankDetail objBank, out bool? dbStatus, bool allowSaveChange = true)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.BankDetails.FirstOrDefault(I => I.Id == objBank.Id);
                if (objEntity != null)
                {

                    objEntity.Name = objBank.Name;
                    objEntity.UpdatedAt = DateTime.Now;
                    if (allowSaveChange == true)
                    {
                        _objContext.SaveChanges();
                    }

                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError("41f054a5-cda8-4947-a9cf-b4918b1c52d9 Error occur during Update Bank in function UpdateBank", ex);
            }
        }

        public void UpdateAdditonalattributes(AddtionalAttribute objAttributes, out bool? dbStatus, bool allowSaveChange = true)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AddtionalAttributes.FirstOrDefault(I => I.Id == objAttributes.Id);
                if (objEntity != null)
                {

                    objEntity.Value = objAttributes.Value;
                    objEntity.UpdatedAt = DateTime.Now;
                    if (allowSaveChange == true)
                    {
                        _objContext.SaveChanges();
                    }

                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError("b2e080f8-2bed-4b33-b267-c020a83170d7 Error occur during Update Additonal attributes in function UpdateAdditonalattributes", ex);
            }
        }

        //public Wallet GetWallet(Func<Wallet, bool> expression, out bool? dbStatus)
        //{

        //    dbStatus = null;
        //    Wallet objResult = null;
        //    try
        //    {
        //        objResult = _objContext.Wallets.Where(expression).FirstOrDefault();

        //        if (objResult != null)
        //        {
        //            dbStatus = true;
        //        }
        //        else
        //        {
        //            dbStatus = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return objResult;
        //}
    }
}
