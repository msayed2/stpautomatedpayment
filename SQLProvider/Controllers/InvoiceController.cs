﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class InvoiceController: Access.InvoiceController
    {
        private TribalEntities _objContext = null;
        public InvoiceController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(Invoice objInvoice, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.Invoices.Add(objInvoice);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a01c7518-11c5-43e8-b414-00bac06921af Error occur during create Invoice in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

       

        public void CreateInvoiceDetail(InvoiceDetail model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.InvoiceDetails.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a01c7518-11c5-43e8-b414-00bac0692yyz2 Error occur during Add Invoice Detail Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

       

       

        

        public List<Invoice> GetAll(Func<Invoice, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Invoice> entity = new List<Invoice>();
            try
            {
                entity = _objContext.Invoices.Where(expression).ToList();
                dbStatus = true;

            }
            catch (Exception ex)
            {
                new Logger().LogError($"e03415eb-0df5-4e40-af06-25ee8c9f50d0 Error occur during get all Invoices Data in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public override List<InvoicesStatu> GetInvoicesStatus(Func<InvoicesStatu, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<InvoicesStatu> invoicesStatusList = null;
            try
            {
                invoicesStatusList = _objContext.InvoicesStatus.Where(expression).ToList();
                if (invoicesStatusList != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4fcd-8249-b752eacb269e Error occur when get Incredits in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return invoicesStatusList;
        }

        public override Invoice Get(Expression<Func<Invoice, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            Invoice objInvoice = null;
            try
            {
                objInvoice = _objContext.Invoices.FirstOrDefault(expression);
                if (objInvoice != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d4016bbb-6c9a-4c75-bf44-0d0ad724d8ba Error occur during get Invoice in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objInvoice;
        }
        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("664a893b-e519-46ba-918a-1919cbbd83e3 Error occur during SaveChanges in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }


}
