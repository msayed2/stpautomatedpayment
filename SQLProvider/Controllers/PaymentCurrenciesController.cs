﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class PaymentCurrenciesController
    {
        private TribalEntities _objContext = null;
        public PaymentCurrenciesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public PaymentCurrency Get(Expression<Func<PaymentCurrency, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            PaymentCurrency entity = null;
            try
            {
                entity = _objContext.PaymentCurrencies.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"671954b2-ada9-4d77-bf6a-346e0ef6069f Error occur during get Payment Currency in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<PaymentCurrency> GetAll(Expression<Func<PaymentCurrency, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<PaymentCurrency> paymentCurrencies = null;
            try
            {
                paymentCurrencies = _objContext.PaymentCurrencies.Where(expression).ToList();
                if (paymentCurrencies != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"9dab082e-8a63-4d5f-9dd9-4d37c118c20d Error occur during get all payment currencies in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return paymentCurrencies;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"56ed5172-8f42-498b-9a9d-e2d20f25799a Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(PaymentCurrency paymentCurrency, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PaymentCurrencies.Add(paymentCurrency);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"21d936f6-d653-4b0a-8014-f655b80ec47f Error occur during Create Payment currency in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(PaymentCurrency paymentCurrency, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.PaymentCurrencies.FirstOrDefault(I => I.Id == paymentCurrency.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("7f6a23a5-a499-4c33-87a5-0f348780a58a Error occur when Updating payment currency {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
