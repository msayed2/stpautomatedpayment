﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyInvoicesSettlementsSettingController : Access.CompanyInvoicesSettlementsSettingController
    {
        private TribalEntities _objContext = null;
        public CompanyInvoicesSettlementsSettingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override CompanyInvoicesSettlementsSetting Get(Func<CompanyInvoicesSettlementsSetting, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyInvoicesSettlementsSetting entity = null;
            try
            {
                entity = _objContext.CompanyInvoicesSettlementsSettings.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"33ee37sdsdsd-428-4a5c-94e0-fc48a9ecc8b7 Error occur during get CompanyInvoicesSettlementsSetting in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public override List<CompanyInvoicesSettlementsSetting> GetAll(Func<CompanyInvoicesSettlementsSetting, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<CompanyInvoicesSettlementsSetting> CompanyInvoicesSettlementsSettings = null;
            try
            {
                CompanyInvoicesSettlementsSettings = _objContext.CompanyInvoicesSettlementsSettings.Where(expression).ToList();
                if (CompanyInvoicesSettlementsSettings != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"986ea9c3-f168-4f5f-89be-651825aswa69 Error occur during get all CompanyInvoicesSettlementsSettings in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return CompanyInvoicesSettlementsSettings;
        }

        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"f7d4824e-1251-4c25-9333-dc2dadsdsda2551 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override void Create(CompanyInvoicesSettlementsSetting objCompanyInvoicesSettlementsSetting, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyInvoicesSettlementsSettings.Add(objCompanyInvoicesSettlementsSetting);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"6415136sdsd-dsaa-4e40-b03e-15695cbfa8a3 Error occur during Create CompanyInvoicesSettlementsSettings in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }
    }
}
