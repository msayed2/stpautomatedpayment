﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Attribute = Entity.Entities.TribalEntities.Attribute;

namespace SQLProvider.Controllers
{
   public class AttributesController
    {
        private TribalEntities _objContext = null;
        public AttributesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public Attribute Get(Expression<Func<Attribute, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Attribute entity = null;
            try
            {
                entity = _objContext.Attributes.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1039ba2e-a788-467b-8913-3abf4af56bc9 Error occur during get Attribute in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<Attribute> GetAll(Expression<Func<Attribute, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Attribute> attributes = null;
            try
            {
                attributes = _objContext.Attributes.Where(expression).ToList();
                if (attributes != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1a098415-ee66-43c3-ae9e-76fdd827ef89 Error occur during get all Attributes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return attributes;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"45bef337-6b34-4fe3-92d9-ac906d450aa8 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(Attribute attribute, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Attributes.Add(attribute);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"54c3e289-024a-4e38-8cf2-be2efc17d354 Error occur during Create Attribute in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(Attribute attribute, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Attributes.FirstOrDefault(I => I.Id == attribute.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("518e4c73-6688-443c-8d38-c1224596fea6 Error occur when Updating Beneficiary {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
