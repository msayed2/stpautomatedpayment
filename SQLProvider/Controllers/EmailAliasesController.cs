﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;
namespace SQLProvider.Controllers
{
    public class EmailAliasesController : Access.EmailAliasesController
    {
        private TribalEntities _objContext = null;
        public EmailAliasesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(EmailAlias objEmailAlias, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.EmailAliases.Add(objEmailAlias);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("05269305-41d8-4815-81f0-ea539ce96bb6 Error occur during Create EmailAlias in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override EmailAlias Get(Func<EmailAlias, bool> expression, out bool? dbStatus)
        {
            EmailAlias objEmailAlias = null;
            dbStatus = null;
            try
            {
                objEmailAlias = _objContext.EmailAliases.FirstOrDefault(expression);
                if (objEmailAlias != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("c8705f8f-bf9a-4a0a-9d2d-84c3c5bb542b Error occur during get EmailAlias in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objEmailAlias;
        }
        public override List<EmailAlias> GetAll(Func<EmailAlias, bool> expression, out bool? dbStatus)
        {
            List<EmailAlias> objEmailAliasList = null;
            dbStatus = null;
            try
            {
                objEmailAliasList = _objContext.EmailAliases.Where(expression).ToList();
                if (objEmailAliasList != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("195aa105-09a0-404e-baf3-91d8b817f3ea Error occur during get all EmailAlias in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objEmailAliasList;
        }
    }
}
