﻿using Spacegene.Logger;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SQLProvider.Controllers
{
    public class ReceivedPaymentsController
    {
        private TribalPaymentEntities _objContext = null;
        public ReceivedPaymentsController(TribalPaymentEntities objContext)
        {
            _objContext = objContext;
        }

        public RecievedPayment Get(Expression<Func<RecievedPayment, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            RecievedPayment entity = null;
            try
            {
                entity = _objContext.RecievedPayments.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"70423592-d9e7-4d99-85b7-501c7ea2af16 Error occur during get Received Payment in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<RecievedPayment> GetAll(Expression<Func<RecievedPayment, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<RecievedPayment> receivedPayment = null;
            try
            {
                receivedPayment = _objContext.RecievedPayments.Where(expression).ToList();
                if (receivedPayment != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"8b81d019-c9b5-4910-8841-636d9d2361c2 Error occur during get all received payments in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return receivedPayment;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"8533166d-4d53-4f2c-92cc-4008f27749b1 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(RecievedPayment receivedPayment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.RecievedPayments.Add(receivedPayment);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"40bedfbc-6e48-493f-9bf7-986577d2b000 Error occur during Create Received Payment in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(RecievedPayment receivedPayment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.RecievedPayments.FirstOrDefault(I => I.Id == receivedPayment.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("0f40afe1-72c8-445e-a129-cabe4fc75084 Error occur when Updating Received Payment {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
