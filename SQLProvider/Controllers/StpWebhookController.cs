﻿using Spacegene.Logger;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SQLProvider.Controllers
{
    public class StpWebhookController
    {
        private TribalPaymentEntities _objContext = null;
        public StpWebhookController(TribalPaymentEntities objContext)
        {
            _objContext = objContext;
        }

        public StpWebhook Get(Expression<Func<StpWebhook, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            StpWebhook entity = null;
            try
            {
                entity = _objContext.StpWebhooks.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"14bd9f23-cc2e-42ee-a80b-8edbac603832 Error occur during get StpWebhook in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<StpWebhook> GetAll(Expression<Func<StpWebhook, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<StpWebhook> objStpWebhook = null;
            try
            {
                objStpWebhook = _objContext.StpWebhooks.Where(expression).ToList();
                if (objStpWebhook != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"852f2a98-71d6-4e5e-99c2-26ee73d0b048 Error occur during get all StpWebhooks in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objStpWebhook;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"96fef8c8-9851-494c-ace6-a21b788605cf Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(StpWebhook stpWebhook, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.StpWebhooks.Add(stpWebhook);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3ca51afc-95f1-4196-a136-e75416f35cb0 Error occur during Create StpWebhook in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(StpWebhook stpWebhook, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.StpWebhooks.FirstOrDefault(I => I.Id == stpWebhook.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e1b2ffc3-2c12-4239-bb76-dff10ad6f759 Error occur when Update stpWebhook Operation {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
