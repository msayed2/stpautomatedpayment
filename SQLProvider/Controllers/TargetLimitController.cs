﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class TargetLimitController : Access.TargetLimitController
    {
        private TribalEntities _objContext = null;
        public TargetLimitController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(TargetLimit objTargetLimit, out bool? dbStatus, bool allowSaveChanges = true)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.TargetLimits.Add(objTargetLimit);
                if (entity != null)
                {
                    if (allowSaveChanges)
                    {
                        _objContext.SaveChanges();
                    }
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("2f15872a-cfc1-4c2f-94e1-99dc65759480 Error occur during Create TargetLimit in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override void CreateBulk(List<TargetLimit> targetLimits, out bool? dbStatus, bool allowSaveChanges = true)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.TargetLimits.AddRange(targetLimits);
                if (entity != null)
                {
                    if (allowSaveChanges)
                    {
                        _objContext.SaveChanges();
                    }
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("52d3a765-5d1d-481b-a22d-1b2ff4672bf8 Error occur during Create Bulk TargetLimits in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override List<TargetLimit> GetAll(Expression<Func<TargetLimit, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<TargetLimit> targetList = null;
            try
            {
                targetList = _objContext.TargetLimits.Where(expression).ToList();
                if (targetList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("c2fdd0f7-6b8e-416a-b236-1a5520f96f55 Error occur during get all TargetLimits in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return targetList;
        }
        public override void DeActivate(Func<TargetLimit, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.TargetLimits.Where(expression).ToList();
                if (objEntity != null)
                {
                    objEntity.Select(obj => { obj.IsActive = false; obj.UpdatedAt = DateTime.UtcNow; return obj; }).ToList();
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"39341836-9a94-4fd8-b06c-608cd2f1921b Error occur when DeActivate target limit in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override TargetLimit Get(Func<TargetLimit, bool> expression, out bool? dbStatus)
        {
            TargetLimit objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.TargetLimits.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("7aa4cb5e-b886-4da0-bc4f-7e3e101aa6fb Error occur during Get Target Limit in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
    }
}
