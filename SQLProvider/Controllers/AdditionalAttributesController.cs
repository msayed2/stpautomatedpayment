﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class AdditionalAttributesController
    {
        private TribalEntities _objContext = null;
        public AdditionalAttributesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public AddtionalAttribute Get(Expression<Func<AddtionalAttribute, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            AddtionalAttribute entity = null;
            try
            {
                entity = _objContext.AddtionalAttributes.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ccb31adb-5c86-4547-91a5-e3b8395249c2 Error occur during get additional attribute in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<AddtionalAttribute> GetAll(Expression<Func<AddtionalAttribute, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<AddtionalAttribute> addtionalAttributes = null;
            try
            {
                addtionalAttributes = _objContext.AddtionalAttributes.Where(expression).ToList();
                if (addtionalAttributes != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"7c56bf4d-64a0-4133-8fb7-eec48b8c72f3 Error occur during get all additional attributes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return addtionalAttributes;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"26b549d3-f2b9-4546-a18f-ab0205c80947 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(AddtionalAttribute addtionalAttribute, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.AddtionalAttributes.Add(addtionalAttribute);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"bdad1326-1e69-496e-a6c3-7cc9b6374bf5 Error occur during Create Additional Attribute in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(AddtionalAttribute addtionalAttribute, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AddtionalAttributes.FirstOrDefault(I => I.Id == addtionalAttribute.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("fbb14b4e-84b1-4de8-8a62-e34bbf141fd2 Error occur when Updating Additional Attribute {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
