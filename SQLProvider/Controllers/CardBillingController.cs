﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
   public class CardBillingController : Access.CardBillingController
    {
        private TribalEntities _objContext = null;
        public CardBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(CardBilling objCardBilling, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CardBillings.Add(objCardBilling);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1738955b-4090-49a7-a8b7-d8bd890f72a9 Error occur during create card billing in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override void CreateBulk(List<CardBilling> cardBillings, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CardBillings.AddRange(cardBillings);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c92c4e02-81f4-4c89-8bf5-45da42579438 Error occur during create bulk card billings in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override void Update(CardBilling objCardBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CardBillings.FirstOrDefault(I => I.Id == objCardBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = objCardBilling.Avaliable;
                    objEntity.Spent = objCardBilling.Spent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("1acde58d-f447-4fa2-9ecb-74a81f07aa14 Error occur when update card billing in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
