﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class UsersController : Access.UsersController
    {
        private TribalEntities _objContext = null;
        public UsersController(TribalEntities objContext)
        {
            _objContext = objContext;
            _objContext.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }


        public override AspNetUser Get(Func<AspNetUser, bool> expression, out bool? dbStatus)
        {
            AspNetUser objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.AspNetUsers.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e135bb65-ae5d-4b04-a2c3-78970dd45c0b Error occur during Get User in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public  int GetUserCount(Func<Profile, bool> expression, out bool? dbStatus)
        {
            int objResult=0;
            dbStatus = null;
            try
            {
                objResult = _objContext.Profiles.Where(expression).Count();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e135bb65-ae5d-4b04-a2c3-78970dd45c0b Error occur during Get User in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public override List<AspNetUser> GetAll(Func<AspNetUser, bool> expression, out bool? dbStatus)
        {
            List<AspNetUser> objResult = null;
            dbStatus = null;
            try
            {
                if (expression == null)
                {
                    objResult = _objContext.AspNetUsers.ToList();
                }
                else
                {
                    objResult = _objContext.AspNetUsers.Where(expression).ToList();
                }
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("71e52b81-89a6-4264-b97a-6128c34f4c22 Error occur during Get Users in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public RegisterToken ValidateToken(string token, out bool? dbStatus)
        {
            RegisterToken objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.RegisterTokens.FirstOrDefault(I => I.HashedToken == token && I.IsActive == true && I.IsExpired == false);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("2f0c02a7-24f9-469f-8fd4-3e743fde25c4 Error occur during Get Company Contacts in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;

        }

        public List<AspNetUser> GetTotalUsers(string user_id, out bool? status)
        {
            status = null;
            List<AspNetUser> result = null;
            try
            {

                result = (from Prof in _objContext.Profiles
                          join asp in _objContext.AspNetUsers
                          on Prof.UserId equals asp.Id
                          where Prof.CompanyId == (from Prof in _objContext.Profiles
                                                   where Prof.UserId == user_id
                                                   select Prof.CompanyId).FirstOrDefault() && Prof.IsActive == true
                          where asp.Id != user_id
                          select asp
                          ).Distinct().OrderByDescending(I => I.Profiles.FirstOrDefault().CreatedAt).ToList();




                status = (result.Count > 0 && result != null) ? true : false;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("0f9d24b2-dd95-4b1a-8961-1497f81a3d24 Exception when getting Users count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public override List<AspNetUser> GetAll(Func<AspNetUser, bool> expression, int skip, int take, out bool? dbStatus)
        {
            List<AspNetUser> objResult = null;
            dbStatus = null;
            try
            {
                if (expression == null)
                {
                    objResult = _objContext.AspNetUsers.Where(expression).ToList();
                }
                else
                {
                    objResult = _objContext.AspNetUsers.Where(expression).OrderBy(I => I.Profiles.FirstOrDefault().CreatedAt).Skip(skip).Take(take).ToList();
                }
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("1911b11e-274f-4fb6-b2b7-fa6600d6cb4f Error occur during Get Users in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public override List<AspNetUser> GetAll(int skip, int take, out bool? dbStatus)
        {
            List<AspNetUser> objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.AspNetUsers.OrderBy(I => I.Profiles.FirstOrDefault().CreatedAt).Skip(skip).Take(take).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ee05b809-3467-496a-8824-2cf9900023d3 Error occur during Get Users in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public override void Save(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e621235c-a05c-4a20-9415-72f9f3c19181 Error occur during Save data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override int GetTotalUsersCount(Func<AspNetUser, bool> expression, out bool? status)
        {
            status = null;
            int result = -1;
            try
            {
                if (expression == null)
                {
                    result = _objContext.AspNetUsers.Count();
                }
                else
                {
                    result = _objContext.AspNetUsers.Where(expression).Count();
                }
                status = (result > 0) ? true : false;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("13ced89b-d8f0-4b8d-a359-4ecc252a135c Exception when getting Users count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public override void Delete(Func<AspNetUser, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AspNetUsers.FirstOrDefault(expression);
                if (objEntity != null)
                {
                    objEntity.Profiles.FirstOrDefault().IsActive = false;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("23e2688a-86e3-4cff-9aea-7e91c9d60f73 Error occur when Delete User in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override List<Device> GetTrustedDevices(Func<Device, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Device> objResult = null;

            try
            {
                objResult = _objContext.Devices.Where(expression).ToList();
                if (objResult != null && objResult.Count() > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("db4d7939-9b09-4f22-80e3-3b7f89025fd4 Error occur when Get Trusted Devices in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public Device GetDevice(Func<Device, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Device objResult = null;

            try
            {
                objResult = _objContext.Devices.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8e2196eb-62bd-42cb-87bc-016d5ea943c9 Error occur when Get Trusted Devices in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public Device GetFirstDevice(Func<Device, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Device objResult = null;

            try
            {
                objResult = _objContext.Devices.OrderBy(D => D.CreationTime).FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8e2196eb-62bd-42cb-87bc-016d5ea943c9 Error occur when Get Trusted Devices in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public void DeleteDevice(Func<Device, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objResult = _objContext.Devices.FirstOrDefault(expression);
                if (objResult != null)
                {
                    objResult.RememberMe = false;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("afb85141-da24-48fa-991c-204628150de2 Error occur when Get Trusted Devices in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override CompanyContact Create(Func<CompanyContact, bool> expression, CompanyContact model, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyContact objResult = null;
            try
            {
                var entity = _objContext.CompanyContacts.FirstOrDefault(I => I.Contact == model.Contact && I.IsActive == true);
                if (entity == null)
                {
                    objResult = _objContext.CompanyContacts.Add(model);
                    if (objResult != null)
                    {
                        _objContext.SaveChanges();
                        dbStatus = true;
                    }
                }
                else
                {
                    dbStatus = false;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("c1fce52f-9fd1-4aa7-9757-380cdf6da056 Error occur when Adding Billing contact in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public override List<AspNetUser> GetMostActiveUsers(string user_id, DateTime start_at, DateTime end_at, int take, out bool? status)
        {
            status = null;
            List<AspNetUser> objusers = null;
            DateTime last30Days = DateTime.UtcNow.AddDays(-30);
            try
            {
                objusers = _objContext.AspNetUsers.Where(U => U.Profiles.FirstOrDefault().CompanyId == user_id).SelectMany(U => U.Cards)
                                  .Select(C => new { card = C, spent = C.Transactions.Where(T => T.IsActive && T.CreatedAt >= start_at && T.CreatedAt <= end_at).Count() > 0 ? C.Transactions.Where(T => T.IsActive && T.CreatedAt >= start_at && T.CreatedAt <= end_at).Sum(T => T.Amount) : 0 })
                                  .GroupBy(C => C.card.AspNetUser)
                                    .OrderBy(I => I.Sum(C => C.spent)).Take(take).Select(C => C.Key).ToList();

                //objusers = (from prof in _objContext.Profiles
                //            join cards in _objContext.Cards
                //            on prof.UserId equals cards.UserId
                //            join trans in _objContext.Transactions
                //            on cards.Id equals trans.CardId
                //            where prof.CompanyId == (from Profle in _objContext.Profiles
                //                                     where Profle.UserId == user_id
                //                                     select Profle.CompanyId).FirstOrDefault()
                //            where trans.CreatedAt >= last30Days
                //            group trans by cards.UserId into G
                //            orderby G.Sum(T => T.Amount) descending
                //            select _objContext.AspNetUsers.FirstOrDefault(item => item.Id == G.Key)).OrderBy(U => U.Cards.SelectMany( C=> C.Transactions).Sum(P=>P.Amount)).Take(take).ToList();

                if (objusers != null && objusers.Count > 0)
                {
                    status = true;
                }
                else
                {
                    status = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("379658ea-d686-4fb3-90d8-e83e0cfb2cd3 Exception when getting Users count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objusers;
        }
        public List<AspNetUser> GetRecentAdded(string user_id, int take, out bool? status)
        {
            status = null;
            List<AspNetUser> objusers = null;

            try
            {
                objusers = (
                                      from prof in _objContext.Profiles
                                      join users in _objContext.AspNetUsers
                                      on prof.UserId equals users.Id
                                      where prof.CompanyId == (from Profle in _objContext.Profiles
                                                               where Profle.UserId == user_id
                                                               select Profle.CompanyId).FirstOrDefault()

                                      orderby prof.CreatedAt descending
                                      select users
                               ).Take(take).ToList();

                if (objusers != null && objusers.Count > 0)
                {
                    status = true;
                }
                else
                {
                    status = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("bc77f34a-fe2f-4420-b62e-ffe70c0f6628 Exception when getting Users count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objusers;
        }
        public override void DeleteBilling(Func<CompanyContact, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CompanyContacts.FirstOrDefault(expression);
                if (objEntity != null)
                {
                    objEntity.IsActive = false;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("c451265a-9589-4d3b-b63e-a836af3bee2e Exception when getting Users count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override void DeleteSuperAdmin(Func<Profile, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;

            try
            {
                int? objuserType = _objContext.Profiles.Where(expression)?.FirstOrDefault()?.CurrentTypeId;
                if (objuserType != null)
                {
                    _objContext.Profiles.Where(expression).FirstOrDefault().CurrentTypeId = _objContext.Profiles.Where(expression).FirstOrDefault().InitialTypeId.Value;
                    _objContext.Profiles.Where(expression).FirstOrDefault().InitialTypeId = objuserType;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d387aa8c-2838-49f0-bd31-6012a340d76b Exception when Deleting Super Admin in database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public override List<CompanyContact> GetAllBillingContacts(string company_id, out bool? dbStatus)
        {
            List<CompanyContact> objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.CompanyContacts.Where(I => I.CompanyId == company_id && I.IsActive == true).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a3fab420-4411-4d88-a5e6-814c02664c21 Error occur during Get Company Contacts in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public CompanyContact GetBilling(string contact_id, out bool? dbStatus)
        {
            CompanyContact objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.CompanyContacts.FirstOrDefault(I => I.Id == contact_id && I.IsActive == true);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("487e10a9-d23e-4b8b-bc2c-894b05b7f6b0 Error occur during Get Company Contacts in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public override List<AspNetUser> GetAdmins(string company_id,int CurrentTypeId, out bool? dbStatus)
        {
            List<AspNetUser> objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.AspNetUsers.Where(I => I.Profiles.FirstOrDefault().CompanyId == company_id && I.Profiles.FirstOrDefault().CurrentTypeId == CurrentTypeId).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("1944af5f-dbf2-4845-a041-9c8ace12c1ca Error occur during Get Company Contacts in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public List<UserState> GetStatus(out bool? dbStatus)
        {
            dbStatus = null;
            List<UserState> objResult = null;
            try
            {
                objResult = _objContext.UserStates.ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("60f36d80-8503-47d4-b62e-4aea30af6664 Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public List<UserRole> GetRoles(out bool? dbStatus)
        {
            dbStatus = null;
            List<UserRole> objResult = null;
            try
            {
                objResult = _objContext.UserRoles.ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("60f36d80-8503-47d4-b62e-4aea30af6664 Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public void UpdateUserStatus(string loggedInId,string user_id, int status_id, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AspNetUsers.FirstOrDefault(I => I.Id == user_id)?.Profiles?.FirstOrDefault();
                if (objEntity != null)
                {
                    objEntity.StatusId = status_id;
                    objEntity.UpdatedAt = DateTime.UtcNow;
                    objEntity.UpdatedBy = loggedInId;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("75ed83a3-638f-45c4-aeac-58a2a9795eea Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void UpdateUserLimit(string user_id, long limit, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AspNetUsers.FirstOrDefault(I => I.Id == user_id)?.Profiles?.FirstOrDefault();
                if (objEntity != null)
                {
                    objEntity.Limit = limit;
                    objEntity.UpdatedAt = DateTime.UtcNow;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("3118770a-b2ae-4bc8-b8d2-8b1b56fec5f4 Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void UpdateUserDepartment(string user_id, string department_id, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AspNetUsers.FirstOrDefault(I => I.Id == user_id)?.Profiles?.FirstOrDefault();
                if (objEntity != null)
                {
                    objEntity.DepartmentId = department_id;
                    objEntity.UpdatedAt = DateTime.UtcNow;
                    objEntity.UpdatedBy = user_id;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e78f6669-46e5-4ab1-8f15-d210cda4100f Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void UpdateUserEmail(string user_id, string email, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AspNetUsers.FirstOrDefault(I => I.Id == user_id);
                if (objEntity != null)
                {
                    objEntity.Email = email;
                    objEntity.Profiles.FirstOrDefault().UpdatedAt = DateTime.UtcNow;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ec7d3204-6ce9-4fe5-b6c7-74014db21e22 Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void UpdateUserPhone(string user_id, string phone, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.AspNetUsers.FirstOrDefault(I => I.Id == user_id);
                if (objEntity != null)
                {
                    objEntity.PhoneNumber = phone;
                    if (objEntity.Profiles != null && objEntity.Profiles.Count > 0)
                    {
                        objEntity.Profiles.FirstOrDefault().UpdatedAt = DateTime.UtcNow;
                    }
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("424ae359-94a2-4dd7-9280-ca0485f8a630 Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public override List<AspNetUser> GetNonSuperAdminsUsers(string user_id, out bool? dbStatus)
        {
            dbStatus = null;
            List<AspNetUser> result = null;
            try
            {

                result = (from Prof in _objContext.Profiles
                          join asp in _objContext.AspNetUsers
                          on Prof.UserId equals asp.Id
                          join Comp in _objContext.Companies
                          on Prof.CompanyId equals Comp.Id

                          where Prof.CompanyId == (from Prof in _objContext.Profiles
                                                   where Prof.UserId == user_id
                                                   select Prof.CompanyId).FirstOrDefault() && Prof.IsActive == true
                          where asp.Id != user_id
                          where (Prof.CurrentTypeId != 0 && Prof.CurrentTypeId != null)
                          select asp
                          ).Distinct().OrderByDescending(I => I.Profiles.FirstOrDefault().CreatedAt).ToList();

                dbStatus = (result.Count > 0 && result != null) ? true : false;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("408c70d0-d25c-422a-828f-c7083c7acdb6 Exception when getting Non Super Admins Users from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public override double GetUserLimit(string user_id, out bool? dbStatus)
        {
            dbStatus = null;
            double result = -1;

            try
            {
                result = _objContext.Profiles.FirstOrDefault(I => I.UserId == user_id).Limit;

                if (result != -1)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("823c31d8-9854-4598-b0c4-a779da175011 Exception when getting user limit from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public List<string> GetCompanyDepartments(string company_id, out bool? dbStatus)
        {
            dbStatus = null;
            List<string> objresult = null;
            try
            {
                objresult = _objContext.CompanyDepartments.Where(I => I.CompanyId == company_id).Select(I => I.Name).ToList();

                if (objresult != null && objresult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }

            catch (Exception ex)
            {
                new Logger().LogError(string.Format("974abb72-2410-4938-824b-2b1a29eb5262 Error occur when get all departments in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;
        }
        public int GetAllCardHolders(out bool? dbStatus)
        {
            dbStatus = null;
            int objresult = 0;

            try
            {
                objresult = _objContext.AspNetUsers.Count();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("b4baca8a-fa4a-4bdf-85f2-52584bcf8712 Error occur when get all Card Holders in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;

        }
        public void CreateRegisterToken(RegisterToken objToken, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.RegisterTokens.Add(objToken);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("76165056-6992-4cad-8f41-60573db4953e Error occur during Create RegisterToken in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public void UpdateEnableWallet(Func<Profile, bool> expression, bool value, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Profiles.Where(expression).FirstOrDefault();
                if (objEntity != null)
                {
                    objEntity.EnableWallet = value;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("59a255fe-9b9d-4578-ae12-6a662ee459e0 Error occur when Update Eenable Wallet in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void UpdateRegisterToken(Func<RegisterToken, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.RegisterTokens.Where(expression);
                if (entity != null)
                {
                    foreach (var item in entity)
                    {
                        item.IsExpired = true;
                    }
                    _objContext.Profiles.FirstOrDefault().UpdatedAt = DateTime.UtcNow;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("fa384bbf-4e77-4af9-b8d1-dcb2a0a53f51 Error occur during update RegisterToken in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void Create(Device objDevice)
        {
            _objContext.Devices.Add(objDevice);
            _objContext.SaveChanges();
        }

    }
}
