﻿using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Access = SQLAccess.Controllers;
using Entity.Entities.TribalEntities;
using Spacegene.Logger;
using System.Reflection;
using Newtonsoft.Json;

namespace SQLProvider.Controllers
{
    public class CardsController : Access.CardsController
    {
        private TribalEntities _objContext = null;
        public CardsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override List<Card> GetAll(int skip, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Card> objCards = null;
            try
            {
                if (take == 0)
                {
                    objCards = _objContext.Cards.Include("AspNetUser.Profiles").Include("Transactions").ToList();
                }
                else
                {
                    objCards = _objContext.Cards.OrderBy(I => I.CreatedAt).Skip(skip).Take(take).ToList();
                }
                if (objCards != null && objCards.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5e3226eb-6228-4b43-ae7b-4fdb1d87d586 Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCards;
        }
        public override List<Card> GetAll(Func<Card, bool> expression, int skip, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Card> objCards = null;
            try
            {
                if (take <= 0)
                {
                    objCards = _objContext.Cards.Where(expression).ToList();
                }
                else
                {
                    objCards = _objContext.Cards.Where(expression).Skip(skip).Take(take).ToList();
                }
                if (objCards != null && objCards.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6dfddb79-60e7-4d00-b485-5873cdf36fab Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCards;
        }
        public override void Create(Profile profile,Card objCard, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Cards.Add(objCard);
                if (entity != null)
                {
                    entity.CreatedBy = profile.UserId;
                    entity.CreatedAt = DateTime.UtcNow;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("322cd77f-4697-47fd-a91f-8cfd95fde523 Error occur during Create Card in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override Card Get(Func<Card, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Card objCard = null;
            try
            {
                objCard = _objContext.Cards.FirstOrDefault(expression);
                if (objCard != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"87eb8952-cece-4430-a737-abd8c5732fc7 Error occur when get Card in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objCard;
        }
        public override void Update(Profile profile,Func<Card, bool> expression, int status_id, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Cards.FirstOrDefault(expression);
                if (objEntity != null)
                {
                    objEntity.StatusId = status_id;
                    objEntity.UpdatedAt = DateTime.UtcNow;
                    objEntity.UpdatedBy = profile.UserId;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("388223b1-4df5-419e-906a-503bc94eabba Error occur when get all Cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override void RemoveTags(ICollection<Tag> tags, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objResult = _objContext.Tags.RemoveRange(tags);
                if (objResult != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6ac5dcfc-e9f4-40b0-a724-669659d8de0b Error occur when update Card in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override int GetTotalCardsCount(Func<Card, bool> expression, out bool? status)
        {
            status = null;
            int result = -1;
            try
            {
                result = _objContext.Cards.Where(expression).Count();
                status = (result > 0) ? true : false;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("033c3c17-a726-42b0-bf66-fb46e104b649 Exception when getting Cards count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public override void Delete(Func<Card, bool> expression, out bool? dbStatus)
        {
            throw new NotImplementedException();
        }
        public override List<CardType> GetTypes(out bool? dbStatus)
        {
            dbStatus = null;
            List<CardType> objResult = null;
            try
            {
                objResult = _objContext.CardTypes.ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("92c258b0-87f6-47e8-895a-9ef74d13109d Exception when getting Cards count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public Status GetStatus(Func<Status, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Status objResult = null;
            try
            {
                objResult = _objContext.Status.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("1237bc39-83f7-449a-b6f4-c37234819e0e Exception when getting Cards count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public List<Card> GetMostUsed(string company_id, DateTime start_at, DateTime end_at, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Card> objCards = null;
            DateTime last30Days = DateTime.UtcNow.AddDays(-30);
            try
            {

                objCards = _objContext.Cards.Where(C => C.AspNetUser.Profiles.FirstOrDefault().CompanyId == company_id)
                                  .Select(C => new { card = C, spent = C.Transactions.Where(T => T.IsActive && T.CreatedAt >= start_at && T.CreatedAt <= end_at).Count() > 0 ? C.Transactions.Where(T => T.IsActive && T.CreatedAt >= start_at && T.CreatedAt <= end_at).Sum(T => T.Amount) : 0 })
                                  .OrderBy(I => I.spent).Take(take).Select(C => C.card).ToList();

                /*objCards = (from prof in _objContext.Profiles
                            join cards in _objContext.Cards
                            on prof.UserId equals cards.UserId
                            join trans in _objContext.Transactions
                            on cards.Id equals trans.CardId
                            where prof.CompanyId == company_id
                            where trans.CreatedAt >= last30Days
                            group trans by cards.Id into G
                            orderby G.Sum(I => I.Amount) descending
                            select _objContext.Cards.FirstOrDefault(I => I.Id == G.Key)).OrderByDescending(I => I.Transactions.Sum(T => T.Amount)).Take(take).ToList();*/

                if (objCards != null && objCards.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("18e40ee2-35d2-4c9f-bfa8-34a09f3771da Error occur when get most used cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCards;

        }

        public List<Card> GetRecentAdded(string company_id, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Card> objCards = null;
            try
            {
                objCards = _objContext.Cards.Where(C => C.AspNetUser.Profiles.FirstOrDefault().CompanyId == company_id)
                     .OrderBy(I => I.CreatedAt).Take(take).ToList();

                /*objCards = (
                                      from prof in _objContext.Profiles
                                      join cards in _objContext.Cards
                                      on prof.UserId equals cards.UserId
                                      where prof.CompanyId == company_id
                                      orderby cards.CreatedAt descending
                                      select cards
                               ).Take(take).ToList();*/
                if (objCards != null && objCards.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("362124d0-8992-4776-9b04-78713d06f75c Error occur when get most used cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCards;

        }
        public List<Status> GetStatus(out bool? dbStatus)
        {
            dbStatus = null;
            List<Status> objResult = null;
            try
            {
                objResult = _objContext.Status.ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("958c6742-8f26-45d6-abfd-ea3fde64a836Error occur when get Cards Status in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public void UpdateCardLimit(string user_id,string card_id, double limit, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Cards.FirstOrDefault(I => I.Id == card_id);
                if (objEntity != null)
                {
                    objEntity.Limit = limit;
                    objEntity.UpdatedAt = DateTime.UtcNow;
                    objEntity.UpdatedBy = user_id;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("55368696-3c08-471c-9d96-6a595d7368c8 Error occur when Update limit in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void SaveChanges()
        {
            try
            {
                _objContext.SaveChanges();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e45d4e02-b49a-4c15-b99e-1c4823835c7a Error occur during Saving cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

    }
    public class CardSpent
    {
        public Card card { set; get; }
        public double? spent { set; get; }
    }
}
