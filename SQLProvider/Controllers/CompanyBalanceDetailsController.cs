﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyBalanceDetailsController : Access.CompanyBalanceDetailsController
    {
        private TribalEntities _objContext = null;
        public CompanyBalanceDetailsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(CompanyBalanceDetail objCompanyBalanceDetail, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyBalanceDetails.Add(objCompanyBalanceDetail);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
               
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a2d3b38b-0f81-40c3-b7e2-1ec536cc9084 Error occur during Create Company Balance Details in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override void DeActivate(string id, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyBalanceDetails.FirstOrDefault(I=>I.Id ==id);
                if (entity != null)
                {
                    entity.IsActive = false;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("2af144cd-b38e-41e8-845e-dbc42880a20f Error occur during DeActivate record in Company Balance Details  in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
