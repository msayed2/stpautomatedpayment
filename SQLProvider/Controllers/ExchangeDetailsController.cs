﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class ExchangeDetailsController:Access.ExchangeDetailsController
    {
        private TribalEntities _objContext = null;
        public ExchangeDetailsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }


        public override void Create(ExchangeDetail model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.ExchangeDetails.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a01c7518-11c5-43e8-b414-00bac0692yyz2 Error occur during Add Exchange Detail Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }


    }
}
