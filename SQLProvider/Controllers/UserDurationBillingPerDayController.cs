﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;


namespace SQLProvider.Controllers
{
    public class UserDurationBillingPerDayController :Access.UserDurationBillingPerDayController
    {
        private TribalEntities _objContext = null;
        public UserDurationBillingPerDayController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Update(UserDurationBillingPerDay objUserDurationBillingPerDay, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.UserDurationBillingPerDays.FirstOrDefault(I => I.Id == objUserDurationBillingPerDay.Id);
                if (objEntity != null)
                {
                    objEntity.IsActive = objUserDurationBillingPerDay.IsActive;
                    objEntity.Spent = objUserDurationBillingPerDay.Spent;
                    objEntity.Avaliable = objUserDurationBillingPerDay.Avaliable;
                    objEntity.HQSpent = objUserDurationBillingPerDay.HQSpent;
                    objEntity.HQAvaliable = objUserDurationBillingPerDay.HQAvaliable;
                    objEntity.Updated = objUserDurationBillingPerDay.Updated;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"a082c061-4205-413b-bfb0-6b5db2ead269 Error occur when update User Duration Billing Per Day in function ",ex);
            }
        }

        public override void Create(UserDurationBillingPerDay objUserBilling, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.UserDurationBillingPerDays.Add(objUserBilling);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"92643a7c-9e89-494e-993c-0ef14cd71805 Error occur during create user billing in function", ex);
            }
        }
    }
}
