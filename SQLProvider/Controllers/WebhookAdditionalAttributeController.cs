﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class WebhookAdditionalAttributeController
    {
        private TribalEntities _objContext = null;
        public WebhookAdditionalAttributeController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public PaymentwebhookadditionalAttribute Get(Expression<Func<PaymentwebhookadditionalAttribute, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            PaymentwebhookadditionalAttribute entity = null;
            try
            {
                entity = _objContext.PaymentwebhookadditionalAttributes.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ec99ac7d-cb51-416d-bcbd-1f6087b9c03d Error occur during get payment webhook additional attribute in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<PaymentwebhookadditionalAttribute> GetAll(Expression<Func<PaymentwebhookadditionalAttribute, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<PaymentwebhookadditionalAttribute> paymentWebhookAddtionalAttributes = null;
            try
            {
                paymentWebhookAddtionalAttributes = _objContext.PaymentwebhookadditionalAttributes.Where(expression).ToList();
                if (paymentWebhookAddtionalAttributes != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c270fe32-43b9-4c7b-b917-dac2fa21c0ea Error occur during get all payments webhook additional attributes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return paymentWebhookAddtionalAttributes;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"f376b18f-d10f-4556-a731-0a8ed3eeda81 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(PaymentwebhookadditionalAttribute paymentWebhookAddtionalAttribute, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PaymentwebhookadditionalAttributes.Add(paymentWebhookAddtionalAttribute);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c978e9fa-759d-4ab3-ad46-681fd299285d Error occur during Create Payment Webhook Additional Attribute in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(PaymentwebhookadditionalAttribute paymentWebhookAddtionalAttribute, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.PaymentwebhookadditionalAttributes.FirstOrDefault(I => I.Id == paymentWebhookAddtionalAttribute.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d878c182-7609-45ea-aced-f4bb1083fd85 Error occur when Updating Payment Webhook Additional Attribute {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
