﻿using Spacegene.Logger;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class ExcludedListController
    {
        private TribalPaymentEntities _objContext = null;
        public ExcludedListController(TribalPaymentEntities objContext)
        {
            _objContext = objContext;
        }

        public ExcludedList Get(Expression<Func<ExcludedList, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            ExcludedList entity = null;
            try
            {
                entity = _objContext.ExcludedLists.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c29d3bd2-2408-4103-a75a-be8a1d39eb87 Error occur during get ExcludedList in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<ExcludedList> GetAll(Expression<Func<ExcludedList, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<ExcludedList> objExcludedList = null;
            try
            {
                objExcludedList = _objContext.ExcludedLists.Where(expression).ToList();
                if (objExcludedList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"acd4a7bf-40c6-4393-b886-dd8d7188b4d2 Error occur during get all ExcludedList in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objExcludedList;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1b86a354-e09f-4d6f-9f9e-fb0758f8e84b Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(ExcludedList excludedList, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.ExcludedLists.Add(excludedList);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"e0c43444-60f8-4a38-8853-67461a5c38d6 Error occur during Create ExcludedList in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(ExcludedList excludedList, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.ExcludedLists.FirstOrDefault(I => I.Id == excludedList.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5d83eee9-5dfb-4045-b945-70aeff0b7de9 Error occur when Update ExcludedList Operation {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
