﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class PaymentFeesController
    {
        private TribalEntities _objContext = null;
        public PaymentFeesController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public PaymentFee Get(Expression<Func<PaymentFee, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            PaymentFee entity = null;
            try
            {
                entity = _objContext.PaymentFees.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"bf9789b6-64d7-46e2-8850-390e8d2f1bda Error occur during get Payment Fee in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<PaymentFee> GetAll(Expression<Func<PaymentFee, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<PaymentFee> paymentFees = null;
            try
            {
                paymentFees = _objContext.PaymentFees.Where(expression).ToList();
                if (paymentFees != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1d574566-e15e-43f6-9d2c-e2647b760448 Error occur during get all payment fees in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return paymentFees;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"916e53cb-b0e3-474d-8106-582df3d48c33 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(PaymentFee paymentFee, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PaymentFees.Add(paymentFee);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"fca0691d-4c91-45a4-9133-1baefb6a2ff6 Error occur during Create Payment Fee in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(PaymentFee paymentFee, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.PaymentFees.FirstOrDefault(I => I.Id == paymentFee.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("08db813a-c816-4b2b-9a24-066ec51501b0 Error occur when Updating payment fee {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
