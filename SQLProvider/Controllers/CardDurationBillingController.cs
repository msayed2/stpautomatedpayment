﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;
namespace SQLProvider.Controllers
{
    public class CardDurationBillingController : Access.CardDurationBillingController
    {
        private TribalEntities _objContext = null;
        public CardDurationBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override CardDurationBilling Get(Func<CardDurationBilling, bool> expression, out bool? dbStatus)
        {
            CardDurationBilling objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.CardDurationBillings.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("b6d559d8-f3ac-42b1-b12b-e8814bc416e3 Error occur during Get Card Duration Billing in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public override void Update(CardDurationBilling objCardDurationBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CardDurationBillings.FirstOrDefault(I => I.Id == objCardDurationBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = objCardDurationBilling.Avaliable;
                    objEntity.Spent = objCardDurationBilling.Spent;
                    objEntity.HQSpent = objCardDurationBilling.HQSpent;
                    objEntity.HQAvaliable = objCardDurationBilling.HQAvaliable;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"0ad945dd-c9ab-40bd-bc64-6d7cd75e1993 Error occur when update user Duration billing in function ", ex);
            }
        }
        public override void Create(CardDurationBilling objCardDurationBilling, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CardDurationBillings.Add(objCardDurationBilling);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"173893fe-40df-49a7-a8b7-d8bd892654dfa9 Error occur during create  card Duration billing in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override void CreateCardDurationSetting(CardDurationSetting objCardDurationSetting, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CardDurationSettings.Add(objCardDurationSetting);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1748ds55b-4150-4fd9a7-a8b7-d8bd890f4859 Error occur during create card Duration setting in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }
    }
}
