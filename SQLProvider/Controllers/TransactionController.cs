﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;



namespace SQLProvider.Controllers
{
    public class TransactionController : Access.TransactionController
    {
        private TribalEntities _objContext = null;
        public TransactionController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override Transaction Get(Expression<Func<Transaction, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Transaction objTransaction = null;
            try
            {
                objTransaction = _objContext.Transactions.FirstOrDefault(expression);
                if (objTransaction != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9f61ebd7-eb87-44f8-babe-fb0e7f6e6ffa Error occur when get Transaction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objTransaction;
        }
        public List<Merchant> GetAllMerchant(Expression<Func<Merchant, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Merchant> objMerchants = null;
            try
            {
                if (expression != null)
                {
                    objMerchants = _objContext.Merchants.Where(expression).ToList();
                }
                else
                {
                    objMerchants = _objContext.Merchants.ToList();
                }
                if (objMerchants != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("2c79ddbc-23ec-4e3f-a697-441e4b0cf490 Error occur when get Merchant in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objMerchants;
        }
        public List<Merchant> GetAllMerchantAsNoTracking(Expression<Func<Merchant, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Merchant> objMerchants = null;
            try
            {
                if (expression != null)
                {
                    objMerchants = _objContext.Merchants.AsNoTracking().Where(expression).Select(I => new Merchant { Id = I.Id, CleanedMerchantName = I.CleanedMerchantName }).ToList();
                }
                else
                {
                    objMerchants = _objContext.Merchants.AsNoTracking().ToList().Select(I => new Merchant { Id = I.Id, CleanedMerchantName = I.CleanedMerchantName }).ToList();
                }
                if (objMerchants != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("2c79ddbc-23ec-4e3f-a697-441e4b0cf490 Error occur when get Merchant in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objMerchants;
        }
        public override List<Transaction> GetAll(int skip, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Transaction> objTransactions = null;
            try
            {
                objTransactions = _objContext.Transactions.OrderBy(I => I.CreatedAt).Skip(skip).Take(take).ToList();
                if (objTransactions != null && objTransactions.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("beb67c23-cf68-4f87-aa59-d91cfab5bb8b Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objTransactions;
        }
        public override List<Transaction> GetAll(Expression<Func<Transaction, bool>> expression, int skip, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Transaction> objTransactions = null;
            try
            {
                objTransactions = new List<Transaction>();
                if (take == 0)
                {
                    objTransactions = _objContext.Transactions.Where(expression).OrderByDescending(group => group.CreatedAt).ToList();
                }
                else
                {
                    objTransactions = _objContext.Transactions.Where(expression).OrderByDescending(group => group.CreatedAt).Skip(skip).Take(take).ToList();
                }
                /*  if (take == 0)
                  {
                      objTransactions = _objContext.Transactions.Where(expression).GroupBy(I => I.StripeId)
                                                    .Select(group =>
                                                          new
                                                          {
                                                              Id = group.Key,
                                                              transaction = group.OrderByDescending(x => x.CreatedAt).FirstOrDefault()
                                                          })
                                                    .OrderByDescending(group => group.transaction.CreatedAt).Select(T => T.transaction).ToList();
                  }
                  else
                  {
                      objTransactions = _objContext.Transactions.Where(expression).GroupBy(I => I.StripeId)
                                                    .Select(group =>
                                                          new
                                                          {
                                                              Id = group.Key,
                                                              transaction = group.OrderByDescending(x => x.CreatedAt).FirstOrDefault()
                                                          })
                                                    .OrderByDescending(group => group.transaction.CreatedAt).Skip(skip).Take(take).Select(T => T.transaction).ToList();

                  }
                  */
                if (objTransactions != null && objTransactions.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)

            {
                new Logger().LogError(string.Format("53189152-ba57-484e-a760-b05a0aa4fb9b Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objTransactions;
        }
        public override void Create(Transaction objTransaction, out bool? dbStatus)
        {

            new Logger().LogWarning($"Creating new trabsaction: {objTransaction.Id}");

            dbStatus = null;
            try
            {
                var entity = _objContext.Transactions.Add(objTransaction);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                    new Logger().LogWarning($"Trabsaction with Id: {objTransaction.Id} has been added");
                }
                else
                {
                    new Logger().LogWarning($"Trabsaction with Id: {objTransaction.Id} was not added");
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9c252ecf-08e9-4ba9-b57a-ba3dce72eff7 Error occur during Create Card in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public Merchant GetMerchant(Expression<Func<Merchant, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Merchant objMerchant = null;
            try
            {
                objMerchant = _objContext.Merchants.FirstOrDefault(expression);
                if (objMerchant != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("2c79ddbc-23ec-4e3f-a697-441e4b0cf490 Error occur when get Merchant in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objMerchant;
        }
        public void CreateMerchant(Merchant objMerchant, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Merchants.Add(objMerchant);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("92641966-dcec-4ce2-94e5-b649d0189e9e Error occur during Create Card in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public void Save(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e621235c-a05c-4a20-9415-72f9f3c19181 Error occur during Save data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public List<TransactionState> GetStates(out bool? dbStatus)
        {
            dbStatus = null;
            List<TransactionState> objResult = null;
            try
            {
                objResult = _objContext.TransactionStates.ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5c58fcb6-4259-4c1c-aa91-94b1403c757f Error occur when get Transaction States in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

        public void UpdateTransactionStatus(string transaction_id, bool active, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Transactions.FirstOrDefault(I => I.Id == transaction_id);
                if (objEntity != null)
                {
                    objEntity.IsActive = active;
                    _objContext.SaveChanges();
                    dbStatus = true;
                    new Logger().LogWarning($"Transaction with Id: {transaction_id} has new state: {active}");
                }
                else
                {

                    dbStatus = false;
                    new Logger().LogWarning($"Faile to update Transaction with Id: {transaction_id} to new state: {active}");
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("47c9ba89-5ddf-422a-8cdd-9670c575f1e3 Error occur when updating transaction state in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override int GetTotalTransactionsCount(Expression<Func<Transaction, bool>> expression, out bool? status)
        {
            {
                status = null;
                int result = -1;
                try
                {
                    result = _objContext.Transactions.Where(expression).GroupBy(I => I.StripeId).Count();
                    status = (result > 0) ? true : false;
                }
                catch (Exception ex)
                {
                    new Logger().LogError(string.Format("a4c02d26-1799-429a-913c-15063dac6434 Exception when getting Transactions count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
                }
                return result;
            }
        }
        public override List<Transaction> GetTransactions(Expression<Func<Transaction,bool>> expression,out bool? dbStatus)
        {
            dbStatus = null;
            List<Transaction> objResult = null;
            try
            {
                objResult = _objContext.Transactions.Include("Card.Tags").Include("Card.AspNetUser.profiles.Company.CompanyDepartments").Include("Merchant").Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("4f6d28cf-87c5-450e-8681-fdd31cc82068 Exception when getting Transactions from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }
        public  List<Transaction> GetALLTransactions(Expression<Func<Transaction, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Transaction> objResult = null;
            try
            {
                objResult = _objContext.Transactions.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("41f33753-e353-4ccf-b39d-a0c1067f9576 Exception when getting Transactions from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }
        public List<Transaction> GetTransactionsAsNoTracking(Expression<Func<Transaction, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Transaction> objResult = null;
            try
            {
                if(expression == null)
                {
                    objResult = _objContext.Transactions.AsNoTracking().ToList();
                }
                else
                {
                    objResult = _objContext.Transactions.AsNoTracking().Where(expression).ToList();
                }
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d5ff7796-31ad-44b9-bc02-1e8a315e8c2a Exception when getting GetTransactionsAsNoTracking from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }

        public List<TransactionsInvoiceDetail> GetTransactionsInvoiceDetails(Expression<Func<TransactionsInvoiceDetail, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<TransactionsInvoiceDetail> objResult = null;
            try
            {
                objResult = _objContext.TransactionsInvoiceDetails.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("41f33753-e353-4ccf-b39d-a0c1067f9576 Exception when getting Transactions from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }

        public override void DeActivate(Func<Transaction, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Transactions.Where(expression).ToList();
                if (objEntity != null)
                {
                    objEntity.Select(obj => { obj.IsActive = false; return obj; }).ToList();
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ae68b72c-4efb-404e-b433-f348524fd842 Error occur when Delete Transaction in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }


        public List<Payment> GetALLWalletTransactions(Expression<Func<Payment, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Payment> objResult = null;
            try
            {
                objResult = _objContext.Payments.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("41f33fd-e33323-4ccf-b39d-a0cefdf9576 Exception when getting Wallet Transactions from database in GetALLWalletTransactions", ex);
            }

            return objResult;
        }

    }
}