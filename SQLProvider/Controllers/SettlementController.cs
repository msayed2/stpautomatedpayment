﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
   public class SettlementController:Access.SettlementController
   {
        private TribalEntities _objContext = null;
        public SettlementController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public void CreateSettlement(Settlement model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.Settlements.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("88697603-aca6-4344-80b9-1c970fdfccbc Error occur during Add Settlement Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public void CreatePendingSettlement(PendingSettlement model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.PendingSettlements.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("88697603-aca6-4344-80b9-1c970fdfccbc Error occur during Add Pending Settlement Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public void CreateSettlementInvoice(InvoiceCurrenciesSettlement model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.InvoiceCurrenciesSettlements.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("88697603-aca6-4344-80b9-1c970fdfccbc Error occur during Add Settlement Invoice Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override List<SettlementType> GetSettlementTypes(Func<SettlementType, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<SettlementType> objAttachmentType = null;
            try
            {
                objAttachmentType = _objContext.SettlementTypes.Where(expression).ToList();
                if (objAttachmentType != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4fcd-8249-b752eacb269e Error occur when get Attachement types in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objAttachmentType;
        }

        public Settlement Get(Func<Settlement, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Settlement objSettlement = null;
            try
            {
                objSettlement = _objContext.Settlements.Where(expression).FirstOrDefault();
                if (objSettlement != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4sdsdfcd-8249-b752eacbsds269e Error occur when get Settlement in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objSettlement;
        }

        public List<Settlement> GetAll(Func<Settlement, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Settlement> entity = new List<Settlement>();
            try
            {
                entity = _objContext.Settlements.Where(expression).ToList();
                dbStatus = true;

            }
            catch (Exception ex)
            {
                new Logger().LogError($"e03415eb-0df5-4e40-af06ss-25ee8ef250d0 Error occur during get all MoneyIN Data in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

    }
}
