﻿using Spacegene.Logger;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class CompanyPaymentInfoController
    {
        private TribalPaymentEntities _objContext = null;
        public CompanyPaymentInfoController(TribalPaymentEntities objContext)
        {
            _objContext = objContext;
        }

        public CompanyPaymentInfo Get(Expression<Func<CompanyPaymentInfo, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyPaymentInfo entity = null;
            try
            {
                entity = _objContext.CompanyPaymentInfoes.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3bf83dad-d058-4ae2-96bb-e14ab0537f94 Error occur during get CompanyPaymentInfo in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<CompanyPaymentInfo> GetAll(Expression<Func<CompanyPaymentInfo, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<CompanyPaymentInfo> companyPaymentInfo = null;
            try
            {
                companyPaymentInfo = _objContext.CompanyPaymentInfoes.Where(expression).ToList();
                if (companyPaymentInfo != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c19c1e7e-8009-40af-b21b-b7945044407f Error occur during get all CompanyPaymentInfo in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return companyPaymentInfo;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"131e7f7e-ca5e-4bdc-baaf-a2c68e692bfc Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(CompanyPaymentInfo companyPaymentInfo, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyPaymentInfoes.Add(companyPaymentInfo);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"65aa26e7-e948-408e-9020-097058f8beee Error occur during Create CompanyPaymentInfo in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(CompanyPaymentInfo companyPaymentInfo, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CompanyPaymentInfoes.FirstOrDefault(I => I.Id == companyPaymentInfo.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6f30e589-f56e-4889-8c47-aa81207efc77 Error occur when Updating CompanyPaymentInfo {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
