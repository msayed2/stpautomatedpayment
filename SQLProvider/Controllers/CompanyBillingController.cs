﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyBillingController : Access.CompanyBillingController
    {
        private TribalEntities _objContext = null;
        public CompanyBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        
        public override void Create(CompanyBilling objCompanyBilling, out bool? dbStatus, bool allowSaveChanges = true)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyBillings.Add(objCompanyBilling);
                if (entity != null)
                {
                    if (allowSaveChanges)
                    {
                        _objContext.SaveChanges();
                    }
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("57b44e98-afed-4d38-b5c9-07ac48f4bf6b Error occur during Create company billig in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override void Update(string id, double limit, double avaliable, double spent, out bool? dbStatus, bool allowSaveChanges = true)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyBillings.FirstOrDefault(I => I.Id == id);
                if (entity != null)
                {
                    entity.Limit = limit;
                    entity.Avaliable = avaliable;
                    entity.Spent = spent;
                    if (allowSaveChanges)
                    {
                        _objContext.SaveChanges();
                    }
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6ce0898c-8988-4378-a590-4bad49a3d6a0 Error occur during update company billig in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        
        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("2aca840a-6e73-472e-9cfd-0faadd816814 Error occur during SaveChanges Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override CompanyBilling Get(Func<CompanyBilling, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyBilling objCompanyBilling = null;
            try
            {
                objCompanyBilling = _objContext.CompanyBillings.FirstOrDefault(expression);
                if (objCompanyBilling != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"b5c36e91-72b6-49d5-b263-9da7a4b762c3 Error occur when get billing period in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objCompanyBilling;
        }

        public override List<CompanyBilling> GetAll(Func<CompanyBilling, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<CompanyBilling> objCompanyBillingList = new List<CompanyBilling>();
            try
            {
                objCompanyBillingList = _objContext.CompanyBillings.Where(expression).ToList();
                if (objCompanyBillingList != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"300b5b1c-e5cc-439e-8662-74136ffc3e1f Error occur when get list of billing period in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objCompanyBillingList;

        }
    }
}
