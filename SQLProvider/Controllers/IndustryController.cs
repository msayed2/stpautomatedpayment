﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;


namespace SQLProvider.Controllers
{
    public class IndustryController :Access.IndustryController
    {
        private TribalEntities _objContext = null;
        public IndustryController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override Industry Get(Func<Industry, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Industry objAttachment = null;
            try
            {
                objAttachment = _objContext.Industries.FirstOrDefault(expression);
                if (objAttachment != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"6a9f13ad-34a1-408b-aaf1-a2dff6e19cba Error occur during Get Industry Data in function", ex);
            }
            return objAttachment;
        }
    }
}
