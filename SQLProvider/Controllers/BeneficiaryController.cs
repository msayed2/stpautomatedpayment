﻿using Entity.Entities.TribalEntities;
using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SQLProvider.Controllers
{
    public class BeneficiaryController
    {
        private TribalEntities _objContext = null;
        public BeneficiaryController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public Beneficiary Get(Expression<Func<Beneficiary, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Beneficiary entity = null;
            try
            {
                entity = _objContext.Beneficiaries.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"33ee3779-0428-4a5c-94e0-fc48a9ecc8b7 Error occur during get Beneficiary in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<Beneficiary> GetAll(Expression<Func<Beneficiary, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<Beneficiary> beneficiaries = null;
            try
            {
                beneficiaries = _objContext.Beneficiaries.Where(expression).ToList();
                if (beneficiaries != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"986ea9c3-f168-4f5f-89be-651825e15469 Error occur during get all Beneficiaries in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return beneficiaries;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"f7d4824e-1251-4c25-9333-dc2da31a2551 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(Beneficiary beneficiaries, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Beneficiaries.Add(beneficiaries);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"64151369-90ba-4e40-b03e-15695cbfa8a3 Error occur during Create Beneficiary in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(Beneficiary beneficiaries, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Beneficiaries.FirstOrDefault(I => I.Id == beneficiaries.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("75ed83a3-638f-45c4-aeac-58a2a9795eea Error occur when Updating Beneficiary {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
