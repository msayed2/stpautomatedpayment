﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class ContactsController
    {
        private TribalEntities _objContext = null;
        public ContactsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public void CreateContactConfirmation(ContactConfirmation objConfirmation, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.ContactConfirmations.Add(objConfirmation);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("3f0e24c9-bf01-471d-b45b-4961230e91c7 Error occur when get all departments in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public ContactConfirmation GetContactConfirmation(Func<ContactConfirmation, bool> expression, out bool? dbStatus)
        {
            ContactConfirmation objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.ContactConfirmations.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("f22057df-abce-4064-977f-ae87b80a8706 Error occur during Get User in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public List<ContactConfirmation> GetContactConfirmations(Func<ContactConfirmation, bool> expression, out bool? dbStatus)
        {
            List<ContactConfirmation> objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.ContactConfirmations.Where(expression).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e68724e1-2e41-4c71-b9c8-bc17f1082bc0 Error occur during Get User in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }
        public void SaveChanges()
        {
            try
            {
                _objContext.SaveChanges();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("71f6c0cf-d712-43c7-9335-88bdb45c3524 Error occur during Saving cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }

    }
}
