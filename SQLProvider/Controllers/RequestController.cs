﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class RequestController : Access.RequestController
    {
        private TribalEntities _objContext = null;
        public RequestController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(ResetPasswordRequest objRequest, out bool? dbStatus)
        {

            dbStatus = null;
            try
            {
                var entity = _objContext.ResetPasswordRequests.Add(objRequest);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("0ccc6135-53bf-4047-a172-5b8a3919b0ab Error occur during Create Request in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override ResetPasswordRequest Get(Func<ResetPasswordRequest, bool> expression, out bool? dbStatus)
        {
            ResetPasswordRequest objResult = null;
            dbStatus = null;
            try
            {
                objResult = _objContext.ResetPasswordRequests.FirstOrDefault(expression);
                if (objResult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("3e6e91f8-bdcc-47e1-968a-e6203bf23693 Error occur during Get ResetPasswordRequest in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objResult;
        }

    }
}
