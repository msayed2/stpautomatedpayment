﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Access = SQLAccess.Controllers;
using Entity.Entities.TribalEntities;
using SQLProvider.TribalDB;
using Spacegene.Logger;
using System.Reflection;
using System.Linq.Expressions;

namespace SQLProvider.Controllers
{
    public class CompanyController : Access.CompanyController
    {
        private TribalEntities _objContext = null;
        public CompanyController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override List<Company> GetAll(int skip, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Company> objResult = null;
            try
            {
                if (take == 0)
                {
                    objResult = _objContext.Companies.Include("Profile").Include("Profiles.AspNetUser").Include("Profiles.AspNetUser.Cards").ToList();
                }
                else
                {
                    objResult = _objContext.Companies.Include("Profiles").Include("Profiles.AspNetUser").Include("Profiles.AspNetUser.Cards").OrderBy(I => I.Name).Skip(skip).Take(take).ToList();
                }
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("deec5e65-9841-49ab-bee0-fffacab7e376 Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }
        public override List<Company> GetAll(Func<Company, bool> expression, int skip, int take, out bool? dbStatus)
        {
            dbStatus = null;
            List<Company> objResult = null;
            try
            {
                
                objResult = _objContext.Companies.Where(expression).Skip(skip).Take(take).ToList();
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("99af0ad2-a5d8-43a8-987f-91760499bf51 Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }
        public  List<Company> GetAll(Func<Company, bool> expression,out bool? dbStatus)
        {
            dbStatus = null;
            List<Company> objResult = null;
            try
            {
                if (expression == null)
                {
                    objResult = _objContext.Companies.ToList();
                }
                else
                {
                    objResult = _objContext.Companies.Where(expression).ToList();
                }
                if (objResult != null && objResult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("c2e7c789-1c93-407f-9418-14a9d2b66ddb Error occur when get data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

            return objResult;
        }
        public override Company Get(Func<Company, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            Company objCompany = null;
            try
            {
                objCompany = _objContext.Companies.FirstOrDefault(expression);
                if (objCompany != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4fcd-8249-b752eacb269e Error occur when get company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCompany;
        }
        public  List<Currency> GetCurrencIes(Func<Currency, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List < Currency> objCurrencIes = null;
            try
            {
                objCurrencIes = _objContext.Currencies.Where(expression).ToList();
                if (objCurrencIes != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5ssdsd071-4fcd-8249-b752eacb269e Error occur when get company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCurrencIes;
        }
        public override void Create(Company objCompany, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.Companies.Add(objCompany);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("41c9605a-042c-4fe4-9525-3b4134f0f327 Error occur during Create Company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override void Update(Company objCompany, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Companies.FirstOrDefault(I => I.Id == objCompany.Id);
                if (objEntity != null)
                {
                    if (objCompany.Addresses != null)
                    {
                        _objContext.Addresses.RemoveRange(objEntity.Addresses);
                        _objContext.SaveChanges();
                    }
                    objEntity.Name = (objCompany.Name != null) ? objCompany.Name : objEntity.Name;
                    objEntity.Addresses = (objCompany.Addresses != null) ? objCompany.Addresses : objEntity.Addresses;
                    objEntity.UpdatedAt = objCompany.UpdatedAt;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("5f7d9e51-de1f-4da3-8735-8facb032dd01 Error occur when update Card in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override void Delete(Func<Company, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.Companies.FirstOrDefault(expression);
                if (objEntity != null)
                {
                    objEntity.IsActive = false;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ae68b72c-4efb-404e-b433-f348524fd842 Error occur when Delete Company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override int GetTotalCompaniesCount(Func<Company, bool> expression, out bool? status)
        {
            status = null;
            int result = -1;
            try
            {
                result = _objContext.Companies.Where(expression).Count();
                status = (result > 0) ? true : false;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("b5312333-2f59-4bb1-b51a-b96059079c3a Exception when getting companies count from database in {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return result;
        }
        public void CreateDepartment(CompanyDepartment objDepartment, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyDepartments.Add(objDepartment);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("37fdec44-633a-4d5c-bc7a-aa5c18837bd6 Error occur during Create Company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public CompanyDepartment GetDepartment(Func<CompanyDepartment, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyDepartment objDepartment = null;
            try
            {
                objDepartment = _objContext.CompanyDepartments.FirstOrDefault(expression);
                if (objDepartment != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6584a96f-e9f0-45bb-8c34-02bf15d5984d Error occur when get company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objDepartment;
        }
        public List<CompanyDepartment> GetDepartments(Func<CompanyDepartment, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<CompanyDepartment> objresult = null;
            try
            {
                objresult = _objContext.CompanyDepartments.Where(expression).ToList();

                if (objresult != null && objresult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }

            catch (Exception ex)
            {
                new Logger().LogError(string.Format("eee0715e-1c56-43ee-85eb-54ad8235868e Error occur when get all departments in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;
        }
        public List<CompanyBilling> GetCompanyBillings(Expression<Func<CompanyBilling, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<CompanyBilling> objresult = null;
            try
            {
                objresult = _objContext.CompanyBillings.Where(expression).OrderBy(B=>B.StartDate).ToList();

                if (objresult != null && objresult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("302da930-6b2f-4f25-a43a-64129dfbc264 Error occur when get all company billing in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;

        }
        public List<CardBilling> GetCardBillings(Expression<Func<CardBilling, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<CardBilling> objresult = null;
            try
            {
                objresult = _objContext.CardBillings.Where(expression).OrderBy(B => B.StartDate).ToList();

                if (objresult != null && objresult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("302da930-6b2f-4f25-a43a-64129dfbc264 Error occur when get all company billing in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;

        }
        public void CreateCompanyBilling (CompanyBilling objBilling,out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
               var entity = _objContext.CompanyBillings.Add(objBilling);
               
                if (entity !=null )
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("a5392ae9-5d21-4b1e-a302-faa120c1c56f Error occur when get all company billing in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public CompanyBilling GetBilling(Expression<Func<CompanyBilling, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyBilling objresult = null;
            try
            {
                objresult = _objContext.CompanyBillings.FirstOrDefault(expression);

                if (objresult != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("4613afa2-81b0-4693-a339-45c02ad67056 Error occur when get all departments in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;

        }

        public override void Update(CompanyBilling objCompanyBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CompanyBillings.FirstOrDefault(I => I.Id == objCompanyBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = objCompanyBilling.Avaliable;
                    objEntity.Spent = objCompanyBilling.Spent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("c3006d8d-be2a-4d17-930d-bea1c5c0ff5e Error occur when update company billing in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public List<DeviceLog> GetAllDeviceLogs(Expression<Func<DeviceLog,bool>> expression,out bool? dbStatus)
        {
            dbStatus = null;
            List<DeviceLog> objresult = null;
            try
            {
                objresult = _objContext.DeviceLogs.Where(expression).ToList();
                if (objresult != null && objresult.Count>0)
                {                  
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ecd0b69e-a789-4028-9f65-3966e3775faa Error occur when GetAll devices logs in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;
        }
        public List<CompanyContact> GetCompanyContacts(Expression<Func<CompanyContact, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<CompanyContact> objCompany = null;
            try
            {
                objCompany = _objContext.CompanyContacts.Where(expression).ToList();
                if (objCompany != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("37584af8-da65-47e6-8d3b-f69b1200409a Error occur when get company in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCompany;
        }
        public CompanyStatu GetCompanyStatus(Func<CompanyStatu, bool>expression,out bool? dbStatus)
        {
            dbStatus = null;
            CompanyStatu objCompanyStatus = null;
            try
            {
                objCompanyStatus = _objContext.CompanyStatus.FirstOrDefault(expression);
                if (objCompanyStatus != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("379ed1eb-7e8d-4893-bbc7-858f07c27478 Error occur when get company status in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objCompanyStatus;
        }
        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception e)
            {
                new Logger().LogError(string.Format("55618c53-a0d7-44c8-8ced-ca2e3bd48a67 Error occur when save company changes in function {0}", MethodBase.GetCurrentMethod().Name), e);
            }
        }

        public override CompanyCreditLine GetCompanyCreditLine(Func<CompanyCreditLine, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanyCreditLine objCompany = null;
            try
            {
                objCompany = _objContext.CompanyCreditLines.FirstOrDefault(expression);
                if (objCompany != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"d346e26b-5071-4fcd-8dsd249-b752eacb269e Error occur when get Company Credit Line  in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objCompany;
        }

        public override void DeActivateCreditLine(Func<CompanyCreditLine, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CompanyCreditLines.Where(expression).ToList();
                if (objEntity != null)
                {
                    objEntity.Select(obj => { obj.IsActive = false; obj.UpdatedAt = DateTime.UtcNow; return obj; }).ToList();
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ae68b72c-4efb-404e-b433-f348524fd842 Error occur when Delete Company Credit Line  in function{MethodBase.GetCurrentMethod().Name}",ex);
            }
        }
        public override void CreateCreditLine(CompanyCreditLine objCompany, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyCreditLines.Add(objCompany);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"41c9605a-042c-4fe4-9525-3b4134f0f327 Error occur during Create Company in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }
        public List<Industry> GetIndustries(Func<Industry, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<Industry> objresult = null;
            try
            {
                objresult = _objContext.Industries.Where(expression).ToList();

                if (objresult != null && objresult.Count > 0)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }

            catch (Exception ex)
            {
                new Logger().LogError(string.Format("eee07fdfde-1c56-43ee-85eb-54548fd5868e Error occur when get all Industries in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return objresult;
        }
        public void CreateCompanyIndustry(CompanyIndustry objCompanyIndustry, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanyIndustries.Add(objCompanyIndustry);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"17f6411e-37be-494a-bbe0-edbe8937b3f2 Error occur during Create Company Industry in function ", ex);
            }

        }

    }
}
