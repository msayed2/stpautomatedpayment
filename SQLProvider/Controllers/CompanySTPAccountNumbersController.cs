﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class CompanySTPAccountNumbersController
    {
        private TribalEntities _objContext = null;
        public CompanySTPAccountNumbersController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public CompanySTPAccountNumber Get(Expression<Func<CompanySTPAccountNumber, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            CompanySTPAccountNumber entity = null;
            try
            {
                entity = _objContext.CompanySTPAccountNumbers.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"2fda4032-25c9-4fab-805f-92be12183903 Error occur during get CompanySTPAccountNumber in function Get", ex);
            }
            return entity;
        }

        public List<CompanySTPAccountNumber> GetAll(Expression<Func<CompanySTPAccountNumber, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<CompanySTPAccountNumber> companySTPAccountNumbers = null;
            try
            {
                companySTPAccountNumbers = _objContext.CompanySTPAccountNumbers.Where(expression).ToList();
                if (companySTPAccountNumbers != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"ebfa883a-0696-4478-87ce-947b0873c5d7 Error occur during get all CompanySTPAccountNumbers in function GetAll", ex);
            }
            return companySTPAccountNumbers;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"4942c1d0-f048-40ec-870d-c9d1f25e866c Error occur during save changes in function SaveChanges", ex);
            }
        }

        public void Create(CompanySTPAccountNumber companySTPAccountNumber, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.CompanySTPAccountNumbers.Add(companySTPAccountNumber);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"896bca26-8a13-4cda-aa00-79484de88f16 Error occur during Create payment in function Create", ex);
            }
        }

        public void Update(CompanySTPAccountNumber companySTPAccountNumber, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.CompanySTPAccountNumbers.FirstOrDefault(I => I.Id == companySTPAccountNumber.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6c36e74d-bcd5-4fde-8682-8e6acb898309 Error occur when Updating payment in funciont Update"), ex);
            }
        }
    }
}
