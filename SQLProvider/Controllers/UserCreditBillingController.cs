﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class UserCreditBillingController : Access.UserCreditBillingController
    {
        private TribalEntities _objContext = null;
        public UserCreditBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Update(UserCreditBilling UserCreditBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.UserCreditBillings.FirstOrDefault(I => I.Id == UserCreditBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = UserCreditBilling.Avaliable;
                    objEntity.Spent = UserCreditBilling.Spent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"c3006d8d-be2a-4d17-930d-bea1c5c0ff5e Error occur when update user credit billing in function ", ex);
            }
        }
    }
}
