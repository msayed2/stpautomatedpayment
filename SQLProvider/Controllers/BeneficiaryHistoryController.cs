﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class BeneficiaryHistoryController
    {
        private TribalEntities _objContext = null;
        public BeneficiaryHistoryController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public BeneficiariesHistory Get(Expression<Func<BeneficiariesHistory, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            BeneficiariesHistory entity = null;
            try
            {
                entity = _objContext.BeneficiariesHistories.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"d685a629-d60c-4ae3-bd65-0675c611ef95 Error occur during get Beneficiary History in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<BeneficiariesHistory> GetAll(Expression<Func<BeneficiariesHistory, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<BeneficiariesHistory> beneficiariesHistory = null;
            try
            {
                beneficiariesHistory = _objContext.BeneficiariesHistories.Where(expression).ToList();
                if (beneficiariesHistory != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"4002ffae-99c2-4d2e-a520-c106fa968ea3 Error occur during get all Beneficiaries History in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return beneficiariesHistory;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"19810133-0731-4e9b-a0f0-b083e15f2936 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(BeneficiariesHistory beneficiariesHistory, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.BeneficiariesHistories.Add(beneficiariesHistory);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"44027f9b-ab4d-44bb-b240-8f2571c69a1c Error occur during Create Beneficiary History in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(BeneficiariesHistory beneficiariesHistory, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.BeneficiariesHistories.FirstOrDefault(I => I.Id == beneficiariesHistory.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("75ed83a3-638f-45c4-aeac-58a2a9795eea Error occur when Updating Beneficiary {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
