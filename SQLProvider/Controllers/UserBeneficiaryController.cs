﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
   public class UserBeneficiaryController
    {
        private TribalEntities _objContext = null;
        public UserBeneficiaryController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public UserBeneficiary Get(Expression<Func<UserBeneficiary, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            UserBeneficiary entity = null;
            try
            {
                entity = _objContext.UserBeneficiaries.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"cac965c3-9958-4e3c-930c-c2ecb33062c2 Error occur during get User Beneficiary in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<UserBeneficiary> GetAll(Expression<Func<UserBeneficiary, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<UserBeneficiary> userBeneficiary = null;
            try
            {
                userBeneficiary = _objContext.UserBeneficiaries.Where(expression).ToList();
                if (userBeneficiary != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"a750ea4b-2cec-4c49-8c2a-2a540e3e6ec9 Error occur during get all User Beneficiaries in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return userBeneficiary;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"8642cb3c-1dfe-43b5-9f0e-9dd14b650602 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(UserBeneficiary userBeneficiary, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.UserBeneficiaries.Add(userBeneficiary);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"304e42a3-781c-4b3a-bddf-5cc1de5621c1 Error occur during Create User Beneficiary in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(UserBeneficiary userBeneficiary, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.UserBeneficiaries.FirstOrDefault(I => I.Id == userBeneficiary.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("98b4a6a7-911d-4409-b5a3-a4d4bc1c2b53 Error occur when Updating User Beneficiary {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
