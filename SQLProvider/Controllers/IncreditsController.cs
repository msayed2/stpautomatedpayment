﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class IncreditsController: Access.IncreditsController
    {
        private TribalEntities _objContext = null;
        public IncreditsController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override List<InCredit> GetALL(Func<InCredit, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<InCredit> inCreditList = null;
            try
            {
                inCreditList = _objContext.InCredits.Where(expression).ToList();
                if (inCreditList != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4fcd-8249-b752eacb269e Error occur when get Incredits in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return inCreditList;
        }

        public void DeActivate(Func<InCredit, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.InCredits.Where(expression).ToList();
                if (objEntity != null)
                {
                    objEntity.Select(obj => { obj.IsActive = false; return obj; }).ToList();
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ae68b72c-4efb-404e-b433-f348524fd842 Error occur when Delete Incredit in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public void Create(InCredit model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.InCredits.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("52c8d282-e05c-4bbc-bc13-5d35c6b988a7 Error occur during Add Company Incredit Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("6dd4be3a-70e6-4f72-ad3f-144cbb5c34a4 Error occur during SaveChanges in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        public override void CreateBulk(List<InCredit> inCredits, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.InCredits.AddRange(inCredits);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("172a8e15-8aeb-47eb-b026-3e4a67b77385 Error occur during CreateBulk Incredit Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
        public override List<InCreditType> GetInCreditTypes (Func<InCreditType, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<InCreditType> inCreditTypeList = null;
            try
            {
                inCreditTypeList = _objContext.InCreditTypes.Where(expression).OrderBy(I=>I.Id).ToList();
                if (inCreditTypeList != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("d346e26b-5071-4fcd-8249-b752eacb269e Error occur when get Incredits in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return inCreditTypeList;
        }
    }
}
