﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLProvider.Controllers
{
    public class PaymentStatusController
    {
        private TribalEntities _objContext = null;
        public PaymentStatusController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public PaymentStatu Get(Expression<Func<PaymentStatu, bool>> expression, out bool? dbStatus)
        {
            dbStatus = null;
            PaymentStatu entity = null;
            try
            {
                entity = _objContext.PaymentStatus.FirstOrDefault(expression);
                if (entity != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"6edce816-00f9-42f3-b65e-ad2fc48915d1 Error occur during get Payment status in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public List<PaymentStatu> GetAll(Expression<Func<PaymentStatu, bool>> expression, out bool? dbStatus)
        {
            dbStatus = false;
            List<PaymentStatu> paymentStatus = null;
            try
            {
                paymentStatus = _objContext.PaymentStatus.Where(expression).ToList();
                if (paymentStatus != null)
                {
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"0226e5b1-2827-4548-a390-862fa1fcc142 Error occur during get all payment status in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return paymentStatus;
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"140889fa-f13a-40eb-bf0a-3896e464bb16 Error occur during save changes in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Create(PaymentStatu paymentStatus, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var entity = _objContext.PaymentStatus.Add(paymentStatus);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"cffe47db-3ac0-493d-99a9-a3623257dd4e Error occur during Create Payment status in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public void Update(PaymentStatu paymentStatus, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.PaymentStatus.FirstOrDefault(I => I.Id == paymentStatus.Id);
                if (objEntity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
                else
                {
                    dbStatus = false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("8dc1c866-c6e8-4ab2-a50e-08e48912c9d7 Error occur when Updating payment status {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
