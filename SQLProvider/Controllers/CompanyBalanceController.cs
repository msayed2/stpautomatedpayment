﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class CompanyBalanceController:Access.CompanyBalanceController
    {
        private TribalEntities _objContext = null;
        public CompanyBalanceController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public void CreateCompanyBalance(CompanyBalance model, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.CompanyBalances.Add(model);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("52c8d282-e05c-4bbc-bc13-5d35c6b988a7 Error occur during Add Company Balance Data in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        // Get Company Balance 
        public override CompanyBalance Get(Func<CompanyBalance, bool> expression, out bool? dbStatus)
        {
            dbStatus = false;
            CompanyBalance entity = new CompanyBalance();
            try
            {
                entity = _objContext.CompanyBalances.FirstOrDefault(expression);
                dbStatus = true;

            }
            catch (Exception ex)
            {
                new Logger().LogError($"e341b4d4-7448-46f9-8561-ffcda995bbfb Error occur during get Company Balance Data in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return entity;
        }

        public override void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"644-a893b-e519-46ba-918a-1919cbbd83e3 Error occur during SaveChanges in function {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }


    }
}
