﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;
namespace SQLProvider.Controllers
{
    public class IntegrationSettingController : Access.IntegrationSettingController
    {
        private TribalEntities _objContext = null;
        public IntegrationSettingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
       

        public override List<IntegrationSetting> GetALL(Func<IntegrationSetting, bool> expression, out bool? dbStatus)
        {
            dbStatus = null;
            List<IntegrationSetting> integrationSettings = null;
            try
            {
                if (expression != null)
                {
                    integrationSettings = _objContext.IntegrationSettings.Where(expression).ToList();
                }
                else
                {
                    integrationSettings = _objContext.IntegrationSettings.ToList();
                }
                if (integrationSettings != null)
                {
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"cb6dbac6-04a2-47eb-869b-6b734292432d Error occur when get all Integration Setting in function {MethodBase.GetCurrentMethod().Name } ", ex);
            }
            return integrationSettings;
        }
    }
}
