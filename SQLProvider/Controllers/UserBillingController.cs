﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class UserBillingController : Access.UserBillingController
    {
        private TribalEntities _objContext = null;
        public UserBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }
        public override void Create(UserBilling objUserBilling, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.UserBillings.Add(objUserBilling);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1738955b-4090-49a7-a8b7-d8bd890f72a9 Error occur during create  user billing in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override void CreateBulk(List<UserBilling> userBillings, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.UserBillings.AddRange(userBillings);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c92c4e02-81f4-4c89-8bf5-45da42579438 Error occur during create bulk user billings in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }
        public override void Update(UserBilling objUserBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.UserBillings.FirstOrDefault(I => I.Id == objUserBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = objUserBilling.Avaliable;
                    objEntity.Spent = objUserBilling.Spent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("38ca5ff9-16de-4639-a8e6-9c49c2dab96d Error occur when update user billing in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
        }
    }
}
