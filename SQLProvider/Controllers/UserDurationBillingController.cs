﻿using Spacegene.Logger;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Access = SQLAccess.Controllers;

namespace SQLProvider.Controllers
{
    public class UserDurationBillingController : Access.UserDurationBillingController
    {
        private TribalEntities _objContext = null;
        public UserDurationBillingController(TribalEntities objContext)
        {
            _objContext = objContext;
        }

        public override void Create(UserDurationBilling objUserDurationBilling, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.UserDurationBillings.Add(objUserDurationBilling);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1738955b-4090-49a7-a8b7-d8bd890f72a9 Error occur during create  user Duration billing in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override UserDurationBilling GetUserDurationBilling(string profile_id)
        {
            UserDurationBilling objUserDurationBilling = null;
            try
            {
                objUserDurationBilling = _objContext.UserDurationBillings.FirstOrDefault(s => s.ProfileId == profile_id);
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1738955b-4090-49a7-a8b7-d8bd890f72a9 Error occur during Get Spent in user Duration billing in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objUserDurationBilling;
        }

        public override void Update(UserDurationBilling objUserDurationBilling, out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                var objEntity = _objContext.UserDurationBillings.FirstOrDefault(I => I.Id == objUserDurationBilling.Id);
                if (objEntity != null)
                {
                    objEntity.Avaliable = objUserDurationBilling.Avaliable;
                    objEntity.Spent = objUserDurationBilling.Spent;
                    objEntity.HQAvaliable = objUserDurationBilling.HQAvaliable;
                    objEntity.HQSpent = objUserDurationBilling.HQSpent;
                    _objContext.SaveChanges();
                    dbStatus = true;
                }

            }
            catch (Exception ex)
            {
                new Logger().LogError($"0ad945dd-c9ab-40bd-bc64-6d7cd75e1993 Error occur when update user Duration billing in function ", ex);
            }
        }

        public void SaveChanges(out bool? dbStatus)
        {
            dbStatus = null;
            try
            {
                _objContext.SaveChanges();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("e45d4e02-b49a-4c15-b99e-1c4823835c7a Error occur during Saving cards in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }

        }
        public override void CreateUserDurationSetting(UserDurationSetting objUserDurationSetting, out bool? dbStatus)
        {
            dbStatus = false;
            try
            {
                var entity = _objContext.UserDurationSettings.Add(objUserDurationSetting);
                if (entity != null)
                {
                    _objContext.SaveChanges();
                    dbStatus = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1748d55b-4150-49a7-a8b7-d8bd890f869 Error occur during create  user Duration setting in function { MethodBase.GetCurrentMethod().Name}", ex);
            }
        }
    }
}
