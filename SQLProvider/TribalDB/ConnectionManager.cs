﻿using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLProvider.TribalDB
{
    public class ConnectionManager
    {
        private TribalEntities _objContext = null;
        private DbContextTransaction _tx;

        public TribalEntities Open(bool useTx = false)
        {
            try
            {
                if (_objContext == null)
                {
                    _objContext = new TribalEntities();
                }

                if (_objContext.Database.Connection.State != System.Data.ConnectionState.Open)
                {
                    _objContext.Database.Connection.Open();
                    if (useTx)
                    {
                        _tx = _objContext.Database.BeginTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"7e2074a2-1d44-4f64-99cb-9786f53b8490 fail to initiate connection with {_objContext.GetType().ToString()}  db Details : ", ex);
                return null;
            }
            return _objContext;
        }
        public void Close()
        {
            try
            {
                if (_objContext != null)
                {
                    _objContext.Database.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3c908aaa-6f1b-4a9c-a1de-c02aec336227 fail to close connection with db {_objContext.GetType().ToString()} Details : ", ex);
            }
        }
        public bool HandleTx(bool success)
        {
            bool result = false;
            try
            {
                if (success)
                {
                    _tx.Commit();
                }
                Close();
                result = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"b3b66c24-3264-4f66-bd73-0dc7f9249d96 fail to handle transaction in {this.GetType().ToString()} Details : ", ex);
            }
            return result;
        }
        public TribalEntities GetContext()
        {
            return _objContext;
        }
        public DbContextTransaction GetTransaction()
        {
            return _tx = _objContext.Database.BeginTransaction();
        }
        public void Commit(DbContextTransaction objTransaction,out bool dbStatus)
        {
            dbStatus = false;
            try
            {
                objTransaction.Commit();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"4c1c1ef5-50a1-46bf-b8e2-f027c35b5b25 Fail to Commit changes to db : ", ex);
            }
        }
        public void RollBack(DbContextTransaction objTransaction)
        {
            try
            {
                objTransaction.Rollback();
            }
            catch (Exception ex)
            {
                new Logger().LogError($"87a064c1-a96d-4e3e-b666-098065f5fa8f Fail to Rollback changes from db : ", ex);
            }
        }

    }
}