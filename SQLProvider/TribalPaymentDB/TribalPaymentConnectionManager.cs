﻿using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLProvider.TribalPaymentDB
{
    public class TribalPaymentConnectionManager
    {
        private TribalPaymentEntities _objContext = null;
        private DbContextTransaction _tx;

        public TribalPaymentEntities Open(bool useTx = false)
        {
            try
            {
                if (_objContext == null)
                {
                    _objContext = new TribalPaymentEntities();
                }

                if (_objContext.Database.Connection.State != System.Data.ConnectionState.Open)
                {
                    _objContext.Database.Connection.Open();
                    if (useTx)
                    {
                        _tx = _objContext.Database.BeginTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"2c2a92a0-a730-4b68-be37-4286e70ca1a1 fail to initiate connection with {_objContext.GetType().ToString()}  db Details : ", ex);
                return null;
            }
            return _objContext;
        }
        public void Close()
        {
            try
            {
                if (_objContext != null)
                {
                    _objContext.Database.Connection.Close();
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c6f2f051-904e-4002-9e02-42efa259163e fail to close connection with db {_objContext.GetType().ToString()} Details : ", ex);
            }
        }
        public bool HandleTx(bool success)
        {
            bool result = false;
            try
            {
                if (success)
                {
                    _tx.Commit();
                }
                Close();
                result = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c728a79a-a3b4-4f02-9f40-cea238dc7188 fail to handle transaction in {this.GetType().ToString()} Details : ", ex);
            }
            return result;
        }
        public TribalPaymentEntities GetContext()
        {
            return _objContext;
        }
        public DbContextTransaction GetTransaction()
        {
            return _tx = _objContext.Database.BeginTransaction();
        }
        public void Commit(DbContextTransaction objTransaction, out bool dbStatus)
        {
            dbStatus = false;
            try
            {
                objTransaction.Commit();
                dbStatus = true;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"4edd4dc8-5990-4851-90c7-233cb4a6dbb7 Fail to Commit changes to db : ", ex);
            }
        }
        public void RollBack(DbContextTransaction objTransaction)
        {
            try
            {
                objTransaction.Rollback();
            }
            catch (Exception ex)
            {
                new Logger().LogError($"c5841293-1404-44ec-852d-74e83583f75b Fail to Rollback changes from db : ", ex);
            }
        }
    }
}
