﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class AttachmentDetailController
    {
        public abstract void Create(AttachmentDetail model, out bool? dbStatus);
    }
}
