﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class TargetLimitController
    {
        public abstract void CreateBulk(List<TargetLimit> targetLimits, out bool? dbStatus,bool allowSaveChanges=true);
        public abstract void Create(TargetLimit objTargetLimit, out bool? dbStatus, bool allowSaveChanges = true);
        public abstract List<TargetLimit> GetAll(Expression<Func<TargetLimit, bool>> expression, out bool? dbStatus);
        public abstract void DeActivate(Func<TargetLimit, bool> expression, out bool? dbStatus);
        public abstract TargetLimit Get(Func<TargetLimit, bool> expression, out bool? dbStatus);
    }
}
