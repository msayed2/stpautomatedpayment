﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CompanyBillingController
    {
        public abstract void Create(CompanyBilling objCompanyBilling, out bool? dbStatus,bool allowSaveChanges=true);
        public abstract void Update(string id,double limit,double avaliable,double spent, out bool? dbStatus, bool allowSaveChanges = true);
        public abstract CompanyBilling Get(Func<CompanyBilling, bool> expression, out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
        public abstract List<CompanyBilling> GetAll(Func<CompanyBilling, bool> expression, out bool? dbStatus);
    }
}
