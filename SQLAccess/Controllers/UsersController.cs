﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class UsersController
    {
        public abstract List<AspNetUser> GetAll(int skip, int take, out bool? dbStatus);
        public abstract List<AspNetUser> GetAll(Func<AspNetUser, bool> expression, int skip, int take, out bool? dbStatus);
        public abstract List<AspNetUser> GetAll(Func<AspNetUser, bool> expression, out bool? dbStatus);
        public abstract AspNetUser Get(Func<AspNetUser, bool> expression, out bool? dbStatus);
        public abstract void Delete(Func<AspNetUser, bool> expression, out bool? dbStatus);
        public abstract int GetTotalUsersCount(Func<AspNetUser, bool> expression, out bool? status);
        public abstract void Save(out bool? dbStatus);
        public abstract List<Device> GetTrustedDevices(Func<Device, bool> expression, out bool? dbStatus);
        public abstract CompanyContact Create(Func<CompanyContact, bool> expression, CompanyContact model, out bool? dbStatus);
        public abstract List<AspNetUser> GetMostActiveUsers(string user_id, DateTime start_at, DateTime end_at, int take, out bool? status);
        public abstract void DeleteBilling(Func<CompanyContact, bool> expression, out bool? dbStatus);
        public abstract void DeleteSuperAdmin(Func<Profile, bool> expression, out bool? dbStatus);
        public abstract List<CompanyContact> GetAllBillingContacts(string company_id, out bool? dbStatus);
        public abstract List<AspNetUser> GetAdmins(string company_id, int CurrentTypeId, out bool? dbStatus);
        public abstract List<AspNetUser> GetNonSuperAdminsUsers(string user_id, out bool? dbStatus);
        public abstract double GetUserLimit(string user_id, out bool? dbStatus);
    }
}
