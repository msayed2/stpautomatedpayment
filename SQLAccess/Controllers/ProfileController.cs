﻿namespace SQLAccess.Controllers
{
    using System;
    using System.Collections.Generic;
    using Entity.Entities.TribalEntities;

    /// <summary>
    /// Defines the <see cref="ProfileController" />
    /// </summary>
    public abstract class ProfileController
    {
        /// <summary>
        /// The Create
        /// </summary>
        /// <param name="objProfile">The objProfile<see cref="Profile"/></param>
        /// <param name="dbStatus">The dbStatus<see cref="bool?"/></param>
        public abstract void Create(Profile objProfile, out bool? dbStatus);

        /// <summary>
        /// The GetVerification
        /// </summary>
        /// <param name="expression">The expression<see cref="Func{DeviceLogin, bool}"/></param>
        /// <param name="dbStatus">The dbStatus<see cref="bool?"/></param>
        /// <returns>The <see cref="DeviceLogin"/></returns>
        public abstract DeviceLogin GetVerification(Func<DeviceLogin, bool> expression, out bool? dbStatus);

        /// <summary>
        /// The Update
        /// </summary>
        /// <param name="objDeviceLogin">The objDeviceLogin<see cref="DeviceLogin"/></param>
        /// <param name="dbStatus">The dbStatus<see cref="bool?"/></param>
        public abstract void Update(DeviceLogin objDeviceLogin, out bool? dbStatus);

        /// <summary>
        /// The Get
        /// </summary>
        /// <param name="expression">The expression<see cref="Func{Profile, bool}"/></param>
        /// <param name="dbStatus">The dbStatus<see cref="bool?"/></param>
        /// <returns>The <see cref="Profile"/></returns>
        public abstract Profile Get(Func<Profile, bool> expression, out bool? dbStatus);
        
        
        /// <summary>
        /// The Update
        /// </summary>
        /// <param name="expression">The expression<see cref="Func{Profile,bool}"/></param>
        /// <param name="objProfile">The objProfile<see cref="Profile"/></param>
        /// <param name="dbStatus">The dbStatus<see cref="bool?"/></param>
        public abstract void Update(Func<Profile, bool> expression, Profile objProfile, out bool? dbStatus);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="dbStatus"></param>
        /// <returns></returns>
        public abstract List<Profile> GetAll(Func<Profile, bool> expression, out bool? dbStatus);
    }
}
