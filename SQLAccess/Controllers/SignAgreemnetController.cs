﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class SignAgreemnetController
    {
        public abstract List <Document> GetAllDocument(Func<Document, bool> expression, out bool? dbStatus);
        public abstract void Create(SignAgreementPendingAction objSignAgreementPendingAction,  out bool? dbStatus);
        public abstract List<SignAgreementPendingAction> Get(Func<SignAgreementPendingAction, bool> expression, out bool? dbStatus);
        public abstract SignAgreementPendingAction GetById(string id, out bool? dbStatus);
    }
}
