﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CardsController
    {
        public abstract List<Card> GetAll(int skip, int take, out bool? dbStatus);
        public abstract List<Card> GetAll(Func<Card, bool> expression, int skip, int take, out bool? dbStatus);
        public abstract Card Get(Func<Card, bool> expression, out bool? dbStatus);
        public abstract void RemoveTags(ICollection<Tag> tags, out bool? dbStatus);
        public abstract void Update(Profile profile,Func<Card, bool> expression, int status_id, out bool? dbStatus);
        public abstract void Create(Profile profile,Card objCard, out bool? dbStatus);
        public abstract void Delete(Func<Card, bool> expression, out bool? dbStatus);
        public abstract int GetTotalCardsCount(Func<Card, bool> expression, out bool? status);
        public abstract List<CardType> GetTypes(out bool? status);
    }
}
