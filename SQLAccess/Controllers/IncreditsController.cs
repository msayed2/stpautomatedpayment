﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
   public abstract class IncreditsController
    {
        public abstract void CreateBulk (List<InCredit> inCredits,out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
        public abstract List<InCredit> GetALL(Func<InCredit, bool> expression, out bool? dbStatus);
        public abstract List<InCreditType> GetInCreditTypes(Func<InCreditType, bool> expression, out bool? dbStatus);
    }
}
