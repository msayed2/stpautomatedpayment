﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CompanyController
    {
        public abstract List<Company> GetAll(int skip, int take, out bool? dbStatus);
        public abstract List<Company> GetAll(Func<Company, bool> expression, int skip, int take, out bool? dbStatus);
        public abstract Company Get(Func<Company, bool> expression, out bool? dbStatus);
        public abstract void Create(Company objCompany, out bool? dbStatus);
        public abstract void Update(Company objCompany, out bool? dbStatus);
        public abstract void Delete(Func<Company, bool> expression, out bool? dbStatus);
        public abstract int GetTotalCompaniesCount(Func<Company, bool> expression, out bool? status);

        public abstract void Update(CompanyBilling objCompanyBilling, out bool? dbStatus);
        public abstract CompanyCreditLine GetCompanyCreditLine(Func<CompanyCreditLine, bool> expression, out bool? dbStatus);
        public abstract void DeActivateCreditLine(Func<CompanyCreditLine, bool> expression, out bool? dbStatus);
        public abstract void CreateCreditLine(CompanyCreditLine objCompany, out bool? dbStatus);
    }
}
