﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;


namespace SQLAccess.Controllers
{
    public abstract class PendingActionsController
    {
        public abstract PendingAction GetById(string id, out bool? dbStatus);
        public abstract List<PendingAction> Get(Func<PendingAction, bool> expression, out bool? dbStatus);
        public abstract void Create(PendingAction objPendingAction, out bool? dbStatus,bool allowSaveChanges=true);
    }
}
