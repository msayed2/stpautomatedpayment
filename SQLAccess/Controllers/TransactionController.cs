﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class TransactionController
    {
        public abstract List<Transaction> GetAll(int skip, int take, out bool? status);
        public abstract List<Transaction> GetAll(Expression<Func<Transaction, bool>> expression, int skip, int take, out bool? status);
        public abstract Transaction Get(Expression<Func<Transaction, bool>> expression, out bool? status);
        public abstract void Create(Transaction objTransaction, out bool? dbStatus);
        public abstract int GetTotalTransactionsCount(Expression<Func<Transaction, bool>> expression, out bool? status);
        public abstract List<Transaction> GetTransactions(Expression<Func<Transaction, bool>> expression, out bool? dbStatus);
        public abstract void DeActivate(Func<Transaction, bool> expression, out bool? dbStatus);
    }
}
