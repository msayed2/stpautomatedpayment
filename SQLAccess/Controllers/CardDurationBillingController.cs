﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CardDurationBillingController
    {
        public abstract CardDurationBilling Get(Func<CardDurationBilling, bool> expression, out bool? dbStatus);
        public abstract void Update(CardDurationBilling objCardDurationBilling, out bool? dbStatus);
        public abstract void Create(CardDurationBilling objCardDurationBilling, out bool? dbStatus);
        public abstract void CreateCardDurationSetting(CardDurationSetting objCardDurationSetting, out bool? dbStatus);
    }
}
