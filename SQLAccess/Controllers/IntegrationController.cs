﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class IntegrationController
    {
        public abstract List<Integration> GetALL(Func<Integration, bool> expression, out bool? dbStatus);
        public abstract void CreateUserIntegration(IntegrationsPerUser objIntegration, out bool? dbStatus);

    }
}
