﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
   public abstract class SettlementController
    {
        public abstract List<SettlementType> GetSettlementTypes(Func<SettlementType, bool> expression, out bool? dbStatus);
    }
}
