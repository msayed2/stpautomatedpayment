﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;


namespace SQLAccess.Controllers
{
    public abstract class NotificationController
    {
        public abstract List<Notification> GetAll(string user_id, out bool? dbStatus);
        public abstract void MarkAsRead(string notification_id,out bool? dbStatus);
        public abstract void UpdateAction(string notification_id, int action_id, out bool? dbStatus);
        public abstract List<NotificationCategory> GetAllNotificationType(Func<NotificationCategory, bool> expression, out bool? dbStatus);
        public abstract void CreateBulkSetting(List<UserSetting> userSettings,out bool? dbStatus);
    }
}
