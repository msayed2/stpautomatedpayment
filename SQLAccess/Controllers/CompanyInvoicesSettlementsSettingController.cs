﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CompanyInvoicesSettlementsSettingController
    {
        public abstract CompanyInvoicesSettlementsSetting Get(Func<CompanyInvoicesSettlementsSetting, bool> expression, out bool? dbStatus);
        public abstract List<CompanyInvoicesSettlementsSetting> GetAll(Func<CompanyInvoicesSettlementsSetting, bool> expression, out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
        public abstract void Create(CompanyInvoicesSettlementsSetting objCompanyInvoicesSettlementsSetting, out bool? dbStatus);
    }
}
