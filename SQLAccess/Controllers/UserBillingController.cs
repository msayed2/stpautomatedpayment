﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class UserBillingController
    {

        public abstract void CreateBulk(List<UserBilling> userBillings, out bool? dbStatus);
        public abstract void Create(UserBilling objUserBilling, out bool? dbStatus);
        public abstract void Update(UserBilling objUserBilling, out bool? dbStatus);
    }
}
