﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class UserDurationBillingPerDayController
    {
        public abstract void Update(UserDurationBillingPerDay objUserDurationBillingPerDay, out bool? dbStatus);
        public abstract void Create(UserDurationBillingPerDay objUserBilling, out bool? dbStatus);

    }
}
