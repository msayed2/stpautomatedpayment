﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class UserDurationBillingController
    {
        public abstract void Create(UserDurationBilling objUserDurationBilling, out bool? dbStatus);
        public abstract UserDurationBilling GetUserDurationBilling(string profile_id);
        public abstract void Update(UserDurationBilling objUserDurationBilling, out bool? dbStatus);
        public abstract void CreateUserDurationSetting(UserDurationSetting objUserDurationSetting, out bool? dbStatus);

    }
}
