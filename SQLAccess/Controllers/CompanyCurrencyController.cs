﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CompanyCurrencyController
    {
        public abstract CompanyCurrency Get(Func<CompanyCurrency, bool> expression, out bool? dbStatus);
        public abstract void Create(CompanyCurrency objCurrency, out bool? dbStatus);
    }
}
