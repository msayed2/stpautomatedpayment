﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;


namespace SQLAccess.Controllers
{
    public abstract class ExchangeDetailsController
    {
        public abstract void Create(ExchangeDetail objExchangeDetails, out bool? dbStatus);
        
    }
}
