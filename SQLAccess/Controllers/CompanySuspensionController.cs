﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CompanySuspensionController
    {
        public abstract void Create(CompanySuspension objCompanySuspension, out bool? dbStatus);
        public abstract CompanySuspension Get(Func<CompanySuspension, bool> expression, out bool? dbStatus);
    }
}
