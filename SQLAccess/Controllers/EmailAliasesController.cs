﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class EmailAliasesController
    {
        public abstract EmailAlias Get(Func<EmailAlias, bool> expression, out bool? dbStatus);
        public abstract List<EmailAlias> GetAll(Func<EmailAlias, bool> expression, out bool? dbStatus);

        public abstract void Create(EmailAlias objEmailAlias, out bool? dbStatus);

    }
}
