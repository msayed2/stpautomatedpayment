﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CurrencyController
    {
        public abstract Currency Get(Expression<Func<Currency, bool>> expression, out bool? dbStatus);
        public abstract List<Currency> GetAll(Expression<Func<Currency, bool>> expression, out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
        public abstract void Create(Currency objCurrency, out bool? dbStatus);
    }
}
