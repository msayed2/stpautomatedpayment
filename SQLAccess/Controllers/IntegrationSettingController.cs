﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
   public abstract class IntegrationSettingController
    {
        public abstract List<IntegrationSetting> GetALL(Func<IntegrationSetting, bool> expression, out bool? dbStatus);
    }
}
