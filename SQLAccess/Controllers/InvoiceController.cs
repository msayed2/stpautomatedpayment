﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
   public abstract class InvoiceController
    {
        public abstract void Create(Invoice objInvoice, out bool? dbStatus);
        public abstract Invoice Get(Expression<Func<Invoice,bool>> expression, out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
        public abstract List<InvoicesStatu> GetInvoicesStatus(Func<InvoicesStatu, bool> expression, out bool? dbStatus);

    }
}
