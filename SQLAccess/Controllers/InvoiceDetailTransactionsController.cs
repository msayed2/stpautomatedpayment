﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class InvoiceDetailTransactionsController
    {
        public abstract void Create(TransactionsInvoiceDetail objTransactionsInvoiceDetails, out bool? dbStatus);
        public abstract void CreateBulk(List<TransactionsInvoiceDetail> transactionsInvoiceDetaislList, out bool? dbStatus);
    }
}
