﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class DigitalWalletMessageController
    {
        public abstract List<DigitalWalletMessage> GetAll(Func<DigitalWalletMessage, bool> expression, out bool? dbStatus);
        public abstract void Create(DigitalWalletMessage objDigitalWalletMessage, out bool? dbStatus, bool allowSaving = true);
        public abstract void Update(string id, int stateId, out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
    }
}
