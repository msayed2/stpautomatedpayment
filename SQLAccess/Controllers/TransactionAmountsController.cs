﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;


namespace SQLAccess.Controllers
{
    public abstract class TransactionAmountsController
    {
        public abstract List<TransactionAmount> GetALLAsNoTracking(Expression<Func<TransactionAmount, bool>> expression, out bool? dbStatus);
        public abstract TransactionAmount Get(Func<TransactionAmount, bool> expression, out bool? dbStatus);


    } 
}
