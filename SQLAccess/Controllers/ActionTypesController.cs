﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
   public abstract class ActionTypesController
    {
        public abstract ActionType Get(Func<ActionType, bool> expression, out bool? dbStatus);
        public abstract List<ActionType> GetAll(Func<ActionType, bool> expression, out bool? dbStatus);
    }
}
