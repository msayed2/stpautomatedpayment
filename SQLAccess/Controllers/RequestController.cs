﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class RequestController
    {
        public abstract void Create(ResetPasswordRequest objRequest, out bool? dbStatus);
        public abstract ResetPasswordRequest Get(Func<ResetPasswordRequest, bool> expression, out bool? dbStatus);
    }
}
