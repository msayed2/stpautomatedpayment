﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CompanyAliasController
    {
        public abstract void Create(CompanyAlias model, out bool? dbStatus);
        public abstract CompanyAlias Get(Func<CompanyAlias, bool> expression, out bool? dbStatus);

    }
}
