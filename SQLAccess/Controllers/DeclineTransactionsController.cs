﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class DeclineTransactionsController
    {
        public abstract void Create(DeclinedTransaction objDeclinedTransaction, out bool? dbStatus);
        public abstract DeclinedTransaction Get(Expression<Func<DeclinedTransaction, bool>> expression, out bool? dbStatus);
        public abstract List<DeclinedTransaction> GetAll(Expression<Func<DeclinedTransaction, bool>> expression, out bool? dbStatus);

    }
}
