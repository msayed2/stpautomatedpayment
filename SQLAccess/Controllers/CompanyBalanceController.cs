﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class CompanyBalanceController
    {
        public abstract CompanyBalance Get(Func<CompanyBalance, bool> expression, out bool? dbStatus);

        public abstract void SaveChanges(out bool? dbStatus);
    }
}
