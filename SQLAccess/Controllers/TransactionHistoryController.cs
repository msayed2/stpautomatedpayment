﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class TransactionHistoryController
    {
        public abstract void Create(TransactionsHistory objTransactionHistory, out bool? dbStatus);
    }
}
