﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
   public abstract class CardBillingController
    {
        public abstract void CreateBulk(List<CardBilling> cardBillings, out bool? dbStatus);
        public abstract void Create(CardBilling objCardBilling, out bool? dbStatus);
        public abstract void Update(CardBilling objCardBilling, out bool? dbStatus);
    }
}
