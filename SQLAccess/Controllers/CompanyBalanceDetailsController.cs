﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
  public abstract class CompanyBalanceDetailsController
    {
        public abstract void Create(CompanyBalanceDetail objCompanyBalanceDetail,out bool? dbStatus);
        public abstract void DeActivate(string id, out bool? dbStatus);

    }
}
