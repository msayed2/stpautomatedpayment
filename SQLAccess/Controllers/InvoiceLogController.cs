﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class InvoiceLogController
    {
        public abstract void Create(InvoiceLog objInvoice, out bool? dbStatus);
    }
}
