﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class AttachmentController
    {
        public abstract void CreateBulk(List<Attachment> model, out bool? dbStatus);

        public abstract void Create(Attachment model, out bool? dbStatus);
        public abstract List<AttachmentType> GetAttachmentTypes(Func<AttachmentType, bool> expression, out bool? dbStatus);
        public abstract Attachment Get(Func<Attachment, bool> expression, out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
    }
}
