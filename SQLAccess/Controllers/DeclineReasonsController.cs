﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class DeclineReasonsController
    {
        public abstract DeclineReason Get(Expression<Func<DeclineReason, bool>> expression, out bool? dbStatus);
        public abstract List<DeclineReason> GetALL(Expression<Func<DeclineReason, bool>> expression, out bool? dbStatus);
        public abstract void Create(DeclineReason objDeclineReason, out bool? dbStatus);
    }
}
