﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class MerchantController
    {
        public abstract Merchant Get(Func<Merchant, bool> expression, out bool? dbStatus);

    }
}
