﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace SQLAccess.Controllers
{
    public abstract class InvoiceCurrencyController
    {
        public abstract void Create(InvoiceCurrency objInvoiceCurrency, out bool? dbStatus);
        public abstract void SaveChanges(out bool? dbStatus);
        public abstract InvoiceCurrency Get(Expression<Func<InvoiceCurrency,bool>> expression, out bool? dbStatus);
    }
}
