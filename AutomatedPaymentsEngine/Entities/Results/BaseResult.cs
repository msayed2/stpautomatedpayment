﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Results
{
    public class BaseResult : CommonResult
    {
        public Status status { set; get; }  
    }
    public class BasePagingResult : BaseResult
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Paging paging { set; get; }
    }

    public class Status
    {
        public bool is_success { set; get; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string error_code { set; get; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string error_message { set; get; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string error_info { set; get; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string extra_info { set; get; }
    }
}
