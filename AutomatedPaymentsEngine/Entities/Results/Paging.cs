﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Results
{
    public class Paging
    {
        public int total_items { set; get; }
        public int items_per_page { set; get; }
        public int total_pages { set; get; }
        public int current_page { set; get; }
    }
}
