﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Parameters
{
    public class StpSendPaymentWebhookParams
    {
        public string id { set; get; }
        public string empresa { set; get; }
        public string folioOrigen { set; get; }
        public string estado { set; get; }
        public string causaDevolucion { set; get; }
    }
}
