﻿using Entity.Entities.TribalEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AutomatedPaymentsEngine.Entities.Parameters
{
    public class CreatePaymentParams : BaseParam
    {
        public string message { get; set; }

        public double amount { get; set; }

        public string beneficiary_id { get; set; }
        [JsonIgnore]
        public string user_Id { get; set; }
        [JsonIgnore]
        public Profile objProfile { get; set; }
        // public string default_account_currency { get; set; }


    }
}
