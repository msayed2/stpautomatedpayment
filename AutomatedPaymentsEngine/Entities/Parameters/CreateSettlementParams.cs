﻿using AutomatedPaymentsEngine.Entities.Parameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Parameters
{
    public class CreateSettlementParams : BaseParam
    {
        [JsonProperty("company_id")]
        public string CompanyId { get; set; }
        [JsonProperty("total_amount")]
        public double TotalAmount { set; get; }
        [JsonProperty("total_usd_amount")]
        public double TotalUSDAmount { set; get; }
        [JsonProperty("currency")]
        public string Currency { set; get; }
        [JsonProperty("paid_date")]
        public DateTime? paidDate { set; get; }
        [JsonProperty("type_id")]
        public int typeId { set; get; }
        [JsonProperty("allow_sending_email")]
        public bool allowSendingEmail { get; set; }
        [JsonProperty("sender_name")]
        public string senderName { get; set; }
        [JsonProperty("reference_id")]
        public string referenceId { get; set; }
        [JsonProperty("sourceTypeId")]
        public int sourceTypeId { get; set; }
    }
}
