﻿using AutomatedPaymentsEngine.Entities.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Parameters
{
    public class ExcludeCompanyParams : BaseParam
    {
        public string companyId { set; get; }
        public bool isExcluded { set; get; }
    }
}
