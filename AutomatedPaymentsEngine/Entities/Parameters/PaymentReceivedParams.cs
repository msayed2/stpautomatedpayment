﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Entities.Parameters
{
    public class PaymentReceivedParams : BaseParam
    {
        public string company_id { get; set; }
        public double amount { get; set; }
        public string device_id { set; get; }
        public DateTime date { get; set; }
        public double balance { get; set; }
        public string type { get; set; }
        public string invoice_name { get; set; }
    }
    public class PaymentReceivedInfoMap : ClassMap<PaymentReceivedParams>
    {
        public PaymentReceivedInfoMap()
        {
            Map(m => m.company_id).Name("companyId");
            Map(m => m.amount).Name("amount");
            Map(m => m.date).Name("Date");
            Map(m => m.balance).Name("balance");
            Map(m => m.type).Name("type");
            Map(m => m.invoice_name).Name("invoice_name");
        }
    }
}
