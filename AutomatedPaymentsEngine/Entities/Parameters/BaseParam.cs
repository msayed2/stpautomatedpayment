﻿using AutomatedPaymentsEngine.Entities.Constatnts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Entities.Parameters
{
    public class BaseParam 
    {
        [JsonIgnore]
        public FeatureTypeEnum OperationType { set; get; }

    }
}
