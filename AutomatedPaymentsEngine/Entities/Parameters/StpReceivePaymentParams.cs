﻿using AutomatedPaymentsEngine.Entities.Parameters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Parameters
{
    public class StpReceivePaymentParams : BaseParam
    {
        public int id { set; get; }
        [JsonProperty("fechaOperacion")]
        public int date { set; get; }
        [JsonProperty("institucionOrdenante")]
        public int payer_bank_key { set; get; }
        [JsonProperty("institucionBeneficiaria")]
        public int beneficiary_bank_key { set; get; }
        [JsonProperty("claveRastreo")]
        public string tracking_code { set; get; }
        [JsonProperty("monto")]
        public float amount { set; get; }
        [JsonProperty("nombreOrdenante")]
        public string payer_name { set; get; }
        [JsonProperty("tipoCuentaOrdenante")]
        public int payer_account_type { set; get; }
        [JsonProperty("cuentaOrdenante")]
        public string payer_account { set; get; }
        [JsonProperty("rfcCurpOrdenante")]
        public string payer_rfc { set; get; }
        [JsonProperty("nombreBeneficiario")]
        public string beneficiary_name { set; get; }
        [JsonProperty("tipoCuentaBeneficiario")]
        public int beneficiary_account_type { set; get; }
        [JsonProperty("cuentaBeneficiario")]
        public string beneficiary_account { set; get; }
        [JsonProperty("rfcCurpBeneficiario")]
        public string beneficiary_rfc { set; get; }
        [JsonProperty("conceptoPago")]
        public string payment_reason { set; get; }
        [JsonProperty("referenciaNumerica")]
        public int ref_number { set; get; }
        [JsonProperty("empresa")]
        public string company_name { set; get; }
    }
}
public class EmailPaymentData
{
    public int id { set; get; }
    [JsonProperty("fechaOperacion")]
    public int date { set; get; }
    [JsonProperty("institucionOrdenante")]
    public int payer_bank_key { set; get; }
    [JsonProperty("institucionBeneficiaria")]
    public int beneficiary_bank_key { set; get; }
}

