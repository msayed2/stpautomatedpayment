﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Entities.Parameters
{
    public class GetCompanyNameParams : BaseParam
    {
        public string company_id { get; set; }
    }
}
