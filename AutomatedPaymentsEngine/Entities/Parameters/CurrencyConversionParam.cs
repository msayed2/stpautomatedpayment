﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Entities.Parameters
{
    public class CurrencyConversionParam
    {
        public string Name { get; set; }
        public double Rate { get; set; }
        public string RateProvider { get; set; }
    }
}
