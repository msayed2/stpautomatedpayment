﻿using SQLProvider.Controllers;
using SQLProvider.TribalDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;

namespace Configuration
{
    public class Loader
    {
        private ConnectionManager _objConnectionManager = null;
        private SystemMessageController _objSystemMessageController = null;
        private UserMessageController _objUserMessageController = null;

        public Loader()
        {
            _objConnectionManager = new ConnectionManager();
        }
        private void Init()
        {
            var objConn = _objConnectionManager.Open();
            if (objConn == null)
            {
                throw new Exception("8c76f28d-bcf4-45c3-a863-6b0d465d6df9 DB Connection failed");
            }
            _objUserMessageController = new UserMessageController(objConn);
            _objSystemMessageController = new SystemMessageController(objConn);
        }

        public void Load()
        {

            DBMessagesConfig objDBConfig = new DBMessagesConfig();
            
            Init();
            bool? dbStatus = null;
            List<SystemMessage> systemMessages = _objSystemMessageController.GetAll(I => I.IsActive == true, out dbStatus);
            List<UserMessage> userMessages = _objUserMessageController.GetAll(I => I.IsActive == true, out dbStatus);
            objDBConfig.data = new MessagesData();

            objDBConfig.data.systemMessage = new List<SystemMessageData>();
            objDBConfig.data.userMessage = new List<UserMessageData>();
            foreach (SystemMessage systemMessage in systemMessages)
            {
                SystemMessageData objSsystemData = new SystemMessageData();
                objSsystemData.id = systemMessage.Id;
                objSsystemData.messageName = systemMessage.Message;
                objSsystemData.langauge = new MessageLangauge();
                objSsystemData.langauge.id = systemMessage.Language.Id;
                objSsystemData.langauge.name = systemMessage.Language.Name;
                objSsystemData.systemMessageTypes = new SystemMessageTypes();
                objSsystemData.systemMessageTypes.id = systemMessage.SystemMessageType.Id;
                objSsystemData.systemMessageTypes.name = systemMessage.SystemMessageType.Name;
                objDBConfig.data.systemMessage.Add(objSsystemData);
            }
            foreach (UserMessage userMessage in userMessages)
            {
                UserMessageData objUserData = new UserMessageData();
                objUserData.id = userMessage.Id;
                objUserData.messageName = userMessage.Message;
                objUserData.langauge = new MessageLangauge();
                objUserData.langauge.id = userMessage.Language.Id;
                objUserData.langauge.name = userMessage.Language.Name;
                objUserData.userMessageTypes = new UserMessageTypes();
                objUserData.userMessageTypes.id = userMessage.UserMessageType.Id;
                objUserData.userMessageTypes.name = userMessage.UserMessageType.Name;
                objDBConfig.data.userMessage.Add(objUserData);
            }
            Globals.Global._DBConfig = objDBConfig;


            Config objConfig = new Config();
            objConfig.StripeApiKey = ConfigurationManager.AppSettings["StripeApiKey"];
            if (string.IsNullOrEmpty(objConfig.StripeApiKey) || string.IsNullOrWhiteSpace(objConfig.StripeApiKey))
            {
                throw new Exception("aad22a31-f119-4c91-b630-57e905c5b140 invalid  StripeApiKey in configuration");
            }

            if(ConfigurationManager.AppSettings["PageSize"] != null)
            {
                objConfig.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            }
            if (ConfigurationManager.AppSettings["MinLimitValue"] != null)
            {
                objConfig.MinLimitValue = Convert.ToInt32(ConfigurationManager.AppSettings["MinLimitValue"]);
            }
            if (ConfigurationManager.AppSettings["Take"] != null)
            {
                objConfig.Take = Convert.ToInt32(ConfigurationManager.AppSettings["Take"]);
            }



            objConfig.TwilioAccountSid = ConfigurationManager.AppSettings["AccountSid"];
            objConfig.TwilioAuthToken = ConfigurationManager.AppSettings["AuthToken"];
            objConfig.TwilioSender = ConfigurationManager.AppSettings["TwilioNumber"];
           

            #region Docusign read Props 
            objConfig.JWTScope = "signature";
            objConfig.PermissionScopes = "signature impersonation";
            Config.PrivateKey = ConfigurationManager.AppSettings["PrivateKey"];
            Config.ClientID = ConfigurationManager.AppSettings["ClientID"];
            Config.AuthServer = ConfigurationManager.AppSettings["AuthServer"];
            Config.ImpersonatedUserGuid = ConfigurationManager.AppSettings["ImpersonatedUserGuid"];
            Config.TargetAccountID = ConfigurationManager.AppSettings["TargetAccountID"];
            objConfig.accountId = ConfigurationManager.AppSettings["accountId"];
            objConfig.basePath = ConfigurationManager.AppSettings["BasePath"];
            objConfig.callBackUrl = ConfigurationManager.AppSettings["CallBackUrl"];
            objConfig.tokenFilePath = ConfigurationManager.AppSettings["tokenFilePath"];
            #endregion

            #region Attachement Paths
            objConfig.InvoicesAttachementPath = ConfigurationManager.AppSettings["InvoicesAttachementPath"];
            objConfig.DownloadAttachementPath = ConfigurationManager.AppSettings["DownloadAttachementPath"];

            
            objConfig.SpacePath = ConfigurationManager.AppSettings["SpacePath"];
            objConfig.RowsPath = ConfigurationManager.AppSettings["RowsPath"];
            #endregion

            #region XE Currency
            objConfig.XeUsername = ConfigurationManager.AppSettings["XeUsername"];
            objConfig.XePassword = ConfigurationManager.AppSettings["XePassword"];
            if (ConfigurationManager.AppSettings["RateDigits"] != null)
            {
                objConfig.RateDigits = Convert.ToInt32(ConfigurationManager.AppSettings["RateDigits"]);
            }
            #endregion

            objConfig.invoicePath = ConfigurationManager.AppSettings["invoicePath"];
            objConfig.CompanyInfo = ConfigurationManager.AppSettings["CompanyInfo"];
         
            objConfig.ExportURL = ConfigurationManager.AppSettings["ExportURL"];
            objConfig.PastCreditsToolTipText = userMessages.FirstOrDefault(I=> I.UserMessageTypeId == 33).Message;
            //objConfig.PastCreditsToolTipText = ConfigurationManager.AppSettings["PastCreditsToolTipText"];
            if (ConfigurationManager.AppSettings["dueByPerDay"] != null)
            {
                objConfig.dueByPerDay = int.Parse(ConfigurationManager.AppSettings["dueByPerDay"]);
            }
            if (ConfigurationManager.AppSettings["ExpiredAtPerMinutes"] != null)
            {
                objConfig.expiredAtPerMinutes = int.Parse(ConfigurationManager.AppSettings["ExpiredAtPerMinutes"]);
            }
            objConfig.Normal_wire_Instructions = ConfigurationManager.AppSettings["NormalWireInstructions"];
            objConfig.MXN_wire_Instructions = ConfigurationManager.AppSettings["MXNWireInstructions"];
            objConfig.ProductionMode = ConfigurationManager.AppSettings["ProductionMode"] == null ? false : bool.Parse(ConfigurationManager.AppSettings["ProductionMode"]);

            objConfig.SingleUSDCurrencyDescription= ConfigurationManager.AppSettings["SingleUSDCurrencyDescription"];
            objConfig.PercentageDaysNumber = ConfigurationManager.AppSettings["PercentageDaysNumber"];
            Globals.Global.Config = objConfig;
        }
    }
}
