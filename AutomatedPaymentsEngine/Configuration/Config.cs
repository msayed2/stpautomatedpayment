﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    public class Config
    {
        public string StripeApiKey { set; get; }
        public int PageSize { set; get; }
        public int Take { set; get; }
        public int MinLimitValue { set; get; }
        public string TwilioAccountSid { set; get; }
        public string TwilioAuthToken { set; get; }
        public string TwilioSender { set; get; }

        #region Docusign prop
        public static  string ClientID { get;  set; }
        public static string ImpersonatedUserGuid { get;  set; }
        public static string TargetAccountID { get;  set; }
       
        public static  string PrivateKey { get;  set; }
        private static  string authServer;
        public string basePath { get; set; }
        public string tokenFilePath { get; set; }
        public string callBackUrl { get; set; }

        public static  string AuthServer
        {
            get { return authServer; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value) && value.StartsWith("https://"))
                {
                    authServer = value.Substring(8);
                }
                else if (!String.IsNullOrWhiteSpace(value) && value.StartsWith("http://"))
                {
                    authServer = value.Substring(7);
                }
                else
                {
                    authServer = value;
                }
            }
        }
      
        public  string PermissionScopes { get;  set; }
        public  string JWTScope { get;  set; }
        public string accountId { get; set; }

        #endregion

        #region Attachement props
        public string InvoicesAttachementPath { get; set; }
        public string DownloadAttachementPath { get; set; }

        
        public string SpacePath { get; set; }
        public string RowsPath { get; set; }
        public string ExportURL { get; set; }
        public string SingleUSDCurrencyDescription { get; set; }

        #endregion
        #region XE Currency
        public int RateDigits { set; get; }
        public string XeUsername { get; set; }
        public string XePassword { get; set; }
        #endregion

        public string invoicePath { get; set; }
        public string CompanyInfo { get; set; }
        public string PastCreditsToolTipText { get; set; }
        public int dueByPerDay { get; set; }
        public int expiredAtPerMinutes { get; set; }

        public string Normal_wire_Instructions { get; set; }
        public string MXN_wire_Instructions { get; set; }
        public bool ProductionMode { get; set; }
        public string PercentageDaysNumber { get; set; }
    }
}
