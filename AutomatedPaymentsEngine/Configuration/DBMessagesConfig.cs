﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    public class DBMessagesConfig
    {
        public MessagesData data { get; set; }
    }
    public class MessagesData
    {
        public List<SystemMessageData> systemMessage { get; set; }

        public List<UserMessageData> userMessage { get; set; }
    }
    public class SystemMessageData
    {
        public int id { get; set; }
        public string messageName { get; set; }
        public MessageLangauge langauge { get; set; }
        public SystemMessageTypes systemMessageTypes { get; set; }
    }
    public class SystemMessageTypes
    {
        public int id { get; set; }
        public string name { get; set; }

    }

    public class MessageLangauge
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class UserMessageData
    {
        public int id { get; set; }
        public string messageName { get; set; }
        public MessageLangauge langauge { get; set; }
        public UserMessageTypes userMessageTypes { get; set; }

    }
    public class UserMessageTypes
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
