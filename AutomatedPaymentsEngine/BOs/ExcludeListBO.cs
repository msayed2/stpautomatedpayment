﻿using Business.Constants;
using Spacegene.Logger;
using SQLProvider.Controllers;
using SQLProvider.TribalDB;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Entity.Entities.TribalEntities;
using Entities.Results;
using Entities.Parameters;

namespace BOs
{
    public class ExcludeListBO
    {
        private TribalPaymentConnectionManager _objConnectionManager = null;
        private ExcludedListController _objExcludedListController = null;
        public ExcludeListBO()
        {
            _objConnectionManager = new TribalPaymentConnectionManager();
        }
        public BaseResult ExcludeCompany(ExcludeCompanyParams model)
        {
            BaseResult objResult = new BaseResult();
            bool? dbStatus = null;
            try
            {
                Init();
                ExcludedList objExcludedCompany = _objExcludedListController.Get(I => I.CompanyId == model.companyId, out dbStatus);
                if (dbStatus == true)
                {
                    objExcludedCompany.UpdatedAt = DateTime.UtcNow;
                    objExcludedCompany.IsExcluded = model.isExcluded;
                    _objExcludedListController.Update(objExcludedCompany, out dbStatus);
                }
                else
                {
                    objExcludedCompany = new ExcludedList();
                    objExcludedCompany.Id = Guid.NewGuid().ToString();
                    objExcludedCompany.CompanyId = model.companyId;
                    objExcludedCompany.CreatedAt = DateTime.UtcNow;
                    objExcludedCompany.UpdatedAt = DateTime.UtcNow;
                    objExcludedCompany.IsExcluded = model.isExcluded;
                    _objExcludedListController.Create(objExcludedCompany, out dbStatus);
                }
                if (dbStatus == true)
                {
                    objResult.status = StatusMessages.Success;
                }
                else
                {
                    objResult.status = StatusMessages.Unknown;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"753dd740-94f8-451f-b28b-11fdc67d9c61 Error occur during exclude company from DB {MethodBase.GetCurrentMethod().Name}", ex);
            }
            finally
            {
                Destroy();
            }
            return objResult;
        }

        private void Init()
        {
            var objConn = _objConnectionManager.Open();
            if (objConn == null)
            {
                throw new Exception("3b3f74d4-d55a-41dc-b29b-ed19d3a26c02 DB Connection failed");
            }
            _objExcludedListController = new ExcludedListController(objConn);
        }
        private void Destroy()
        {
            _objConnectionManager.Close();
        }
    }
}
