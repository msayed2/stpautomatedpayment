﻿
using Business.Constants;
using Entities.Parameters;
using Entities.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Spacegene.Logger;
using SQLProvider.Controllers;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static AutomatedPaymentsEngine.Entities.Constatnts.Enums;

namespace BOs
{
    public class TribalPaymentBO
    {
        #region Private Fields
        private TribalPaymentConnectionManager _objConnectionManager = null;
        private ReceivedPaymentsController _objReceivedPaymentController = null;
        private StpWebhookController _objStpWebHookController = null;
        #endregion

        #region Constructors
        public TribalPaymentBO()
        {
            _objConnectionManager = new TribalPaymentConnectionManager();
        }
        #endregion
        private void Init()
        {
            var objConn = _objConnectionManager.Open();
            if (objConn == null)
            {
                throw new Exception("4ec78610-fa10-4e1d-80b3-5a10dbc61145 Fail to initialize Database Connection");
            }
            _objReceivedPaymentController = new ReceivedPaymentsController(objConn);
            _objStpWebHookController = new StpWebhookController(objConn);
        }
        private void Destroy()
        {
            _objConnectionManager.Close();
        }

        public BaseResult CreateReceivedPayment(StpReceivePaymentParams recievedPayment)
        {
            BaseResult objResult = new BaseResult();
            bool? dbStatus = null;
            try
            {
                Init();
             //   RecievedPayment objExistingRecievedPayment = _objReceivedPaymentController.Get(I => I.RefNumber == recievedPayment.ref_number.ToString(), out dbStatus);
             //   if(dbStatus != true)
                {
                    RecievedPayment objReceivedPayment = new RecievedPayment();
                    objReceivedPayment.Id = Guid.NewGuid().ToString();
                    objReceivedPayment.Amount = recievedPayment.amount;
                    objReceivedPayment.Bank = recievedPayment.payer_bank_key.ToString();
                    string paymentDate = recievedPayment.date.ToString();
                    objReceivedPayment.CreatedAt = DateTime.ParseExact(paymentDate, "yyyyMMdd", null);

                    DateTime dateNow = DateTime.UtcNow;
                    DateTime dateCombined= new DateTime(objReceivedPayment.CreatedAt.Year, objReceivedPayment.CreatedAt.Month, objReceivedPayment.CreatedAt.Day, dateNow.Hour, dateNow.Minute, dateNow.Second);

                    objReceivedPayment.CreatedAt = dateCombined;

                    if(dateCombined>DateTime.UtcNow)
                    {
                        objReceivedPayment.CreatedAt = DateTime.UtcNow;
                    }
                    objReceivedPayment.RefNumber = recievedPayment.ref_number.ToString();
                    objReceivedPayment.ReceiverId = (int)PaymentReceiver.STP;
                    objReceivedPayment.Description = recievedPayment.payment_reason;
                    objReceivedPayment.PayerName = recievedPayment.payer_name;
                    objReceivedPayment.PayerRFC = recievedPayment.payer_rfc;
                    objReceivedPayment.StatusId = (int)ReceivedPaymentStatus.Recieved;
                    objReceivedPayment.TransferId = recievedPayment.tracking_code;
                    objReceivedPayment.PaymentId = recievedPayment.id;
                    objReceivedPayment.BeneficiaryAccount = recievedPayment.beneficiary_account;
                    objReceivedPayment.PayerAccount = recievedPayment.payer_account;
                    objReceivedPayment.IsActive = true;
                    _objReceivedPaymentController.Create(objReceivedPayment, out dbStatus);
                    if (dbStatus == true)
                    {
                        objResult.status = StatusMessages.Success;
                    }
                    else
                    {
                        objResult.status = StatusMessages.Unknown;
                        new Logger().LogWarning(string.Format("ac8b8451-55ae-4394-9e2c-253c16ee3959 Exception in Creating received payment in database in function {0}", MethodBase.GetCurrentMethod().Name));
                    }
                }
              //  else
                {
              //      objResult.status = StatusMessages.AlreadyExistPayment;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ac8b8451-55ae-4394-9e2c-253c16ee3959 Exception in Creating received payment in database in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            finally
            {
                Destroy();
            }
            return objResult;
        }

        public string GetBankName(int bank_key)
        {
            string objBankName = null;
            IRestResponse response;
            var client = new RestClient(ConfigurationManager.AppSettings.Get("SwapCreatePaymentURL"));
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("bank_key", bank_key);
            response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                JObject json = JObject.Parse(response.Content);
                if(json["is_success"].ToString() == "true")
                {
                    objBankName = json["bank_name"].ToString();
                }
            }
            return objBankName;
        }

        private void Parse(string body, out RecievedPayment objReceivedPayment)
        {

            /*
             * An incoming USD wire greater than or equal to your alert threshold amount of $1.00 has been posted.
             * Account ending in: ******3033
             * Account Title: Analysis Checking
             * Transaction Amount: $5,000.00
             * Transaction Posting Date: 10/29/2020
             * Reference#(Fedwire/SWIFT/SVB):20203031318100
             * Sender Name: INDIEFY INC.
             */

            List<string> lines = body.Split(new List<string>() { Environment.NewLine }.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            string sender = lines.FirstOrDefault(I => I.Contains("Sender Name:")).Trim();
            sender = sender.Remove(0, sender.IndexOf("Sender Name:")).Replace("Sender Name:", "").Trim();
            string refrence = lines.FirstOrDefault(I => I.Contains("Reference#(Fedwire/SWIFT/SVB):")).Trim();
            refrence = refrence.Remove(0, refrence.IndexOf("Reference#(Fedwire/SWIFT/SVB):")).Replace("Reference#(Fedwire/SWIFT/SVB):", "").Trim();
            string amount = lines.FirstOrDefault(I => I.Contains("Transaction Amount:")).Trim();
            amount = amount.Remove(0, amount.IndexOf("Transaction Amount:")).Replace("Transaction Amount:", "").Replace("$", "").Trim();
            string timeLine = lines.FirstOrDefault(I => I.Contains("Transaction Posting Date:")).Trim();
            DateTime time = DateTime.Parse(timeLine.Remove(0, timeLine.IndexOf("Transaction Posting Date:")).Replace("Transaction Posting Date:", "").Trim());

            objReceivedPayment = new RecievedPayment();
            objReceivedPayment.Id = Guid.NewGuid().ToString();
            objReceivedPayment.Amount = Convert.ToDouble(amount);
            objReceivedPayment.USDAmount = Convert.ToDouble(amount);
            objReceivedPayment.CreatedAt = time;
            objReceivedPayment.RefNumber = refrence;
            objReceivedPayment.ReceiverId = (int)PaymentReceiver.Mail;
            objReceivedPayment.PayerName = sender;
            objReceivedPayment.StatusId = (int)ReceivedPaymentStatus.Recieved;
            objReceivedPayment.IsActive = true;
        }

        public BaseResult CreateEmailReceivedPayment(string emailRecievedPayment)
        {
            BaseResult objResult = new BaseResult();
            bool? dbStatus = null;
            try
            {
                Init();
                RecievedPayment objReceivedPayment = new RecievedPayment();
                Parse(emailRecievedPayment, out objReceivedPayment);
                _objReceivedPaymentController.Create(objReceivedPayment, out dbStatus);
                if (dbStatus == true)
                {
                    objResult.status = StatusMessages.Success;
                }
                else
                {
                    objResult.status = StatusMessages.Unknown;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("ac8b8451-55ae-4394-9e2c-253c16ee3959 Exception in Creating received payment in database in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            finally
            {
                Destroy();
            }
            return objResult;
        }

        public BaseResult SaveStpWebHook(StpSendPaymentWebhookParams stpParams)
        {
            BaseResult objResult = new BaseResult();
            try
            {
                Init();
                bool? dbStatus = null;
                StpWebhook objStpWebhook = _objStpWebHookController.Get(I => I.PaymentId == stpParams.id && I.Status == stpParams.estado, out dbStatus);
                if (dbStatus == false)
                {
                    StpWebhook stpWebhook = new StpWebhook();
                    stpWebhook.Id = Guid.NewGuid().ToString();
                    stpWebhook.PaymentId = stpParams.id;
                    stpWebhook.Company = stpParams.empresa;
                    stpWebhook.Source = stpParams.folioOrigen;
                    stpWebhook.Status = stpParams.estado;
                    stpWebhook.ErrorDescription = stpParams.causaDevolucion;
                    stpWebhook.IsSent = false;
                    _objStpWebHookController.Create(stpWebhook, out dbStatus);
                    if (dbStatus == true)
                    {
                        new Logger().LogWarning("Webhook for payment id " + stpParams.id + "saved successfully");
                        objResult.status = StatusMessages.Success;
                    }
                    else
                    {
                        new Logger().LogError("Webhook for payment id " + stpParams.id + " Not saved successfully");
                        objResult.status = StatusMessages.Success;
                    }
                }
                else
                {
                    new Logger().LogInfo("payment webhook is already exists" + stpParams.id);
                    objResult.status = StatusMessages.Success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"60e29c50-8a73-4118-b5f4-ad831a92868e Error occur during Save webhook function {MethodBase.GetCurrentMethod().Name}", ex);
            }
            return objResult;
        }

        public bool ProcessWebhook()
        {
            bool? dbStatus = null;
            new Logger().LogWarning("Start Sending STP webhook process");
            try
            {
                Init();
                List<StpWebhook> objStpWebhooks = _objStpWebHookController.GetAll(I => I.IsSent == false, out dbStatus);
                if (objStpWebhooks.Count() > 0)
                {
                    foreach (var item in objStpWebhooks)
                    {
                        new Logger().LogWarning("Send webhook for payment " + item.PaymentId);

                        StpSendPaymentWebhookParams model = new StpSendPaymentWebhookParams();
                        model.id = item.PaymentId;
                        model.empresa = item.Company;
                        model.estado = item.Status;
                        model.folioOrigen = item.Source;
                        model.causaDevolucion = item.ErrorDescription;
                        new Logger().LogWarning("Model Prepared for payment  " + item.PaymentId + "Model " + model.ToString());
                        IRestResponse response = null;
                        var client = new RestClient(ConfigurationManager.AppSettings.Get("STPSendPaymentWebhookRequestURL"));
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("content-type", "application/json");
                        request.AddParameter("Application/Json", JsonConvert.SerializeObject(model), ParameterType.RequestBody);
                        response = client.Execute(request);
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            item.IsSent = true;
                            _objStpWebHookController.SaveChanges(out dbStatus);
                            if (dbStatus == true)
                            {
                                new Logger().LogWarning("Payment webhook updated successfully For  " + item.PaymentId);
                            }
                            else
                            {
                                new Logger().LogError("Error in updating webhook payment for" + item.PaymentId);
                            }
                        }
                        else
                        {
                            new Logger().LogError("Error in calling the URL " + item.PaymentId);
                        }
                    }
                }
                else
                {
                    new Logger().LogWarning("No webhook payment request found");
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError("85d58d55-da61-4003-add5-e3e55f894052 sending process for STP " + ex);
            }

            return true;
        }

    }
}
