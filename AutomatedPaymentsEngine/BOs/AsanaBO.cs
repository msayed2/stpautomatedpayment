﻿
using AsanaLib.APIs;
using AsanaLib.Entities;
using AutomatedPaymentsEngine.Entities.Parameters;
using BOs;
using Newtonsoft.Json.Linq;
using RestSharp;
using SQLProvider.Controllers;
using SQLProvider.TribalDB;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static AutomatedPaymentsEngine.Entities.Constatnts.Enums;

namespace AutomatedPaymentsEngine
{
    public class AsanaBO : AsanaAPIs
    {
        private TribalPaymentConnectionManager _objConnectionManager = null;
        private SubtaskInfoController _objSubTaskInfo = null;
        private ReceivedPaymentsController _objRecievedPayment = null;
        private CompanyPaymentInfoController _objCompanyPaymentInfoController = null;
        public AsanaBO()
        {
            _objConnectionManager = new TribalPaymentConnectionManager();
        }
        public AsanaBO(TribalPaymentConnectionManager objTribalPaymentConnectionManager)
        {
            _objConnectionManager = objTribalPaymentConnectionManager;
        }
        public dynamic CreateSubTask(RecievedPayment paymentRecord)
        {
            Init();
            bool? dbStatus = null;
            AddTask objAddTask = new AddTask();
            PrepareTaskMetaData(objAddTask);
            PrepareTaskModel(paymentRecord, objAddTask);
            dynamic result = SubTaskAPIs.CreateSubTask(objAddTask);

            /*TEMP TO SOVLE ASANA ISSUE*/

            if (result == null)
            {
                result = $"tmp_{Guid.NewGuid().ToString()}";
            }

            if (result != null)
            {
                SubTaskInfo objSubtaskInfo = new SubTaskInfo();
                objSubtaskInfo.Id = Guid.NewGuid().ToString();
                objSubtaskInfo.SubTaskId = result;
                objSubtaskInfo.IsActive = true;
                objSubtaskInfo.CreatedAt = DateTime.UtcNow;
                objSubtaskInfo.UpdatedAt = DateTime.UtcNow;
                _objSubTaskInfo.Create(objSubtaskInfo, out dbStatus);
                if (dbStatus == true)
                {
                    return objSubtaskInfo.Id;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public dynamic UpdateSubTask(RecievedPayment paymentRecord)
        {
            Init();
            bool? dbStatus = null;
            UpdateTask objUpdateTask = new UpdateTask();
            PrepareTaskModel(paymentRecord, objUpdateTask);
            bool result = false;
            SubTaskInfo objSubtaskInfo = _objSubTaskInfo.Get(I => I.Id == paymentRecord.SubTaskInfoId, out dbStatus);
            if (dbStatus == true)
            {
                result = SubTaskAPIs.UpdateSubTask(objSubtaskInfo.SubTaskId, objUpdateTask);
            }
            return result;
        }

        private void PrepareTaskMetaData(AsanaLib.Entities.AddTask objTask)
        {
            objTask.workspace = AsanaAPIs.WorkspaceId;
            objTask.projects = new List<string>() { AsanaAPIs.TribalProjectId };
        }

        private void PrepareTaskModel(RecievedPayment paymentRecord, AsanaLib.Entities.Task objTask)
        {
            string companyName = null;
            bool? dbStatus = false;
            objTask.assignee = AsanaAPIs.userID;
            if (paymentRecord.PayerName == null)
            {
                //objTask.name = defaultSubTaskName; TODO
            }
            else
            {
                objTask.name = paymentRecord.PayerName;
            }
            if (paymentRecord.ReceiverId == (int)PaymentReceiver.STP)
            {
                List<string> ExcludedPayerNames = ConfigurationManager.AppSettings.Get("ExcludedPayerNames").Split(',').ToList();
                if (!ExcludedPayerNames.Contains(paymentRecord.PayerName))
                {
                    string companyId = new AutomatedPaymentsBO().GetCompanyId(paymentRecord.BeneficiaryAccount);
                    companyName = new AsanaBO().GetCompanyName(companyId);
                }
            }
            else
            {
                CompanyPaymentInfo objCompanyPaymentInfo = _objCompanyPaymentInfoController.Get(I => I.SenderName == paymentRecord.PayerName, out dbStatus);
                if (dbStatus == true)
                {
                    companyName = objCompanyPaymentInfo.CompanyName;
                }
            }
            objTask.html_notes = PrepareAsanaDescription(paymentRecord, companyName);
            objTask.due_at = paymentRecord.CreatedAt.ToString("yyyy-MM-ddTHH:mm:sszzz");
            //objTask.followers = new List<string>();
            string followerStr = ConfigurationManager.AppSettings.Get("followers");
            //objTask.followers = followerStr.Split(new string[] {"," },StringSplitOptions.RemoveEmptyEntries).ToList();
            if (string.IsNullOrEmpty(objTask.html_notes) || string.IsNullOrWhiteSpace(objTask.html_notes))
            {
                objTask.html_notes = "<body></body>";
            }
            //Dictionary<string, object> custom_field = PrepareCustomFieldData(paymentRecord , _objContext);
            //if (custom_field != null && custom_field.Count > 0)
            //{
            //    objTask.custom_fields = custom_field;
            //}
        }
        public string PrepareAsanaDescription(RecievedPayment recievedPayment, string companyName)
        {
            string description = $"<body>";
            if (recievedPayment.StatusId == (int)ReceivedPaymentStatus.PaymentSent)
            {
                description += $"<strong>Payment State</strong>: Sent. {Environment.NewLine}{Environment.NewLine}";
            }
            else
            {
                description += $"<strong>Payment State</strong>: Not Sent. {Environment.NewLine}{Environment.NewLine}";
            }
            if (recievedPayment.PayerName == null)
            {
                description += $"<strong>Payer Name</strong>: Unidentified{Environment.NewLine}{Environment.NewLine}";
            }
            else
            {
                description += $"<strong>Payer Name</strong>: {recievedPayment.PayerName}{Environment.NewLine}{Environment.NewLine}";
            }
            if(recievedPayment.PayerRFC != null)
            {
                description += $"<strong>Payer RFC</strong>: {recievedPayment.PayerRFC}{Environment.NewLine}{Environment.NewLine}";
            }
            if (recievedPayment.Description != null)
            {
                description += $"<strong>Description</strong>: {recievedPayment.Description}{Environment.NewLine}{Environment.NewLine}";
            }
            description += $"<strong>Date</strong>: {recievedPayment.CreatedAt}{Environment.NewLine}{Environment.NewLine}";
            description += $"<strong>Num Reference</strong>: {recievedPayment.RefNumber}{Environment.NewLine}{Environment.NewLine}";
            if (recievedPayment.RefNumber != null)
            {
                description += $"<strong>Transfer Id</strong>: {recievedPayment.TransferId}{Environment.NewLine}{Environment.NewLine}";
            }
            if (recievedPayment.Bank != null)
            {
                description += $"<strong>Bank</strong>: {recievedPayment.Bank}{Environment.NewLine}{Environment.NewLine}";
            }
            if (recievedPayment.BeneficiaryAccount != null)
            {
                description += $"<strong>Account Number</strong>: {recievedPayment.BeneficiaryAccount}{Environment.NewLine}{Environment.NewLine}";
            }
            if(recievedPayment.ReceiverId == (int)PaymentReceiver.STP)
            {
                description += $"<strong>Amount MXN</strong>: {recievedPayment.Amount}{Environment.NewLine}{Environment.NewLine}";
            }
            if (recievedPayment.USDAmount != null)
            {
                description += $"<strong>Amount USD</strong>: {recievedPayment.USDAmount}{Environment.NewLine}{Environment.NewLine}";
            }
            else
            {
                description += $"<strong>Amount USD</strong>: Conversion Pending{Environment.NewLine}{Environment.NewLine}";
            }
            if (companyName != null)
            {
                description += $"<strong>Company Name</strong>: {companyName}{Environment.NewLine}{Environment.NewLine} </body>";
            }
            else
            {
                description += $"<strong>Company Name</strong>: Unidentified{Environment.NewLine}{Environment.NewLine} </body>";
            }
            return description;
        }

        public dynamic CreateTaskHook(string subTaskId, string primId)
        {
            dynamic result = null;
            string target = ConfigurationManager.AppSettings.Get("SubTaskHookookURL");
            result = new WebHookAPIs().CreateWebhook(subTaskId, $"{target}/{primId}");
            if (result != null)
            {
                return result;
            }
            return null;
        }

        public dynamic ProcessUnidentifiedSenderPayment(string subTasktInfoId)
        {
            Init();
            bool? dbStatus = null;
            RecievedPayment objReceivedPayment = _objRecievedPayment.Get(I => I.SubTaskInfoId == subTasktInfoId, out dbStatus);
            if(dbStatus == true)
            {
                dynamic objAsanaTask = GET(objReceivedPayment.SubTaskInfo.SubTaskId);
                string objCompanyId = GetCompanyId(objAsanaTask, out dbStatus);
                if (!string.IsNullOrEmpty(objCompanyId))
                {
                    CompanyPaymentInfo objCompanyPaymentInfo = _objCompanyPaymentInfoController.Get(I => I.CompanyId == objCompanyId, out dbStatus);
                    if (dbStatus == true)
                    {
                        string commentText = ConfigurationManager.AppSettings.Get("UpdateExistingCompanyId");
                        dynamic commentResult = new CommentsAPIs().AddComment(commentText + objCompanyPaymentInfo.CompanyName, objReceivedPayment.SubTaskInfo.SubTaskId);
                    }
                    else
                    {
                        string companyName = GetCompanyName(objCompanyId);
                        if (!string.IsNullOrEmpty(companyName))
                        {
                            CompanyPaymentInfo objNewCompanyPaymentInfo = new CompanyPaymentInfo();
                            objNewCompanyPaymentInfo.Id = Guid.NewGuid().ToString();
                            objNewCompanyPaymentInfo.CompanyId = objCompanyId;
                            objNewCompanyPaymentInfo.CompanyName = companyName;
                            objNewCompanyPaymentInfo.SenderName = objReceivedPayment.PayerName;
                            objNewCompanyPaymentInfo.CreatedAt = DateTime.UtcNow;
                            objNewCompanyPaymentInfo.UpdatedAt = DateTime.UtcNow;
                            objNewCompanyPaymentInfo.IsActive = true;
                            _objCompanyPaymentInfoController.Create(objNewCompanyPaymentInfo, out dbStatus);
                            string commentText = ConfigurationManager.AppSettings.Get("UpdateNotExistingCompanyId");
                            dynamic commentResult = new CommentsAPIs().AddComment(commentText + objNewCompanyPaymentInfo.CompanyName, objReceivedPayment.SubTaskInfo.SubTaskId);
                        }
                    }
                }
            }
            return dbStatus;
        }

        public string GetCompanyName(string company_Id)
        {
            string objCompanyName = null;
            GetCompanyNameParams model = new GetCompanyNameParams();
            model.company_id = company_Id;
            IRestResponse response = null;
            var client = new RestClient(ConfigurationManager.AppSettings.Get("GetCompanyNameURL"));
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddJsonBody(model);
            //request.AddParameter("key", ConfigurationManager.AppSettings.Get("GetCompanyNameKey"),ParameterType.QueryString);
            response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                JObject json = JObject.Parse(response.Content);
                JObject status = (JObject)json["status"];
                bool is_success = Convert.ToBoolean(status["is_success"]);
                if (is_success == true)
                {
                    objCompanyName = json["company_name"].ToString();
                }
            }
            return objCompanyName;
        }

        private void Init()
        {
            var objConn = _objConnectionManager.GetContext();
            if(objConn == null)
            {
                objConn = _objConnectionManager.Open();
            }
            if (objConn == null)
            {
                throw new Exception("4ec78610-fa10-4e1d-80b3-5a10dbc61145 Fail to initialize Database Connection");
            }
            _objCompanyPaymentInfoController = new CompanyPaymentInfoController(objConn);
            _objSubTaskInfo = new SubtaskInfoController(objConn);
            _objRecievedPayment = new ReceivedPaymentsController(objConn);
        }
        private void Destroy()
        {
            _objConnectionManager.Close();
        }
    }
}
