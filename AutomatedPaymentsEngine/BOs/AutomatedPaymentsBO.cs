﻿
using AsanaLib.APIs;
using AutomatedPaymentsEngine;
using AutomatedPaymentsEngine.ApiCalls;
using AutomatedPaymentsEngine.Entities.Constatnts;
using AutomatedPaymentsEngine.Utils;
using CsvHelper;
using CsvHelper.Configuration;
using Entities.Parameters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using SendGrid;
using SendGrid.Helpers.Mail;
using Spacegene.Logger;
using SQLProvider.Controllers;
using SQLProvider.TribalPaymentDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static AutomatedPaymentsEngine.Entities.Constatnts.Enums;

namespace BOs
{
    public class AutomatedPaymentsBO
    {
        #region Private properties for Datatabase Connection
        private TribalPaymentConnectionManager _objConnectionManager = null;
        private SubtaskInfoController _objSubTaskInfo = null;
        private ReceivedPaymentsController _objRecievedPayment = null;
        private CompanyPaymentInfoController _objCompanyPaymentInfoController = null;
        private ExcludedListController _objExcludedListController = null;
        #endregion
        #region Constructor of AutomatedPaymentsBO
        public AutomatedPaymentsBO()
        {
            _objConnectionManager = new TribalPaymentConnectionManager();
        }
        #endregion
        #region Private methods for Database 
        private void Init()
        {
            var objConn = _objConnectionManager.Open();
            if (objConn == null)
            {
                throw new Exception("8c76f28d-bcf4-45c3-a863-6b0d465d6df9 DB Connection failed");
            }
            _objCompanyPaymentInfoController = new CompanyPaymentInfoController(objConn);
            _objSubTaskInfo = new SubtaskInfoController(objConn);
            _objRecievedPayment = new ReceivedPaymentsController(objConn);
            _objExcludedListController = new ExcludedListController(objConn);
        }
        private void Destroy()
        {
            _objConnectionManager.Close();
        }
        #endregion

        public void Process()
        {
            try
            {
                bool? dbStatus = null;
                bool isSuccess = false;
                string companyId;
                Init();
                //new Business.Configuration.Loader().Load();
                List<RecievedPayment> objReceivedPayments = _objRecievedPayment.GetAll(i => i.StatusId != (int)ReceivedPaymentStatus.PaymentSent && i.StatusId != (int)ReceivedPaymentStatus.PaymentNotSent, out dbStatus);
                if (dbStatus == true)
                {
                    foreach (RecievedPayment objReceivedPayment in objReceivedPayments)
                    {
                        #region OldLogic
                        //if(item.USDAmount is null)
                        //{
                        //   var convertRes = new XECurrencyBO().GetHistoricRate(item, objContext);
                        //   if(convertRes is true)
                        //    {
                        //        _objTribalPaymentController.UpdateStatus(item.id, (int)PaymentStatus.Converted, out dbStatus, true);
                        //    }
                        //}
                        //else
                        //{
                        //    _objTribalPaymentController.UpdateStatus(item.id, (int)PaymentStatus.Converted, out dbStatus, true);
                        //}
                        //if (item.StatusId == (int)PaymentStatus.Converted)
                        //{
                        //    if (item.payerName != null)
                        //    {
                        //        var paymentProceesResult = new CreatePaymentBO().ProcesPayment(item, objContext);
                        //        if (paymentProceesResult is true)
                        //        {
                        //            _objTribalPaymentController.UpdateStatus(item.id, (int)PaymentStatus.Sent, out dbStatus, true);
                        //        }
                        //        else
                        //        {
                        //            _objTribalPaymentController.UpdateStatus(item.id, (int)PaymentStatus.FailSent, out dbStatus, true);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        _objTribalPaymentController.UpdateStatus(item.id, (int)PaymentStatus.SenderUnIdentified, out dbStatus, true);
                        //    }
                        //}
                        //    if(item.StatusId == (int)PaymentStatus.Sent || item.StatusId == (int)PaymentStatus.FailSent || item.StatusId == (int)PaymentStatus.SenderUnIdentified)
                        //    {
                        //        dynamic result = _objAsanaSubTaskBO.CreateSubTask(item, objContext);
                        //        if (result != null)
                        //        {
                        //            _objTribalPaymentController.UpdateSubTaskInfoId(item.id, result, out dbStatus, true);
                        //            _objTribalPaymentController.UpdateStatus(item.id, (int)PaymentStatus.PostedToAsana, out dbStatus, true);
                        //        }
                        //        else
                        //        {
                        //            _objTribalPaymentController.UpdateStatus(item.id, (int)PaymentStatus.FailPostToAsana, out dbStatus, true);
                        //        }
                        //    } 
                        #endregion
                        #region New Logic
                        companyId = null;
                        if (string.IsNullOrEmpty(objReceivedPayment.SubTaskInfoId))
                        {
                            string subtaskInfoId = new AsanaBO(_objConnectionManager).CreateSubTask(objReceivedPayment);
                            if (subtaskInfoId != null)
                            {
                                objReceivedPayment.SubTaskInfoId = subtaskInfoId;
                                _objRecievedPayment.Update(objReceivedPayment, out dbStatus);
                            }
                            else
                            {
                                continue;
                            }
                        }
                        if (objReceivedPayment.USDAmount == 0 || objReceivedPayment.USDAmount == null)
                        {
                            //double mxnRate = Constants.CurrcurrenciesIsoCode.MXN.GetCurrentCurrencyRate();
                            double? objAmount = CurrencyConverter.Convert(Constants.CurrcurrenciesIsoCode.MXN, Constants.CurrcurrenciesIsoCode.USD, objReceivedPayment.Amount);
                            if (objAmount != 0)
                            {
                                objReceivedPayment.USDAmount = objAmount;
                                _objRecievedPayment.Update(objReceivedPayment, out dbStatus);
                                if (dbStatus == true)
                                {
                                    dynamic result = new AsanaBO(_objConnectionManager).UpdateSubTask(objReceivedPayment);
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                        if (!string.IsNullOrEmpty(objReceivedPayment.USDAmount.ToString()) && objReceivedPayment.StatusId != (int)ReceivedPaymentStatus.PaymentSent)
                        {
                            if (objReceivedPayment.PayerName != null)
                            {
                                if(objReceivedPayment.ReceiverId == (int)PaymentReceiver.STP)
                                {
                                    List<string> ExcludedPayerNames = ConfigurationManager.AppSettings.Get("ExcludedPayerNames").Split(',').ToList();
                                    if (!ExcludedPayerNames.Contains(objReceivedPayment.PayerName))
                                    {
                                        companyId = GetCompanyId(objReceivedPayment.BeneficiaryAccount);
                                    }
                                }
                                else
                                {
                                    CompanyPaymentInfo objCompanyPaymentInfo = _objCompanyPaymentInfoController.Get(I => I.SenderName == objReceivedPayment.PayerName, out dbStatus);
                                    if (dbStatus == true)
                                    {
                                        companyId = objCompanyPaymentInfo.CompanyId;
                                    }
                                }
                                if (!string.IsNullOrEmpty(companyId))
                                {
                                    if ((objReceivedPayment.ReceiverId == (int)PaymentReceiver.STP && Convert.ToBoolean(ConfigurationManager.AppSettings.Get("STPProcessPayment")) == true) || (objReceivedPayment.ReceiverId == (int)PaymentReceiver.Mail && Convert.ToBoolean(ConfigurationManager.AppSettings.Get("MailProcessPayment")) == true))
                                    {
                                        ExcludedList objExcludedList = _objExcludedListController.Get(I => I.CompanyId == companyId && I.IsExcluded == true, out dbStatus);
                                        if (dbStatus != true)
                                        {
                                            bool validate = ValidatePayment(objReceivedPayment.TransferId);
                                            if (validate)
                                            {
                                                CreateSettlementParams model = null;
                                                PreparePayment(out model, objReceivedPayment, companyId);
                                                isSuccess = ProcessPayment(model);
                                                //isSuccess = true;
                                                if (isSuccess == true)
                                                {
                                                    objReceivedPayment.StatusId = (int)ReceivedPaymentStatus.PaymentSent;
                                                    _objRecievedPayment.Update(objReceivedPayment, out dbStatus);
                                                    if (dbStatus == true)
                                                    {
                                                        dynamic result = new AsanaBO(_objConnectionManager).UpdateSubTask(objReceivedPayment);
                                                        if (result != null)
                                                        {
                                                            SubTaskInfo objSubtaskInfo = _objSubTaskInfo.Get(I => I.SubTaskId == objReceivedPayment.SubTaskInfo.SubTaskId, out dbStatus);
                                                            string commentText = ConfigurationManager.AppSettings.Get("PaymentSentComment");
                                                            dynamic commentResult = new CommentsAPIs().AddComment(commentText, objSubtaskInfo.SubTaskId);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (objReceivedPayment.StatusId != (int)ReceivedPaymentStatus.PaymentNotSent)
                                                    {
                                                        objReceivedPayment.StatusId = (int)ReceivedPaymentStatus.PaymentNotSent;
                                                        _objRecievedPayment.Update(objReceivedPayment, out dbStatus);
                                                        if (dbStatus == true)
                                                        {
                                                            SubTaskInfo objSubtaskInfo = _objSubTaskInfo.Get(I => I.SubTaskId == objReceivedPayment.SubTaskInfo.SubTaskId, out dbStatus);
                                                            string commentText = ConfigurationManager.AppSettings.Get("PaymentFailSentComment");
                                                            dynamic commentResult = new CommentsAPIs().AddComment(commentText, objSubtaskInfo.SubTaskId);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //TODO: Send an email to user to un exclude the company
                                            string Receiver = ConfigurationManager.AppSettings.Get("CompanyExcludeTo");
                                            string Subject = ConfigurationManager.AppSettings.Get("CompanyExcludeSubject");
                                            string body = ConfigurationManager.AppSettings.Get("CompanyExcludebody") + companyId;
                                            string fromEmail = ConfigurationManager.AppSettings.Get("CompanyExcludeFrom");
                                            string apiKey = ConfigurationManager.AppSettings.Get("SendGridAPIKey");
                                            var client = new SendGridClient(apiKey);
                                            SendGridMessage objSendGridMessage = new SendGridMessage();
                                            objSendGridMessage.AddTo(Receiver);
                                            objSendGridMessage.PlainTextContent = body;
                                            objSendGridMessage.From = new EmailAddress(fromEmail);
                                            objSendGridMessage.Subject = Subject;
                                            var response = client.SendEmailAsync(objSendGridMessage).Result;
                                        }
                                    }
                                }
                                else
                                {
                                    if (objReceivedPayment.StatusId != (int)ReceivedPaymentStatus.SenderUnidentified)
                                    {
                                        if(objReceivedPayment.ReceiverId == (int)PaymentReceiver.Mail)
                                        {
                                            objReceivedPayment.StatusId = (int)ReceivedPaymentStatus.SenderUnidentified;
                                            _objRecievedPayment.Update(objReceivedPayment, out dbStatus);
                                            if(dbStatus == true)
                                            {
                                                SubTaskInfo objSubtaskInfo = _objSubTaskInfo.Get(I => I.Id == objReceivedPayment.SubTaskInfoId, out dbStatus);
                                                string commentText = ConfigurationManager.AppSettings.Get("PaymentSenderUnidentifiedComment");
                                                dynamic commentResult = new CommentsAPIs().AddComment(commentText, objSubtaskInfo.SubTaskId);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (objReceivedPayment.StatusId != (int)ReceivedPaymentStatus.SenderUnidentified)
                                {
                                    objReceivedPayment.StatusId = (int)ReceivedPaymentStatus.SenderUnidentified;
                                    _objRecievedPayment.Update(objReceivedPayment, out dbStatus);
                                    if (dbStatus == true)
                                    {
                                        SubTaskInfo objSubtaskInfo = _objSubTaskInfo.Get(I => I.SubTaskId == objReceivedPayment.SubTaskInfoId, out dbStatus);
                                        string commentText = ConfigurationManager.AppSettings.Get("PaymentSenderUnidentifiedComment");
                                        dynamic commentResult = new CommentsAPIs().AddComment(commentText, objSubtaskInfo.SubTaskId);
                                    }
                                }
                            }
                        }
                        if (objReceivedPayment.StatusId == (int)ReceivedPaymentStatus.SenderUnidentified)
                        {
                            if (objReceivedPayment.ReceiverId == (int)PaymentReceiver.Mail)
                            {
                                SubTaskInfo objSubtaskInfo = _objSubTaskInfo.Get(I => I.Id == objReceivedPayment.SubTaskInfoId, out dbStatus);
                                if (dbStatus == true && objSubtaskInfo.HookId == null)
                                {
                                    dynamic objSubtaskHookId = new AsanaBO(_objConnectionManager).CreateTaskHook(objSubtaskInfo.SubTaskId, objSubtaskInfo.Id);
                                    if (objSubtaskHookId != null)
                                    {
                                        objSubtaskInfo.HookId = objSubtaskHookId;
                                        _objSubTaskInfo.Update(objSubtaskInfo, out dbStatus);
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("806e2d6e-6497-4e24-8506-11d20359137a Error occur during Process asana in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            finally
            {
                Destroy();
            }
        }

        public void PreparePayment(out CreateSettlementParams createSettlementParams, RecievedPayment receivedPayment, string companyId)
        {
            bool? dbStatus = null;
            CompanyPaymentInfo objCompanyPaymentInfo = _objCompanyPaymentInfoController.Get(I => I.SenderName == receivedPayment.PayerName, out dbStatus);
            createSettlementParams = new CreateSettlementParams();
            createSettlementParams.allowSendingEmail = true;
            createSettlementParams.CompanyId = companyId;
            if (receivedPayment.ReceiverId == (int)PaymentReceiver.Mail)
            {
                createSettlementParams.Currency = "USD";
                createSettlementParams.sourceTypeId = 2;
            }
            else
            {
                createSettlementParams.Currency = "MXN";
                createSettlementParams.sourceTypeId = 1;
            }
            createSettlementParams.paidDate = receivedPayment.CreatedAt;
            createSettlementParams.referenceId = receivedPayment.TransferId;
            createSettlementParams.senderName = receivedPayment.PayerName;
            createSettlementParams.TotalAmount = receivedPayment.Amount;
            createSettlementParams.TotalUSDAmount = Convert.ToDouble(receivedPayment.USDAmount);
            createSettlementParams.typeId = 1;
        }

        public bool ProcessPayment(CreateSettlementParams model)
        {
            string status;
            bool objResult = new bool();
            try
            {
                IRestResponse response = null;
                var client = new RestClient(ConfigurationManager.AppSettings.Get("TribalCreatePaymentURL"));
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddParameter("Application/Json", JsonConvert.SerializeObject(model), ParameterType.RequestBody);
                //request.AddJsonBody(model);
                response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    JObject jsonResponse = JObject.Parse(response.Content);
                    JObject result = (JObject)jsonResponse["status"];
                    status = result["is_success"].ToString();
                    objResult = bool.Parse(status);
                }
                else
                {
                    new Logger().LogWarning($"Fail when Create Payment {model.referenceId} in Tribal, and StatusCode is {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"1896a172-e5fd-49cb-a9f7-8c6dffffeb16 Exception when Create Payment in Tribal ", ex);
            }
            return objResult;
        }

        public bool ValidatePayment(string trackingCode)
        {
            bool validationResponse = false;
            try
            {
                IRestResponse response = null;
                var client = new RestClient(ConfigurationManager.AppSettings.Get("TribalValidatePaymentURL"));
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                request.AddQueryParameter("referenceId", trackingCode);
                //request.AddJsonBody(model);
                response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    validationResponse = bool.Parse(response.Content);
                }
                else
                {
                    new Logger().LogWarning($"Fail when Validate Payment with tracking code = {trackingCode} in Tribal, and StatusCode is {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"dd506c65-7718-4d8e-b2ac-d0c65f9253ae Exception when Validate Payment in Tribal ", ex);
            }
            return validationResponse;
        }

        public string GetCompanyId(string beneficiaryAccountNumber)
        {
            string companyId = null;
            bool status;
            try
            {
                IRestResponse response = null;
                var client = new RestClient(ConfigurationManager.AppSettings.Get("TribalGetCompanyIdURL"));
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                request.AddQueryParameter("account_number", beneficiaryAccountNumber);
                //request.AddParameter("Application/Json", JsonConvert.SerializeObject(model), ParameterType.RequestBody);
                //request.AddJsonBody(model);
                response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    JObject jsonResponse = JObject.Parse(response.Content);
                    JObject result = (JObject)jsonResponse["status"];
                    status = bool.Parse(result["is_success"].ToString());
                    if(status == true)
                    {
                        companyId = jsonResponse["company_Id"].ToString();
                    }
                }
                else
                {
                    new Logger().LogError($"83df1bd4-742d-4495-98ee-c0f20f6ef513 Exception when Getting CompanyId in Tribal");
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError($"3ff7ef25-2e13-46ae-9655-589732ab31f8 Exception when Getting CompanyId in Tribal ", ex);
            }
            return companyId;
        }

        //public string GetCompanyId(string beneficiaryAccountNumber)
        //{
        //    string companyId = null;
        //    try
        //    {
        //        string s = ConfigurationManager.AppSettings["STPAccountsFile"];
        //        using (var reader = new StreamReader(ConfigurationManager.AppSettings["STPAccountsFile"]))
        //        using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
        //        {
        //            companyId = csv.GetRecords<Accounts>().ToList().Where(item => item.AccountNumber == beneficiaryAccountNumber).FirstOrDefault().CompanyId;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogError($"07d38bf8-b061-431b-b267-97511589969b Exception when Get company Id from the file ", ex);
        //    }
        //    return companyId;
        //}
        public class Accounts
        {
            public string AccountNumber
            { get; set; }
            public string CompanyId { get; set; }
            public string Name
            { get; set; }
        }
    }
}
