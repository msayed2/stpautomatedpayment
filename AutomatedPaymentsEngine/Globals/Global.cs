﻿using Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
    public class Global
    {
        private static Config _Config;
        public static DBMessagesConfig _DBConfig;

        public static Config Config
        {
            get
            {
                return Global._Config;
            }
            set
            {
                Global._Config = value;
            }
        }
        public static DBMessagesConfig DBConfig
        {
            get
            {
                return Global._DBConfig;
            }
            set
            {
                Global._DBConfig = value;
            }
        }
    }
}
