﻿using IO.Swagger.Api;
using IO.Swagger.Model;
using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace AutomatedPaymentsEngine.Utils
{
    public class CurrencyConverter : XEAPIs
    {
        public static double? Convert(string from, string to, double amount)
        {
            double? toAmount = null;
            try
            {
                ConvertFromResponse objConvertFromResponse = client.ConvertFromGet(to, from, amount, obsolete, inverse, decimalPlaces);
                toAmount = (double?)objConvertFromResponse?.To?.FirstOrDefault()?.Mid;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"29949300-360b-434d-8dd6-1a3cd6d64128 Error Occur when convert {from} to {to} and amount is {amount} in function {MethodBase.GetCurrentMethod().Name}.", ex);
            }
            return toAmount;
        }
        public static ConvertFromResponse ConvertUSDToBulk(string from, string to, double amount)
        {
            ConvertFromResponse objConvertFromResponse = null;
            try
            {
                objConvertFromResponse = client.ConvertFromGet(to, from, amount, obsolete, true, 3);
            }
            catch (Exception ex)
            {
                new Logger().LogError($"38a87080-5884-4721-a1b0-a2c449d6cf37 Error Occur when convert {from} to {to} and amount is {amount} in function {MethodBase.GetCurrentMethod().Name}.", ex);
            }
            return objConvertFromResponse;
        }
        public static double? ConvertHistoric(string from, string to, double amount, DateTime convertDate)
        {
            double? toAmount = null;
            try
            {
                //  objConvertFromResponse = client.ConvertFromGet(to, from, amount, obsolete, inverse, decimalPlaces);
                HistoricRateResponse results = _HistoricRatesApiClient.HistoricRateGet(to, convertDate.ToString("yyyy-MM-dd"), from, 1, null, null, null);
                toAmount = (double?)results?.To?.FirstOrDefault()?.Mid;
            }
            catch (Exception ex)
            {
                new Logger().LogError($"38a87080-5884-4721-a1b0-a2c449d6cf37 Error Occur when convert {from} to {to} and amount is {amount} in function {MethodBase.GetCurrentMethod().Name}.", ex);
            }
            return toAmount;
        }
    }
    public class XEAPIs
    {
        public static ConversionsApi client = null;
        public static HistoricRatesApi _HistoricRatesApiClient = null;
        public static double? decimalPlaces = Convert.ToInt32(ConfigurationManager.AppSettings.Get("RateDigits"));
        public static bool? inverse = true;
        public static bool? obsolete = true;
        static XEAPIs()
        {
            IO.Swagger.Client.Configuration.Username = ConfigurationManager.AppSettings.Get("XeUsername");
            IO.Swagger.Client.Configuration.Password = ConfigurationManager.AppSettings.Get("XePassword");
            client = new ConversionsApi();
            _HistoricRatesApiClient = new HistoricRatesApi();
        }
    }
}
