﻿using AutomatedPaymentsEngine.Schedulers.Jobs;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutomatedPaymentsEngine.Schedulers.JobsNames.Enums;

namespace AutomatedPaymentsEngine.Schedulers.SchedulerManager
{
   public class SchedulerManager
    {
        private IScheduler ObjScheduler { get; set; }
        public SchedulerManager()
        {
            ObjScheduler = StdSchedulerFactory.GetDefaultScheduler();
            Start();
        }
        public IJobDetail GetJobs(JobsName JobClassName)
        {
            IJobDetail job = null;
            switch (JobClassName)
            {
                case JobsName.AutomatedPayments:
                    job = JobBuilder.Create<AutomatedPaymentsJob>().Build();
                    break;
                case JobsName.SendStpWebhook:
                    job = JobBuilder.Create<StpWebhookJob>().Build();
                    break;
                default:
                    break;
            }
            return job;

        }
        public ITrigger GetTriggerSimple(IJobDetail jobDetailObject, int secondTime = 3)
        {

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity(jobDetailObject.Key.Name, jobDetailObject.Key.Group + jobDetailObject.Key.Name)
                .StartNow()
                .ForJob(jobDetailObject.Key)
                .WithSimpleSchedule(s => s
                .WithIntervalInSeconds(3)
                .RepeatForever())
                .Build();
            ObjScheduler.ScheduleJob(jobDetailObject, trigger);
            return trigger;

        }

        public ITrigger GetTriggerCron(IJobDetail jobDetailObject, string cronExpression = "* * * * * ? *")
        {

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity(jobDetailObject.Key.Name, jobDetailObject.Key.Group + jobDetailObject.Key.Name)
                .WithCronSchedule(cronExpression)
                .StartNow()
                .ForJob(jobDetailObject.Key)
                .Build();
            ObjScheduler.ScheduleJob(jobDetailObject, trigger);
            return trigger;

        }
        public void Start()
        {
            ObjScheduler.Start();
        }
        public void Stop()
        {
            ObjScheduler.Shutdown();
        }
    }
}

