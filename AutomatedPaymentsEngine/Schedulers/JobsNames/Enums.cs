﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Schedulers.JobsNames
{
    public class Enums
    {
        public enum JobsName
        {
            AutomatedPayments,
            SendStpWebhook
        }
    }
}
