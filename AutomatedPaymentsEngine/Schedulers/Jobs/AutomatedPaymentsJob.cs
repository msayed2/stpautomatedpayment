﻿using Quartz;
using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Schedulers.Jobs
{
    [DisallowConcurrentExecution]
    public class AutomatedPaymentsJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                new BOs.AutomatedPaymentsBO().Process();
            }
            catch (Exception ex)
            {
                new Logger().LogError($"e44f7b26-a2ff-4995-9e30-ba6123c665ae Exception occur in Execute AutomatedPaymentsJob, job details :", ex);
            }
        }
    }
}
