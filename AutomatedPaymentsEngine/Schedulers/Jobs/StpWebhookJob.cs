﻿using BOs;
using Quartz;
using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Schedulers.Jobs
{
    [DisallowConcurrentExecution]
    public class StpWebhookJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                new TribalPaymentBO().ProcessWebhook();
            }
            catch (Exception except)
            {
                new Logger().LogError(string.Format("d1ac06e0-9a51-4ea3-aac3-dfd516fa212f Exception occur in {0} job details :", "Execute StpWebhook  "), except);
            }
        }
    }
}
