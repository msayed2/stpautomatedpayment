﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Entities.Constatnts
{
    public class Enums
    {
        public enum CompanyTypes
        {
            days,
            months
        }
        public enum CardTypes
        {
            physical,
            @virtual
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum SelectTransactionsTypes
        {
            CompanyId,
            UserId,
            CardId
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum SelectCardsTypes
        {
            CompanyId,
            UserId,
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum CardsStatus
        {
            stolen,
            lost,
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public enum PendingType
        {
            Email = 0,
            Phone = 1,
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum DateFilter
        {
            Last30Days,
            Last60Days,
            Last90Days
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public enum CardsFilter
        {
            MostUsed,
            Total,
        }
        [JsonConverter(typeof(StringEnumConverter))]
        public enum UsersFilter
        {
            MostActive,
            Total,
        }
        public enum UserTypes : int
        {
            SuperAdmin = 0,
            Admin = 1,
            CXO = 2,
            Manager = 3,
            User = 4,
            BookKeeper = 5,
            Accountant = 6
        }
        public enum CardStatus : int
        {
            Inactive = 0,
            Active = 1,
            Stolen = 2,
            Lost = 3,
            Stopped = 4,
            Canceled = 5
        }
        public enum ContactTypes : int
        {
            Email = 0,
            Phone = 1,
        }
        public enum ReportsTypes
        {
            MerchantCategory,
            MerchantCountry,
            MerchantName,
            User,
            Department,
            CardName,
            Last4Digits,
            Currency,
            Tags
        }
        public enum UserStatus : int
        {
            Active = 0,
            Suspend = 1,
            Pending = 2,
            Waiting = 3,
        }

        public enum OrderTypes
        {
            Asc,
            Desc
        }
        public enum DateTypes
        {
            Days,
            Months
        }

        public enum PendingActionTypes : int
        {
            SignAgreemnet = 0,
            AgreementUpdate = 1
            // add more pending actions
        }
        public enum DocusignEventTypes
        {
            // TODO: add event types 
            signing_complete,
            Pending
        }
        public enum AgreementUpdateEventTypes
        {
            Accepted,
            Rejected
        }


        public enum PendingActionStates : int
        {
            Created = 0,
            Done = 1,
            Pending = 2
        }
        public enum CompanyType : int
        {
            monthly = 1,
            duration = 2
        }
        public enum EmailAliasTypes : int
        {
            DigitalWallet = 1
        }

        public enum DigitalWalletMessagesStatus : int
        {
            Pending = 1,
            Done = 2
        }
        public enum CompanyStatus : int
        {
            Normal = 1,
            Suspended_Inactivity = 2,
            Suspended_UnPaidSettlement = 3,
            PartiallySuspended = 4,
            FullySuspended = 5
            // add more status here 
        }

        public enum InvoiceTypes : int
        {
            Month = 1,
            MidMonth = 2
        }
        public enum CurrencyTypes : int
        {
            usd = 1,
            egp = 2,
            peso = 3,
            Euro = 4
        }

        public enum InvoiceCategories : int
        {
            Spent = 1,
            InCredits = 2
        }

        public enum AttachmentTypes : int
        {
            Pdf = 1,
            Excel = 2
        }

        public enum MoneyInMethods : int
        {
            WireTansfer = 1,
            Cache = 2,
            Debit = 3,
            AutomatedWireTransfer = 4
        }

        public enum MoneyInUsages : int
        {
            Settlement = 1,
            Payment = 2,
            Deposit = 3
        }

        public enum PaidInvoiceEffects : int
        {
            Full = 1,
            PartialStart = 2,
            Partial = 3,
            PartialEnd = 4
        }
        public enum PaidCategories : int
        {
            Incredits = 1,
            Settlements = 2
        }
        public enum SettlementTypes : int
        {
            Normal_Settlement = 1,
            Bank_Feeds_Adjustment = 2
        }

        public enum LimitTypes : int
        {
            CreditLine = 1,
            Temp = 2,
            AdvancePayment = 3,
            OtherCredits = 4,
            PastCredits = 6
        }
        public enum TargetTypes : int
        {
            Company = 1,
            User = 2,
            Card = 3
        }

        public enum IncreditTypes : int
        {
            advance_payment = 1,
            past_credits = 2,
            other_credits = 3
        }

        public enum invoiceStatus : int
        {
            past_due = 1,
            paid = 2,
            paid_late = 3,
            due_now = 4
        }
        public enum InvoiceDetailUsage : int
        {
            TotalInvoice = 1,
            InvoiceCurrencies = 2
        }
        public enum IncreditsUsage : int
        {
            TotalInvoice = 1,
            InvoiceCurrencies = 2
        }

        public enum AttachmentCategories : int
        {
            FromInvoice = 1,
            FromDownloadAttachement = 2

        }


        public enum NotificationCategories : int
        {
            [Description("Advance Payment")]
            advance_payments = 1,
            [Description("Monthly Statement")]
            monthly_statements = 2,
            [Description("Payment reminder")]
            payment_reminders = 3,
            [Description("Payment Confirmation")]
            payment_confirmations = 4,
            [Description("Overdue Payment")]
            overdue_payments = 5,
            [Description("Declined Transaction")]
            declined_transactions = 6,
            [Description("Approved Transaction")]
            approved_transactions = 7
        }
        public enum NotificationCategoriesTypes : int
        {
            //Types of monthly statement category
            MonthlyStatement = 1,
            MidmonthStatement = 2,
            //Payment reminder
            PaymentReminder = 3,
            PaymentReminderFirst = 4,
            PaymentReminderSecond = 5,
            OverDueReminderFirst = 6,
            OverDueReminderSecond = 7,
            DeclineInactiveCompany = 8,
            OverDueReminderThird = 9
        }
        public enum TransactionType : int
        {
            Exceededcardlimit = 1,
            UserLimitAvailable = 2,
            Exceededcompanyaccountlimit = 3,
            Companyinactive = 4,
            Suspendedcard = 5,
            singleusedcard = 6

        }
        public enum UserTypesFromEmail : int
        {
            SuperAdmin = 0,
            User = 1,
            AdminFromUser = 2

        }
        public enum DeclineReasons
        {
            [Description("Card is suspended")]
            CardSuspend = 1,
            [Description("Exceeded card limit")]
            ExceedCardLimit = 2,
            [Description("Exceeded cardholder account limit")]
            ExceedCardHolderLimit = 3,
            [Description("Exceeded company account limit")]
            ExceedCompanyAccountLimit = 4,
            [Description("Company account is inactive")]
            CompanyAccountInActive = 5,
            [Description("This 'single use' card has already been used")]
            SingleCardUsage = 6,
            [Description("Unverified cardholder account")]
            UnverifiedCardHolder = 7,
            [Description("Expired card")]
            CardExpired = 8

        }
        public enum TriggerType
        {
            first,
            Second,
            Third
        }
        public enum ReminderType
        {
            PaymentReminder = 1,
            OverDueReminder = 2
        }

        public enum CompanyAliasTypes
        {
            LigalName = 0,
            LegalAddress = 1,
            BillingAddress = 2,
            TaxID = 3,
            Attention = 4
        }
        public enum IntegrationName
        {
            Upswot = 1,
            Wallet = 2
        }
        public enum IntegrationSettingTypes
        {
            IntegrationPerRole = 1,
            IntegrationPerUser = 2,
            IntegrationPerCompany = 3
        }

        public enum SystemLanguage
        {
            English = 1,
            Spanish = 2
        }
        public enum InvoiceCalculationTypes
        {
            card_currency = 1,
            merchant = 2
        }

        public enum ProductTypes : int
        {
            All = 1,
            Credit = 2,
            Cash = 3
        }

        public enum PaymentStatus : int
        {
            in_process = 1,
            returned = 2,
            paid = 3,
            failed = 4
        }
        public enum PaymentStatusCC : int
        {
            ready_to_send = 1,
            returned = 2,
            completed = 3,
            failed = 4
        }
        public enum CurrencyCloudWebhookFields : int
        {
            failure_reason = 14,
            failure_returned_amount = 15,
            beneficiary_id = 16,
            conversion_id = 17,
            currency = 18,
            reference = 19,
            reason = 28,
            payment_type = 20,
            payment_date = 21,
            transferred_at = 22,
            authorisation_steps_required = 23,
            creator_contact_id = 24,
            last_updater_contact_id = 25,
            short_reference = 27,
            payment_group_id = 29,
            unique_request_id = 30
        }
        public enum CurrencyCloudFields : int
        {
            failurereason = 14,
            failurereturnedamount = 15,
            beneficiaryid = 16,
            conversionid = 17,
            currency = 18,
            reference = 19,
            reason = 28,
            paymenttype = 20,
            paymentdate = 21,
            transferredat = 22,
            authorisationstepsrequired = 23,
            creatorcontactid = 24,
            lastupdatercontactid = 25,
            shortreference = 27,
            paymentgroupid = 29,
            uniquerequestid = 30
        }
        public enum CCNotificationType
        {
            payment_failed_notification,
            payment_compliance_failed_notification
        }
        public enum PaymentTypes : int
        {
            regular = 1,
            priority = 2,
            Local = 3
        }
        public enum PaymentDetailsTypes : int
        {
            Regular = 1,
            Priority = 2,
            Local = 3
        }
        public enum WireFeesTypes : int
        {
            Free = 1
        }

        public enum PaymentIntegrationTypes : int
        {
            currencyCloud = 1,
            swap = 2,
        }

        public enum PaymentDetails
        {
            MXN = 1,
            SameDay = 2,
        }

        public enum BeneficiaryAllRequiredInfo : int
        {
            Clabe = 1,
            BeneficiaryCompanyName = 2,
            BeneficiaryLastName = 3,
            BeneficiaryFirstName = 4,
            BeneficiaryCountry = 5,
            BeneficiaryCity = 6,
            BeneficiaryAddress = 7,
            BeneficiaryPostcode = 8,
            BeneficiaryStateOrProvince = 9,
            BeneficiaryEntityType = 10,
            BicSwift = 11,
            BranchCode = 12,
            BankCode = 13,
            InstitutionNo = 14,
            Iban = 15,
            SortCode = 16,
            Aba = 17,
            AcctNumber = 18,
            Cnaps = 19,
            Ifsc = 20
        }

        public enum BeneficiaryRequiredInfo : int
        {
            PaymentType = 1,
            BeneficiaryCompanyName = 2,
            BeneficiaryLastName = 3,
            BeneficiaryFirstName = 4,
            BeneficiaryCountry = 5,
            BeneficiaryCity = 6,
            BeneficiaryAddress = 7,
            BeneficiaryPostcode = 8,
            BeneficiaryStateOrProvince = 9,
            BeneficiaryEntityType = 10,
            BicSwift = 11
        }

        public enum CurrencyCloudAttributes : int
        {
            branchcode = 1,
            clabe = 2,
            bankcode = 3,
            institutionno = 4,
            bsbcode = 5,
            iban = 6,
            sortcode = 7,
            aba = 8,
            cnaps = 10,
            ifsc = 11,
            bankname = 12,
            acctnumber = 13,
            rfc = 31
        }
        public enum BeneficiaryEntityType
        {
            company
        }
        public enum SwapCurrency
        {
            MXN = 1
        }
        public enum RoutingCode
        {
            [Description("bsb_code")]
            bsbcode,
            [Description("institution_no")]
            institutionno,
            [Description("bank_code")]
            bankcode,
            [Description("branch_code")]
            branchcode,
            [Description("sort_code")]
            sortcode,
        }
        public enum FeaturesType : int
        {
            AddFunds = 1,
            SendPayments = 2,
        }
        public enum GetTransactionsType : int
        {
            All = 1,
            Approve = 2,
            Decline = 3
        }
        public enum Validationobjects : int
        {
            UserDuration = 1,
            CardDuration = 2
        }
        public enum CompanyModes : int
        {
            Working = 1,
            Test = 2
        }
        public enum TransactionStatus : int
        {
            Pending = 0,
            Approved = 1,
            Declined = 2,
            Canceled = 3,
            Dispute_Won = 4,
            Dispute_Lost = 5
        }
        public enum DueByUnitTypes : int
        {
            day = 1,
            month = 2
        }
        public enum SuspensionSettings : int
        {
            disallowedReactivation = 1,
            disalloweSuspension = 2
        }

        public enum PaymentReceiver : int
        {
            Swap = 0,
            Mail = 1,
            STP = 2
        }

        public enum ReceivedPaymentStatus
        {
            Recieved = 1,
            SenderUnidentified = 2,
            PaymentSent = 3,
            PaymentNotSent = 4
        }
    }
}
