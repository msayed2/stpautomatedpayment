﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedPaymentsEngine.Entities.Constatnts
{
    public class Constants
    {
        public const long ZeroSpent = 0;
        public const long ZeroBalance = 0;
        public struct CurrcurrenciesIsoCode
        {
            public static string USD = "USD";
            public static string AED = "AED";
            public static string SGD = "SGD";
            public static string MXN = "MXN";
            public static string SAR = "SAR";
        }
        public struct TransactionTypes
        {
            public static string refund = "refund";
            public static string carryOver = "Carry over";
            public static string capture = "capture";
            public static string dispute = "dispute";
        }
        public struct TransactionStates
        {
            public static string Pending = "Pending";
            public static string Approved = "Approved";
            public static string Declined = "Declined";
            public static string Canceled = "Canceled";
            public static string Won = "Won";
            public static string Lost = "Lost";
        }
        public struct PaymentStates
        {
            public static string Pending = "Pending";
            public static string Completed = "Completed";
            public static string Failed = "Failed";
            public static string Returned = "Returned";
            public static string Processing = "Processing";
        }
        public struct UserMessagesTypeIds
        {
            public static int NotificationMonthlyStatement = 2;
            public static int NotificationPaymentConfirmation = 3;
            public static int NotificationApprovedTransactionUser = 17;
            public static int NotificationApprovedTransactionAdmin = 18;
            public static int NotificationAdvancePaymentContent = 16;
            public static int NotificationDeclineCardSuspend = 4;
            public static int NotificationDeclineExceedCardLimit = 5;
            public static int NotificationDeclineExceedCardHolderLimit = 6;
            public static int NotificationDeclineExceedCompanyAccountLimit = 7;
            public static int NotificationDeclineCompanyAccountInActive = 8;
            public static int NotificationDeclineSingleCardUsage = 9;
            public static int NotificationDeclineUnverifiedCardHolder = 10;
            public static int NotificationDeclineCardExpired = 11;
            public static int NotificationPaymentReminderFirstContent = 12;
            public static int NotificationPaymentReminderSecondContent = 13;
            public static int NotificationOverDueReminderFirstContent = 14;
            public static int NotificationOverDueReminderSecondContent = 15;
            public static int NotificationOverDueReminderThirdContent = 167;




        }
        public struct DeclineReasonTypes
        {
            public static string company_inactive = "company_inactive";
            public static string company_limit = "company_limit";
            public static string user_limit = "user_limit";
            public static string card_limit = "card_limit";
            public static string card_inactive = "card_inactive";

            public static string card_stopped = "card_stopped";
            public static string suspended_inactivity = "suspended_inactivity";
            public static string suspended_unpaid_settlement = "suspended_unpaid_settlement";
            public static string account_compliance_disabled = "account_compliance_disabled";
            public static string account_inactive = "account_inactive";
            public static string authentication_failed = "authentication_failed";
            public static string cardholder_inactive = "cardholder_inactive";
            public static string insufficient_funds = "insufficient_funds";
            public static string cardholder_verification_required = "cardholder_verification_required";
            public static string not_allowed = "not_allowed";
            public static string suspected_fraud = "suspected_fraud";
            public static string incorrect_cvc = "incorrect_cvc";
            public static string incorrect_expiry = "incorrect_expiry";
            public static string authorization_controls_blocked_categories = "authorization_controls_blocked_categories";
            public static string authorization_controls_spending_limits = "authorization_controls_spending_limits";
            public static string invalid_merchant = "invalid_merchant";
            public static string profile_inactive = "profile_inactive";
            public static string unknown = "unknown";
            public static string companyUnnormal = "company_unnormal";
            public static string partiallySuspendedCompany = "partially_suspended_company";
            public static string fullySuspendedCompany = "fully_suspended_company";
        }

        public struct HQ
        {
            public static string mexico = "mexico";
        }
    }
}
