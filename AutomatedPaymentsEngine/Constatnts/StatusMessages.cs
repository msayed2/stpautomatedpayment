﻿using Entities.Results;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Constants
{
    public static class StatusMessages
    {
      /*  public static DBMessagesConfig objDBMessage = new DBMessagesConfig()
        {
            data = Globals.Global.DBConfig.data
        };*/

        public static string GetMessage(string code, int langId)
        {
            return Globals.Global.DBConfig.data.systemMessage.Where(I => I.systemMessageTypes.name == code && I.langauge.id == langId).FirstOrDefault()?.messageName;            
        }

        // Success
        public static Status Success = new Status() { is_success = true };

        // General errors E0XX
        public static Status Unknown = new Status() { is_success = false, error_code = "E001", error_message = ConfigurationManager.AppSettings["E001"] };
        public static Status InvalidParams = new Status() { is_success = false, error_code = "E002", error_message = ConfigurationManager.AppSettings["E002"] };
        public static Status NotExistGetResult = new Status() { is_success = false, error_code = "E003", error_message = ConfigurationManager.AppSettings["E003"] };
        public static Status PageNotFound = new Status() { is_success = false, error_code = "E004", error_message = ConfigurationManager.AppSettings["E004"] };
        public static Status UnAuthorized = new Status() { is_success = false, error_code = "E005", error_message = ConfigurationManager.AppSettings["E005"] };
        public static Status NotAllowedRole = new Status() { is_success = false, error_code = "E006", error_message = ConfigurationManager.AppSettings["E006"] };
        public static Status InvalidDateFormats = new Status() { is_success = false, error_code = "E007", error_message = ConfigurationManager.AppSettings["E007"] };
        public static Status InvalidUserName = new Status() { is_success = false, error_code = "E008", error_message = ConfigurationManager.AppSettings["E008"] };

        // Login errors E1XX
        public static Status InvalidMailOrPassword = new Status() { is_success = false, error_code = "E101", error_message = ConfigurationManager.AppSettings["E101"] };
        public static Status InvalidSMSCode = new Status() { is_success = false, error_code = "E102", error_message = ConfigurationManager.AppSettings["E102"] };
        public static Status AlreadyExistingEmail = new Status() { is_success = false, error_code = "E103", error_message = ConfigurationManager.AppSettings["E103"] };
        public static Status InvalidDeviceId = new Status() { is_success = false, error_code = "E104", error_message = ConfigurationManager.AppSettings["E104"] };
        public static Status InvalidCodeId = new Status() { is_success = false, error_code = "E105", error_message = ConfigurationManager.AppSettings["E105"] };
        public static Status UnknownCompany = new Status() { is_success = false, error_code = "E106", error_message = ConfigurationManager.AppSettings["E106"] };
        public static Status InvalidUserTypeId = new Status() { is_success = false, error_code = "E107", error_message = ConfigurationManager.AppSettings["E107"] };
        public static Status ConfirmationEmailSent = new Status() { is_success = true, error_code = "E108", error_message = ConfigurationManager.AppSettings["E108"] };
        public static Status InvalidConfirmationToken = new Status() { is_success = false, error_code = "E109", error_message = ConfigurationManager.AppSettings["E109"]  };
        public static Status SomethingWrong = new Status() { is_success = false, error_code = "E110", error_message = ConfigurationManager.AppSettings["E110"] };
        public static Status InvalidOldPassword = new Status() { is_success = false, error_code = "E111", error_message = ConfigurationManager.AppSettings["E111"] };
        public static Status InvalidEmail = new Status() { is_success = false, error_code = "E112", error_message = ConfigurationManager.AppSettings["E112"] };
        public static Status InvalidIntervalTime = new Status() { is_success = false, error_code = "E113", error_message = ConfigurationManager.AppSettings["E113"] };
        public static Status PhoneIsAlreadyExist = new Status() { is_success = false, error_code = "E114", error_message = ConfigurationManager.AppSettings["E114"] };
        public static Status PhoneNumberIsNotConfirmed = new Status() { is_success = false, error_code = "E115", error_message = ConfigurationManager.AppSettings["E115"] };
        public static Status UserDoesNotHavePhoneNumber = new Status() { is_success = false, error_code = "E116", error_message = ConfigurationManager.AppSettings["E116"] };
        public static Status inValidMobileNumber = new Status() { is_success = false, error_code = "E117", error_message = ConfigurationManager.AppSettings["E117"] };
        public static Status InActivteCompany = new Status() { is_success = false, error_code = "E119", error_message = ConfigurationManager.AppSettings["E119"] };
        public static Status UnPaidSettlementCompany = new Status() { is_success = false, error_code = "E120", error_message = ConfigurationManager.AppSettings["E120"] };



        // Cards erros E2XX
        public static Status NotAllowedPhysicalPin = new Status() { is_success = false, error_code = "E201", error_message = ConfigurationManager.AppSettings["E201"] };
        public static Status NotAllowedVirtualReportasLost = new Status() { is_success = false, error_code = "E202", error_message = ConfigurationManager.AppSettings["E202"] };
        public static Status CardMarkedAsStolenorLost = new Status() { is_success = false, error_code = "E203", error_message = ConfigurationManager.AppSettings["E203"] };
        public static Status CardHaveSameStatus = new Status() { is_success = false, error_code = "E204", error_message = ConfigurationManager.AppSettings["E204"] };
        public static Status InSufficientUserLimit = new Status() { is_success = false, error_code = "E205", error_message = ConfigurationManager.AppSettings["E205"] };
        public static Status CardLimitlessThanMinLimit = new Status() { is_success = false, error_code = "E206", error_message = ConfigurationManager.AppSettings["E206"] };
        public static Status InsufficientCompanyLimit = new Status() { is_success = false, error_code = "E207", error_message = ConfigurationManager.AppSettings["E207"] };
        public static Status UserLimitMustBeGreaterThanTen = new Status() { is_success = false, error_code = "E208", error_message = ConfigurationManager.AppSettings["E208"] };
        public static Status CardNameMustBeLessThan50 = new Status() { is_success = false, error_code = "E209", error_message = ConfigurationManager.AppSettings["E209"] };
        public static Status AlreadyExistingContactCardName = new Status() { is_success = false, error_code = "E210", error_message = ConfigurationManager.AppSettings["E210"] };
        public static Status CanNotUpdateWithSameName = new Status() { is_success = false, error_code = "E211", error_message = ConfigurationManager.AppSettings["E211"] };
        public static Status UserLimitMustBeGreaterThanSpent = new Status() { is_success = false, error_code = "E212", error_message = ConfigurationManager.AppSettings["E212"] };
        public static Status NotAllowedIntervalId = new Status() { is_success = false, error_code = "E213", error_message = ConfigurationManager.AppSettings["E213"] };
        public static Status CardWithSameNameAlreadyExist = new Status() { is_success = false, error_code = "E214", error_message = ConfigurationManager.AppSettings["E214"] };
        public static Status EmailandPhoneNotUpdated = new Status() { is_success = false, error_code = "E215", error_message = ConfigurationManager.AppSettings["E215"] };
        public static Status PhoneNotUpdated = new Status() { is_success = false, error_code = "E216", error_message = ConfigurationManager.AppSettings["E216"] };
        public static Status UserAlreadyHaveThisEmail = new Status() { is_success = false, error_code = "E217", error_message = ConfigurationManager.AppSettings["E217"] };
        public static Status UserAlreadyHaveThisPhone = new Status() { is_success = false, error_code = "E218", error_message = ConfigurationManager.AppSettings["E218"] };
        public static Status UserMustIncludenewLimit = new Status() { is_success = false, error_code = "E219", error_message = ConfigurationManager.AppSettings["E219"] };
        public static Status CannotUpdateAdminorSuperAdminLimit = new Status() { is_success = false, error_code = "E220", error_message = ConfigurationManager.AppSettings["E220"] };
        public static Status SameUserRole = new Status() { is_success = false, error_code = "E221", error_message = ConfigurationManager.AppSettings["E221"] };
        public static Status UnSupportedCurrency = new Status() { is_success = false, error_code = "E222", error_message = ConfigurationManager.AppSettings["E222"] };
        public static Status NotExistingCard = new Status() { is_success = false, error_code = "E223", error_message = ConfigurationManager.AppSettings["E223"] };


        // Settings Controller E3XX
        public static Status AlreadyExistingContact = new Status() { is_success = false, error_code = "E301", error_message = ConfigurationManager.AppSettings["E301"] };
        public static Status LastContactCanNotBeRemoved = new Status() { is_success = false, error_code = "E302", error_message = ConfigurationManager.AppSettings["E302"] };
        public static Status AlreadyExistingDepartment = new Status() { is_success = false, error_code = "E303", error_message = ConfigurationManager.AppSettings["E303"] };
        public static Status InvalidDepartmentId = new Status() { is_success = false, error_code = "E304", error_message = ConfigurationManager.AppSettings["E304"] };
        // Company controller
        public static Status AlreadyExistingCompanyName = new Status() { is_success = false, error_code = "E401", error_message = ConfigurationManager.AppSettings["E401"] };

        // Actions controller

        public static Status UnSupportedAction = new Status() { is_success = false, error_code = "E501", error_message = ConfigurationManager.AppSettings["E501"] };
        public static Status UnSupportedEvent = new Status() { is_success = false, error_code = "E5012", error_message = ConfigurationManager.AppSettings["E5012"] };
        public static Status SigningNotComplete = new Status() { is_success = false, error_code = "E5013", error_message = ConfigurationManager.AppSettings["E5013"] };
        public static Status NoPendingActions = new Status() { is_success = false, error_code = "E5014", error_message = ConfigurationManager.AppSettings["E5014"] };
        public static Status inValidPendingActionId = new Status() { is_success = false, error_code = "E5015", error_message = ConfigurationManager.AppSettings["E5015"] };
        public static Status inValidPendingActionType= new Status() { is_success = false, error_code = "E5016", error_message = ConfigurationManager.AppSettings["E5016"] };
        public static Status inValidDocumnetType = new Status() { is_success = false, error_code = "E5017", error_message = ConfigurationManager.AppSettings["E5017"] };
        public static Status inValidUserId = new Status() { is_success = false, error_code = "E5018", error_message = ConfigurationManager.AppSettings["E5018"] };
        public static Status UserAlreadyHaveThisPendingAction = new Status() { is_success = false, error_code = "E5019", error_message = ConfigurationManager.AppSettings["E5019"] };


        // Invoice 
        public static Status AlreadyExistingMonthlyInvoice = new Status() { is_success = false, error_code = "E601", error_message = ConfigurationManager.AppSettings["E601"] };
        public static Status CanNotCreateMidMonthInvoice = new Status() { is_success = false, error_code = "E602", error_message = ConfigurationManager.AppSettings["E602"] };

        // currency
        public static Status CurrencyNotSupportedInSystem = new Status() { is_success = false, error_code = "E701", error_message = ConfigurationManager.AppSettings["E701"] };
        public static Status CurrencyNotSupportedInCompany = new Status() { is_success = false, error_code = "E702", error_message = ConfigurationManager.AppSettings["E702"] };
        public static Status AlreadyExistingCurrency = new Status() { is_success = false, error_code = "E703", error_message = ConfigurationManager.AppSettings["E703"] };
        public static Status AlreadyExistingSystemCurrency = new Status() { is_success = false, error_code = "E704", error_message = ConfigurationManager.AppSettings["E704"] };


        public static Status InvalidStettlementParmas { get; internal set; }


        // payments
        public static Status PaymentNotCreated = new Status() { error_code = "E801" };

        public static Status BeneficairyNotFound = new Status() { error_code = "E802" };
        public static Status ExceededCompanyLimit= new Status() { error_code = "E803" };
        public static Status ExceededUserLimit = new Status() { error_code = "E804" };
        public static Status InvalidClabeNumber = new Status() { error_code = "E805" };
        public static Status InvalidAmount = new Status() { error_code = "E806" };
        public static Status InvalidBankName = new Status() { error_code = "E807" };
        public static Status UnKnownPaymentError = new Status() { error_code = "E808" };
        public static Status ExceededCompanyUsersNumber = new Status() { is_success = false, error_code = "E901" };
        public static Status CurrecnyCloudError = new Status() { is_success = false };
        public static Status BeneficiaryDetailsError = new Status() { is_success = false , error_code = "data_not_consistant" };
        public static Status BeneficiaryIsExist = new Status() { is_success = false , error_code = "beneficiary_name_exist" };
        public static Status AlreadyExistPayment = new Status() { is_success = false , error_message = ConfigurationManager.AppSettings["AlreadyExistPayment"] };



    }
}
