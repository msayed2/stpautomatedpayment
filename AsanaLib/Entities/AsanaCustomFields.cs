﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsanaLib.Entities
{
    public class AsanaCustomFields
    {
        public string gid { get; set; }
        public string text_value { get; set; }
    }
}
