﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsanaLib.Entities
{
    public class Task
    {
        public Task()
        {

            //custom_fields = new Dictionary<string, object>();
            notes = "";
            name = "";
            html_notes = "";
        }
        public string assignee { get; set; }
        public string notes { get; set; }
        public string html_notes { get; set; }
        public string due_at { get; set; }
        public string name { get; set; }
        //public List<string> followers { get; set; }
        //public Dictionary<string, object> custom_fields { get; set; }
    }

    public class AddTask : Task
    {
        public AddTask()
        {
            followers = new List<string>();
            projects = new List<string>();
        }
        public List<string> followers { get; set; }
        public List<string> projects { get; set; }
        public string workspace { get; set; }
    }

    public class UpdateTask : Task
    {
        public string taskId { get; set; }
    }
}
