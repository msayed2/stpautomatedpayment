﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsanaLib.Entities
{
    public class SubTaskWebHook
    {
        public SubTaskWebHook()
        {
            filters = new List<Filter>();
        }
        public string resource { get; set; }
        public string target { get; set; }
        public List<Filter> filters { get; set; }
    }

    public class Filter
    {
        public Filter()
        {
            fields = new List<string>();
        }
        public string action { get; set; }
        public List<string> fields { get; set; }
        public string resource_type { get; set; }

    }
}
