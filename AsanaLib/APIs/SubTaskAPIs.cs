﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AsanaLib.Entities;
using Spacegene.Logger;
using System.Reflection;
using System.Net;

namespace AsanaLib.APIs
{
    public class SubTaskAPIs : AsanaAPIs
    {

        public static dynamic CreateSubTask(Task objSubTask)
        {
            try
            {
                dynamic result = POST($"tasks/{MainTaskId}/subtasks", objSubTask);
                JObject createdTask = JObject.FromObject(result);
                return createdTask["data"]["gid"].ToString();
            }
            catch (Exception ex)
            {
                
                string sub_task_details = "";

                if (objSubTask != null)
                {
                    sub_task_details = JsonConvert.SerializeObject(objSubTask);
                }
                new Logger().LogError($"Error creating subtask - details: {sub_task_details} - Exception data: {JsonConvert.SerializeObject(ex.Data)}", ex);

                return null;
            }
        }

        public static dynamic GetSubTask(string TaskId)
        {
            try
            {
                dynamic result = GET("tasks/" + TaskId);
                JObject objTask = JObject.FromObject(result);
                //return objTask["data"]["custom_fields"]
                //return objTask["data"]["assignee"]["name"].ToString();
                return "7980cfd6-5c47-4471-b08f-1d27c12513ac";
            }
            catch (System.Exception ex)
            {
                new Logger().LogError(string.Format("31829c92-e37c-4047-bf48-3b381c1673e4 Error occur during GetSubTask in asana in function {0} ", MethodBase.GetCurrentMethod().Name), ex);

                return null;
            }

        }

        public static bool UpdateSubTask(string taskId, dynamic data)
        {
            try
            {
                dynamic result = PUT("tasks/" + taskId, data);
                return true;
            }
            catch (System.Exception ex)
            {
                string task_details = "";

                if (data != null)
                {
                    task_details = JsonConvert.SerializeObject(data);
                }

                new Logger().LogError($"e9fde756-6a65-4b2c-82eb-1b363cbab5ca Error in update Asana Sub task, Sub task details:{task_details}", ex);
                return false;
            }
        }
    }
}
