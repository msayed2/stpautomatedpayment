﻿using Newtonsoft.Json.Linq;
using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AsanaLib.APIs
{
    public class CommentsAPIs : AsanaAPIs
    {
        public dynamic AddComment(string text, string TaskID)
        {
            try
            {
                new Logger().LogInfo("Start Post Asana Comment");
                dynamic newTask = new { html_text = $"<body>{text}</body>" };
                dynamic result = POST("tasks/" + TaskID + "/stories", newTask);
                JObject createdComment = JObject.FromObject(result);
                new Logger().LogInfo("End Post Asana Comment");
                return createdComment["data"]["gid"].ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("9332e64c-5efc-404f-9505-6a0a835c1d97 Error occur during Post in asana in function {0}", MethodBase.GetCurrentMethod().Name), ex);
                return null;
            }
        }
    }
}
