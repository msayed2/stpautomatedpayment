﻿using AsanaLib.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AsanaLib.APIs
{
    public class AsanaAPIs
    {
        private static string baseURL = "";
        public static string userToken = "";
        public static string WorkspaceId = "";
        public static string TribalProjectId = "";
        public static string MainTaskId = "";
        public static string userID = "";
        public static string SubTaskHookookAction = "";
        public static string SubTaskHookookField = "";
        public static string SubTaskHookookResource_type = "";

        static AsanaAPIs()
        {
            baseURL = ConfigurationManager.AppSettings.Get("baseURL");
            userToken = ConfigurationManager.AppSettings.Get("userToken");
            WorkspaceId = ConfigurationManager.AppSettings.Get("WorkspaceId");
            TribalProjectId = ConfigurationManager.AppSettings.Get("TribalProjectId");
            MainTaskId = ConfigurationManager.AppSettings.Get("MainTaskId");
            userID = ConfigurationManager.AppSettings.Get("userID");
            SubTaskHookookAction = ConfigurationManager.AppSettings.Get("SubTaskHookookAction");
            SubTaskHookookField = ConfigurationManager.AppSettings.Get("SubTaskHookookField");
            SubTaskHookookResource_type = ConfigurationManager.AppSettings.Get("SubTaskHookookResource_type");
        }

        public struct Description
        {
            
        }

        public struct CustomFields
        {
            public static string companyId = ConfigurationManager.AppSettings.Get("companyId");

        }

        protected static dynamic POST(string apiPath, dynamic content, Dictionary<string, string> headers = null)
        {
            HttpWebRequest profileRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", baseURL, apiPath));

            profileRequest.Headers["Authorization"] = string.Format("Bearer {0}", userToken);
            
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    profileRequest.Headers.Add(header.Key, header.Value);
                }
            }
            profileRequest.Accept = "application/json";
            profileRequest.ContentType = "application/json";
            dynamic formattedContent = new { data = content };
            string text = JsonConvert.SerializeObject(formattedContent);

            byte[] bytes = Encoding.UTF8.GetBytes(text);
            profileRequest.Method = WebRequestMethods.Http.Post;
            profileRequest.ContentLength = bytes.Length;

            using (Stream requeststream = profileRequest.GetRequestStream())
            {
                requeststream.Write(bytes, 0, bytes.Length);
                requeststream.Close();
            }
            string response;
            dynamic formattedResponse;

            using (HttpWebResponse webResponse = (HttpWebResponse)profileRequest.GetResponse())
            {
                using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
                {
                    response = sr.ReadToEnd();
                    formattedResponse = JsonConvert.DeserializeObject(response);
                    sr.Close();
                }
                webResponse.Close();
            }
            return formattedResponse;
        }

        protected static dynamic GET(string apiPath)
        {
            HttpWebRequest profileRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", baseURL + "/tasks", apiPath));
            profileRequest.Headers["Authorization"] = string.Format("Bearer {0}", userToken);
            profileRequest.Headers.Add("Asana-Disable", "string_ids");
            profileRequest.Accept = "application/json";
            profileRequest.ContentType = "application/json";
            profileRequest.Method = "GET";

            string response;
            dynamic formattedResponse;

            using (HttpWebResponse webResponse = (HttpWebResponse)profileRequest.GetResponse())
            {
                using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
                {
                    response = sr.ReadToEnd();
                    formattedResponse = JsonConvert.DeserializeObject(response);
                    sr.Close();
                }
                webResponse.Close();
            }
            return formattedResponse;
        }

        protected static dynamic PUT(string apiPath, dynamic content)
        {
            HttpWebRequest profileRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}/{1}", baseURL, apiPath));

            profileRequest.Headers["Authorization"] = string.Format("Bearer {0}", userToken);
            profileRequest.Accept = "application/json";
            profileRequest.ContentType = "application/json";
            dynamic formattedContent = new { data = content };
            string text = JsonConvert.SerializeObject(formattedContent);
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            profileRequest.Method = "PUT";
            profileRequest.ContentLength = bytes.Length;

            using (Stream requeststream = profileRequest.GetRequestStream())
            {
                requeststream.Write(bytes, 0, bytes.Length);
                requeststream.Close();
            }
            string response;
            dynamic formattedResponse;

            using (HttpWebResponse webResponse = (HttpWebResponse)profileRequest.GetResponse())
            {
                using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
                {
                    response = sr.ReadToEnd();
                    formattedResponse = JsonConvert.DeserializeObject(response);
                    sr.Close();
                }
                webResponse.Close();
            }
            return formattedResponse;
        }

        public string GetCompanyId(dynamic objAsanaTask, out bool? status)
        {
            string value = null;
            status = null;
            try
            {
                JObject asanaTask = JObject.FromObject(objAsanaTask);
                JArray customfieldsList = (JArray)asanaTask["data"]["custom_fields"];
                IList<AsanaCustomFields> asanaCustomFieldList = customfieldsList.ToObject<IList<AsanaCustomFields>>();
                string objCompanyFieldId = ConfigurationManager.AppSettings.Get("CompanyUID");
                AsanaCustomFields objAsanaCustomField = asanaCustomFieldList.FirstOrDefault(I => I.gid == objCompanyFieldId);
                if (objAsanaCustomField != null)
                {
                    value = objAsanaCustomField.text_value;
                    status = true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogError(string.Format("0776e982-35cc-47b3-8402-8434cf280511 Error occur when Get Company UID in function {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return value;
        }
    }
}
