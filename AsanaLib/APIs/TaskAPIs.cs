﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AsanaLib.Entities;
using Spacegene.Logger;
using System.Reflection;

namespace AsanaLib.APIs
{
    public class TaskAPIs : AsanaAPIs
    {
        public static dynamic CreateTask(Task objTask)
        {
            try
            {
                dynamic result = POST("tasks", objTask);
                JObject createdTask = JObject.FromObject(result);
                return createdTask["data"]["gid"].ToString();
            }
            catch (System.Exception ex)
            {
                string task_details = "";

                if (objTask != null)
                {
                    task_details = JsonConvert.SerializeObject(objTask);
                }
                new Logger().LogError(string.Format("75904e81-31b1-423f-8b6e-ce7da7693afb Error occur during Create Task in asana in function {0} task details is {1}", MethodBase.GetCurrentMethod().Name, task_details), ex);

                return null;
            }
        }
    }
}
