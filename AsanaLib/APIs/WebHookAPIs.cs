﻿using AsanaLib.Entities;
using Newtonsoft.Json.Linq;
using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsanaLib.APIs
{
    public class WebHookAPIs:AsanaAPIs
    {
        public dynamic CreateWebhook(string resource, string target)
        {
            try
            {
                //List<string> fields = new List<string> { SubTaskHookookField };
                //var filter = new Filter { action = SubTaskHookookAction, fields = fields, resource_type = SubTaskHookookResource_type };
                //var filters = new List<Filter> { filter };
                //dynamic subTaskHook = new SubTaskWebHook { resource = resource, target = target ,filters = filters };
                dynamic webhockRequest = new { resource = resource, target = target };
                dynamic result = POST("webhooks", webhockRequest);
                JObject createdTask = JObject.FromObject(result);
                return createdTask["data"]["gid"].ToString();
            }
            catch (System.Exception ex)
            {
                new Logger().LogError("eed129d2-6005-4a5c-9d79-9f1902857329 Error in create asana task webhook ", ex);
                return null;
            }
        }
    }
}
