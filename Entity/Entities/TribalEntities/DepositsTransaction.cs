//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DepositsTransaction
    {
        public string Id { get; set; }
        public double Amount { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public int StatusId { get; set; }
        public string CompanyDepositId { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public System.DateTime UpdatedAt { get; set; }
        public string Payer { get; set; }
        public bool Active { get; set; }
        public double MerchantAmount { get; set; }
        public string MerchantCurrency { get; set; }
    
        public virtual CompanyDeposit CompanyDeposit { get; set; }
    }
}
