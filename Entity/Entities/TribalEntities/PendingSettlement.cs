//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PendingSettlement
    {
        public string Id { get; set; }
        public double Amount { get; set; }
        public bool IsActive { get; set; }
        public string OriginalSettlementId { get; set; }
        public string CurrencyId { get; set; }
    
        public virtual Currency Currency { get; set; }
        public virtual Settlement Settlement { get; set; }
    }
}
