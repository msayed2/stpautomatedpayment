//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class BeneficiariesHistory
    {
        public string Id { get; set; }
        public string CityId { get; set; }
        public string CountryId { get; set; }
        public string StateId { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> UpdatedAt { get; set; }
        public string CurrencyId { get; set; }
        public string Name { get; set; }
        public string BankId { get; set; }
        public Nullable<int> IntegrationId { get; set; }
        public bool IsActive { get; set; }
        public string BankAccountHolderName { get; set; }
        public string BeneficiaryId { get; set; }
        public string Email { get; set; }
        public string BeneficiaryIdCC { get; set; }
    
        public virtual Beneficiary Beneficiary { get; set; }
    }
}
