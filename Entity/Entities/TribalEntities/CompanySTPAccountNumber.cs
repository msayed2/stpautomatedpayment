//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompanySTPAccountNumber
    {
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public string AccountNumberId { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public System.DateTime UpdatedAt { get; set; }
        public bool IsActive { get; set; }
    }
}
