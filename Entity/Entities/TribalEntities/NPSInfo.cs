//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class NPSInfo
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public System.DateTime NPSMonth { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> NPS { get; set; }
        public string feedback { get; set; }
        public string CompanyId { get; set; }
        public string UserId { get; set; }
    }
}
