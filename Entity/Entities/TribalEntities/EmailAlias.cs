//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmailAlias
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EmailAlias()
        {
            this.DigitalWalletMessages = new HashSet<DigitalWalletMessage>();
        }
    
        public string Id { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public Nullable<bool> IsCurrent { get; set; }
        public string ProfileId { get; set; }
        public int TypeId { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public System.DateTime UpdatedAt { get; set; }
        public string FullName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DigitalWalletMessage> DigitalWalletMessages { get; set; }
        public virtual EmailAliasType EmailAliasType { get; set; }
        public virtual Profile Profile { get; set; }
    }
}
