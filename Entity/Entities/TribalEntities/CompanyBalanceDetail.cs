//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompanyBalanceDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompanyBalanceDetail()
        {
            this.CompanyBalanceDetailsSettlements = new HashSet<CompanyBalanceDetailsSettlement>();
        }
    
        public string Id { get; set; }
        public string BillingId { get; set; }
        public string CompanyId { get; set; }
        public double InitialAmount { get; set; }
        public double FinalAmount { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public System.DateTime UpdatedAt { get; set; }
        public bool IsActive { get; set; }
        public string InvoiceId { get; set; }
    
        public virtual CompanyBilling CompanyBilling { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyBalanceDetailsSettlement> CompanyBalanceDetailsSettlements { get; set; }
    }
}
