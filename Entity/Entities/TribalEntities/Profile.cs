//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Profile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Profile()
        {
            this.EmailAliases = new HashSet<EmailAlias>();
            this.IntegrationsPerUsers = new HashSet<IntegrationsPerUser>();
            this.UserBillings = new HashSet<UserBilling>();
            this.UserCashBillings = new HashSet<UserCashBilling>();
            this.UserCreditBillings = new HashSet<UserCreditBilling>();
            this.UserDurationBillings = new HashSet<UserDurationBilling>();
            this.UserDurationBillingPerDays = new HashSet<UserDurationBillingPerDay>();
            this.UserDurationSettings = new HashSet<UserDurationSetting>();
        }
    
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserId { get; set; }
        public string StripeId { get; set; }
        public string CompanyId { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public System.DateTime UpdatedAt { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> InitialTypeId { get; set; }
        public string LinkedIn { get; set; }
        public double Limit { get; set; }
        public int CurrentTypeId { get; set; }
        public int StatusId { get; set; }
        public string Title { get; set; }
        public string DepartmentId { get; set; }
        public string MobileNumber { get; set; }
        public string MobileCode { get; set; }
        public Nullable<double> LastLimit { get; set; }
        public bool PendingAgreement { get; set; }
        public bool EnableWallet { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string NameOnCard { get; set; }
        public int LanguageId { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual Company Company { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmailAlias> EmailAliases { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntegrationsPerUser> IntegrationsPerUsers { get; set; }
        public virtual Language Language { get; set; }
        public virtual UserRole UserRole { get; set; }
        public virtual UserState UserState { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserBilling> UserBillings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCashBilling> UserCashBillings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCreditBilling> UserCreditBillings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserDurationBilling> UserDurationBillings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserDurationBillingPerDay> UserDurationBillingPerDays { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserDurationSetting> UserDurationSettings { get; set; }
    }
}
