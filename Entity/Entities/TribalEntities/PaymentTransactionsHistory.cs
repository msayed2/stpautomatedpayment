//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentTransactionsHistory
    {
        public string Id { get; set; }
        public double Amount { get; set; }
        public System.DateTime CreatedAt { get; set; }
        public System.DateTime UpdatedAt { get; set; }
        public string OriginalPaymentId { get; set; }
        public int PaymentTypeId { get; set; }
        public double MerchantAmount { get; set; }
        public string BeneficiaryId { get; set; }
        public string CompanyId { get; set; }
        public string UserId { get; set; }
        public int StatusId { get; set; }
        public string Description { get; set; }
    
        public virtual Payment Payment { get; set; }
    }
}
