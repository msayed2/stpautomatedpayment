//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Entities.TribalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Device
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Device()
        {
            this.DeviceLogs = new HashSet<DeviceLog>();
        }
    
        public string DeviceId { get; set; }
        public string UserId { get; set; }
        public string DeviceName { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> RememberMe { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeviceLog> DeviceLogs { get; set; }
    }
}
