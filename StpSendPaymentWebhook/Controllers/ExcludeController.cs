﻿
using BOs;
using Entities.Parameters;
using Entities.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace STPSendPaymentWebhook.Controllers
{
    [RoutePrefix("api")]
    public class ExcludeController : ApiController
    {
        [HttpPost]
        [Route("exclude-company")]
        public BaseResult ExcludeCompany(ExcludeCompanyParams model)
        {
            BaseResult objResult = null;
            objResult = new ExcludeListBO().ExcludeCompany(model);
            return objResult;
        }
    }
}
