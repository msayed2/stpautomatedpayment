﻿using AutomatedPaymentsEngine;
using BOs;
using Entities.Parameters;
using Entities.Results;
using Newtonsoft.Json;
using Spacegene.Logger;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace STPSendPaymentWebhook.Controllers
{
    [RoutePrefix("api/webhook")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WebhookController : ApiController
    {
        [HttpPost]
        [Route("send")]
        public BaseResult StpSendPaymentWebhook(StpSendPaymentWebhookParams model)
        {
            BaseResult objResult = null;
            new Logger().LogWarning(JsonConvert.SerializeObject(model));
            objResult = new TribalPaymentBO().SaveStpWebHook(model);
            return objResult;
        }

        [HttpPost]
        [Route("receive")]
        public BaseResult PaymentReceivedStpWebhook(StpReceivePaymentParams model)
        {
            new Logger().LogWarning(JsonConvert.SerializeObject(model));
            BaseResult objResult = new BaseResult();
            objResult = (BaseResult)new TribalPaymentBO().CreateReceivedPayment(model);
            return objResult;
        }

        [HttpPost]
        [Route("receive-email")]
        public BaseResult PaymentReceivedEmail([FromBody] string body)
        {
            BaseResult objResult = new BaseResult();
            var ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (String.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            if (ip == ConfigurationManager.AppSettings.Get("HookRequestIp") || ConfigurationManager.AppSettings.Get("HookRequestIp") == "*")
            {
                objResult = new TribalPaymentBO().CreateEmailReceivedPayment(body);
            }
            return objResult;
        }

        [Route("task-hook/{subtaskId}")]
        [HttpPost]
        public HttpResponseMessage TaskHook(string subtaskId)
        {
            HttpStatusCode objHttpStatusCode = HttpStatusCode.InternalServerError;
            try
            {
                var headers = Request.Headers;
                if (headers.Contains("X-Hook-Secret"))
                {
                    string token = headers.GetValues("X-Hook-Secret").FirstOrDefault();
                    new Logger().LogWarning(string.Format("X-Hook-Secret token is {0} , task id is {1}", token, subtaskId));
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    response.Headers.Add("X-Hook-Secret", token);
                    return response;
                }
                else
                {
                    if (subtaskId != null)
                    {
                        dynamic objResult = new AsanaBO().ProcessUnidentifiedSenderPayment(subtaskId);
                        if (objResult == true)
                        {
                            objHttpStatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            objHttpStatusCode = HttpStatusCode.OK;
                        }
                    }
                    else
                    {
                        objHttpStatusCode = HttpStatusCode.OK;
                    }
                }
            }
            catch(Exception ex)
            {
                new Spacegene.Logger.Logger().LogError(string.Format("e40a7966-dda9-4da0-b9b8-a18f571e8ba9 Error occur during TaskHook in API {0}", MethodBase.GetCurrentMethod().Name), ex);
            }
            return Request.CreateResponse(objHttpStatusCode);
        }
    }
}
