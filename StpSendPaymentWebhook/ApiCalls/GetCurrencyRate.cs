﻿using AutomatedPaymentsEngine.Entities.Parameters;
using Newtonsoft.Json;
using RestSharp;
using Spacegene.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace STPSendPaymentWebhook.ApiCalls
{
    public static class GetCurrencyRate
    {
        public static RestClient restClient = new RestClient(ConfigurationManager.AppSettings["serviceUrl"]);
        public static string key = ConfigurationManager.AppSettings["autKey"];
        public static string apigetcurrencyRate = ConfigurationManager.AppSettings["apiCall"];
        public static string apigethistoriccurrencyRate = ConfigurationManager.AppSettings["apiCallhistoric"];

        public static double GetCurrentCurrencyRate(this string currencyCode)
        {

            CurrencyConversionParam currencyConversion = null;
            try
            {
                if (currencyCode.ToLower() == "usd")
                    return 1;
                var request = new RestRequest(apigetcurrencyRate + "?key=" + key + "&iso=" + currencyCode + "&currencyRateSource=" + 1, Method.GET);

                var response = restClient.Execute(request);

                if (response?.StatusCode == HttpStatusCode.OK)
                    currencyConversion = JsonConvert.DeserializeObject<CurrencyConversionParam>(response?.Content);
            }
            catch (Exception ex)
            {
                new Logger().LogError("Cannot get rate of this currency", ex);

            }

            return currencyConversion.Rate;
        }
    }
}