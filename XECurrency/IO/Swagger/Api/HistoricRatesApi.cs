using System;
using System.Collections.Generic;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IHistoricRatesApi
    {
        /// <summary>
        /// Get historic rates for one base currency against one or more counter currencies. The Currency Data API provides you with access to daily historical rates back to 1998. When retrieving historical rates through our Currency Data API, you would specify your requested currencies along with the date and/or date range and time (hh:mm). If your account is registered for a Live package then you have access to minutely rates for the past 7 days and outside of 7 days you have access to rates on the hour. Historical rates are available with all API packages. The time parameter (hh:mm) only applies to Live packages.  The endpoint returns the historic rate for a single base currency and one or more counter currencies.  &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/historic_rate.csv/?from&#x3D;USD&amp;date&#x3D;2011-03-05&amp;to&#x3D;CAD,EUR\&quot;&gt;https://xecdapi.xe.com/v1/historic_rate.csv/?from&#x3D;USD&amp;date&#x3D;2011-03-05&amp;to&#x3D;CAD,EUR&lt;/a&gt; 
        /// </summary>
        /// <param name="to">Comma separated list of to currencies ISO 4217 codes. This will limit the data returned to only those currencies you are interested in. Use an asterisk * to specify all currencies.  Note: Obsolete currencies are replaced by their precursor or successor currency.  </param>
        /// <param name="date">UTC date should be in the form of YYYY-MM-DD, up to 1995-11-16. If your account is registered for a Daily package your endpoint will return rates at your preferred daily lock-in time. If your account is registered for a Live package your endpoint will return XE mid-day rate unless you specify a time parameter in your call request. </param>
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code. Note if this parameter is omitted, USD is assumed. </param>
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1.00 is assumed. </param>
        /// <param name="time">OPTIONAL – *Time parameter is applicable to Live package only*. UTC time is in format of HH:MM Time option is only available for the last 24 hours, if time is not specified, only one table is returned using the XE mid-day rates (As returned in http://www.xe.com/currencytables/) </param>
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param>
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will display the inverse of the converted value. If ‘false‘ then it will not be displayed.</param>
        /// <returns>HistoricRateResponse</returns>
        HistoricRateResponse HistoricRateGet (string to, string date, string from, decimal? amount, string time, bool? obsolete, bool? inverse);
        /// <summary>
        /// Get historic rates for one base currency against one or more counter currencies over a period of time. This endpoint returns a daily historic rate for a single base currency and one or more counter currencies over a period of time.  &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/historic_rate/period.csv/?from&#x3D;USD&amp;to&#x3D;CAD&amp;start_timestamp&#x3D;2017-09-01&amp;end_timestamp&#x3D;2017-11-30&amp;per_page&#x3D;500\&quot;&gt;https://xecdapi.xe.com/v1/historic_rate/period.csv/?from&#x3D;USD&amp;to&#x3D;CAD&amp;start_timestamp&#x3D;2017-09-01&amp;end_timestamp&#x3D;2017-11-30&amp;per_page&#x3D;500&lt;/a&gt;  Optional parameters available with Live accounts:  &amp;interval&#x3D;minutely &amp;interval&#x3D;hourly  
        /// </summary>
        /// <param name="to">Comma separated list of to currencies based on ISO 4217 codes. This will limit the data returned to only those currencies you are interested in.  Note: Obsolete currencies are replaced by their precursor or successor currency.  </param>
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code.  Note if this parameter is omitted, USD is assumed. </param>
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1.00 is assumed. </param>
        /// <param name="startTimestamp">OPTIONAL - ISO 8601 timestamp in the format yyyy-mm-ddThh:mm giving the UTC date and time of the start of the period for which you would like rates returned.  If your account is registered for a Daily package your endpoint will return rates at your preferred daily lock-in time starting on the date specified in your request.  If your account does not have a preferred daily lock-in time then rates will return as of 00:00 UTC If your account is registered for a Live package your endpoint will return rates starting at 00:00 UTC if no time portion is specified. </param>
        /// <param name="endTimestamp">OPTIONAL – ISO 8601 timestamp in the format yyyy-mm-ddThh:mm giving the UTC date and time of the end of the period for which you would like rates returned. If a time in the future is specified, the current time will be used. If no end_time is specified, the time specified in the  “start_timestamp” paramenter will also be used for the end_timestamp.” If your account is registered for a Daily package your endpoint will return rates at your preferred daily lock-in time ending on the date specified in your request.  If your account does not have a preferred daily lock-in time then rates will return as of 00:00 UTC. If your account is registered for a Live package your endpoint will return rates at 00:00 UTC unless you specify a time parameter in your rate request. </param>
        /// <param name="interval">OPTIONAL – Interval is applicable to Live packages only.  Using one of the interval values below in your call request will return rates for that specific interval within the time period specified.  Example: adding the interval of \&quot;hourly\&quot; will return rates for every hour in the time period you specified.    \&quot;daily\&quot; - Returns one rate for the days specified in your time period,  \&quot;hourly\&quot; - Returns rates for every hour in the time period you specify  If omitted, \&quot;daily\&quot; is used.  This parameter is only used if both the \&quot;start_timestamp\&quot; and \&quot;end_timestamp\&quot; parameters have been specified</param>
        /// <param name="page">OPTIONAL – You can specify the page number you want to request. Note: that page numbering is 1-based (the first page being page 1). Omitting this parameter will return the first page. </param>
        /// <param name="perPage">OPTIONAL – You can specify the number of results per page. The default is 30 results per page with a maximum of 500 results per page. </param>
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param>
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will display the inverse of the converted value. If ‘false‘ then it will not be displayed.</param>
        /// <returns>HistoricRatePeriodResponse</returns>
        HistoricRatePeriodResponse HistoricRatePeriodGet (string to, string from, decimal? amount, string startTimestamp, string endTimestamp, string interval, decimal? page, decimal? perPage, bool? obsolete, bool? inverse);
    }
  
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class HistoricRatesApi : IHistoricRatesApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HistoricRatesApi"/> class.
        /// </summary>
        /// <param name="apiClient"> an instance of ApiClient (optional)</param>
        /// <returns></returns>
        public HistoricRatesApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = Configuration.DefaultApiClient; 
            else
                this.ApiClient = apiClient;
        }
    
        /// <summary>
        /// Initializes a new instance of the <see cref="HistoricRatesApi"/> class.
        /// </summary>
        /// <returns></returns>
        public HistoricRatesApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
    
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            this.ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return this.ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}
    
        /// <summary>
        /// Get historic rates for one base currency against one or more counter currencies. The Currency Data API provides you with access to daily historical rates back to 1998. When retrieving historical rates through our Currency Data API, you would specify your requested currencies along with the date and/or date range and time (hh:mm). If your account is registered for a Live package then you have access to minutely rates for the past 7 days and outside of 7 days you have access to rates on the hour. Historical rates are available with all API packages. The time parameter (hh:mm) only applies to Live packages.  The endpoint returns the historic rate for a single base currency and one or more counter currencies.  &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/historic_rate.csv/?from&#x3D;USD&amp;date&#x3D;2011-03-05&amp;to&#x3D;CAD,EUR\&quot;&gt;https://xecdapi.xe.com/v1/historic_rate.csv/?from&#x3D;USD&amp;date&#x3D;2011-03-05&amp;to&#x3D;CAD,EUR&lt;/a&gt; 
        /// </summary>
        /// <param name="to">Comma separated list of to currencies ISO 4217 codes. This will limit the data returned to only those currencies you are interested in. Use an asterisk * to specify all currencies.  Note: Obsolete currencies are replaced by their precursor or successor currency.  </param> 
        /// <param name="date">UTC date should be in the form of YYYY-MM-DD, up to 1995-11-16. If your account is registered for a Daily package your endpoint will return rates at your preferred daily lock-in time. If your account is registered for a Live package your endpoint will return XE mid-day rate unless you specify a time parameter in your call request. </param> 
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code. Note if this parameter is omitted, USD is assumed. </param> 
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1.00 is assumed. </param> 
        /// <param name="time">OPTIONAL – *Time parameter is applicable to Live package only*. UTC time is in format of HH:MM Time option is only available for the last 24 hours, if time is not specified, only one table is returned using the XE mid-day rates (As returned in http://www.xe.com/currencytables/) </param> 
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param> 
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will display the inverse of the converted value. If ‘false‘ then it will not be displayed.</param> 
        /// <returns>HistoricRateResponse</returns>            
        public HistoricRateResponse HistoricRateGet (string to, string date, string from, decimal? amount, string time, bool? obsolete, bool? inverse)
        {
            
            // verify the required parameter 'to' is set
            if (to == null) throw new ApiException(400, "Missing required parameter 'to' when calling HistoricRateGet");
            
            // verify the required parameter 'date' is set
            if (date == null) throw new ApiException(400, "Missing required parameter 'date' when calling HistoricRateGet");
            
    
            var path = "/historic_rate";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (from != null) queryParams.Add("from", ApiClient.ParameterToString(from)); // query parameter
 if (to != null) queryParams.Add("to", ApiClient.ParameterToString(to)); // query parameter
 if (amount != null) queryParams.Add("amount", ApiClient.ParameterToString(amount)); // query parameter
 if (date != null) queryParams.Add("date", ApiClient.ParameterToString(date)); // query parameter
 if (time != null) queryParams.Add("time", ApiClient.ParameterToString(time)); // query parameter
 if (obsolete != null) queryParams.Add("obsolete", ApiClient.ParameterToString(obsolete)); // query parameter
 if (inverse != null) queryParams.Add("inverse", ApiClient.ParameterToString(inverse)); // query parameter
                                        
            // authentication setting, if any
            String[] authSettings = new String[] { "basicAuth" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling HistoricRateGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling HistoricRateGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (HistoricRateResponse) ApiClient.Deserialize(response.Content, typeof(HistoricRateResponse), response.Headers);
        }
    
        /// <summary>
        /// Get historic rates for one base currency against one or more counter currencies over a period of time. This endpoint returns a daily historic rate for a single base currency and one or more counter currencies over a period of time.  &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/historic_rate/period.csv/?from&#x3D;USD&amp;to&#x3D;CAD&amp;start_timestamp&#x3D;2017-09-01&amp;end_timestamp&#x3D;2017-11-30&amp;per_page&#x3D;500\&quot;&gt;https://xecdapi.xe.com/v1/historic_rate/period.csv/?from&#x3D;USD&amp;to&#x3D;CAD&amp;start_timestamp&#x3D;2017-09-01&amp;end_timestamp&#x3D;2017-11-30&amp;per_page&#x3D;500&lt;/a&gt;  Optional parameters available with Live accounts:  &amp;interval&#x3D;minutely &amp;interval&#x3D;hourly  
        /// </summary>
        /// <param name="to">Comma separated list of to currencies based on ISO 4217 codes. This will limit the data returned to only those currencies you are interested in.  Note: Obsolete currencies are replaced by their precursor or successor currency.  </param> 
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code.  Note if this parameter is omitted, USD is assumed. </param> 
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1.00 is assumed. </param> 
        /// <param name="startTimestamp">OPTIONAL - ISO 8601 timestamp in the format yyyy-mm-ddThh:mm giving the UTC date and time of the start of the period for which you would like rates returned.  If your account is registered for a Daily package your endpoint will return rates at your preferred daily lock-in time starting on the date specified in your request.  If your account does not have a preferred daily lock-in time then rates will return as of 00:00 UTC If your account is registered for a Live package your endpoint will return rates starting at 00:00 UTC if no time portion is specified. </param> 
        /// <param name="endTimestamp">OPTIONAL – ISO 8601 timestamp in the format yyyy-mm-ddThh:mm giving the UTC date and time of the end of the period for which you would like rates returned. If a time in the future is specified, the current time will be used. If no end_time is specified, the time specified in the  “start_timestamp” paramenter will also be used for the end_timestamp.” If your account is registered for a Daily package your endpoint will return rates at your preferred daily lock-in time ending on the date specified in your request.  If your account does not have a preferred daily lock-in time then rates will return as of 00:00 UTC. If your account is registered for a Live package your endpoint will return rates at 00:00 UTC unless you specify a time parameter in your rate request. </param> 
        /// <param name="interval">OPTIONAL – Interval is applicable to Live packages only.  Using one of the interval values below in your call request will return rates for that specific interval within the time period specified.  Example: adding the interval of \&quot;hourly\&quot; will return rates for every hour in the time period you specified.    \&quot;daily\&quot; - Returns one rate for the days specified in your time period,  \&quot;hourly\&quot; - Returns rates for every hour in the time period you specify  If omitted, \&quot;daily\&quot; is used.  This parameter is only used if both the \&quot;start_timestamp\&quot; and \&quot;end_timestamp\&quot; parameters have been specified</param> 
        /// <param name="page">OPTIONAL – You can specify the page number you want to request. Note: that page numbering is 1-based (the first page being page 1). Omitting this parameter will return the first page. </param> 
        /// <param name="perPage">OPTIONAL – You can specify the number of results per page. The default is 30 results per page with a maximum of 500 results per page. </param> 
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param> 
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will display the inverse of the converted value. If ‘false‘ then it will not be displayed.</param> 
        /// <returns>HistoricRatePeriodResponse</returns>            
        public HistoricRatePeriodResponse HistoricRatePeriodGet (string to, string from, decimal? amount, string startTimestamp, string endTimestamp, string interval, decimal? page, decimal? perPage, bool? obsolete, bool? inverse)
        {
            
            // verify the required parameter 'to' is set
            if (to == null) throw new ApiException(400, "Missing required parameter 'to' when calling HistoricRatePeriodGet");
            
    
            var path = "/historic_rate/period";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (from != null) queryParams.Add("from", ApiClient.ParameterToString(from)); // query parameter
 if (to != null) queryParams.Add("to", ApiClient.ParameterToString(to)); // query parameter
 if (amount != null) queryParams.Add("amount", ApiClient.ParameterToString(amount)); // query parameter
 if (startTimestamp != null) queryParams.Add("start_timestamp", ApiClient.ParameterToString(startTimestamp)); // query parameter
 if (endTimestamp != null) queryParams.Add("end_timestamp", ApiClient.ParameterToString(endTimestamp)); // query parameter
 if (interval != null) queryParams.Add("interval", ApiClient.ParameterToString(interval)); // query parameter
 if (page != null) queryParams.Add("page", ApiClient.ParameterToString(page)); // query parameter
 if (perPage != null) queryParams.Add("per_page", ApiClient.ParameterToString(perPage)); // query parameter
 if (obsolete != null) queryParams.Add("obsolete", ApiClient.ParameterToString(obsolete)); // query parameter
 if (inverse != null) queryParams.Add("inverse", ApiClient.ParameterToString(inverse)); // query parameter
                                        
            // authentication setting, if any
            String[] authSettings = new String[] { "basicAuth" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling HistoricRatePeriodGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling HistoricRatePeriodGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (HistoricRatePeriodResponse) ApiClient.Deserialize(response.Content, typeof(HistoricRatePeriodResponse), response.Headers);
        }
    
    }
}
