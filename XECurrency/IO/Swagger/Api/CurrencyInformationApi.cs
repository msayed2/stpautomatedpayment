using System;
using System.Collections.Generic;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface ICurrencyInformationApi
    {
        /// <summary>
        /// Access a list of all available currencies. This endpoint will return a list of all currencies, active and obsolete, available via the XE Currency Data Feed API.  If the obsolete optional parameter is included, then the list will contain both active and obsolete currencies.
        /// </summary>
        /// <param name="iso">OPTIONAL – Comma separated list of ISO 4217 codes. This will limit the data returned to only those currencies that are specified. If this parameter is omitted, this endpoint will return results for all currencies.    It is a prefix match; you can provide it with one, two, or three characters and it will return a list of all the currencies with ISO 4217 codes that match.    A list of acceptable ISO 4217 currency codes can be found here:http://www.xe.com/iso4217.php</param>
        /// <param name="obsolete">OPTIONAL – If &#39;true&#39; then endpoint will display currencies that are obsolete but for which historical data is available</param>
        /// <param name="language">OPTIONAL – Default is en. Specified as an RFC-1766-compliant language tag.  Languages available: ar, de, en, es, fr, it, ja, pt, sv, zh-CN, zh-HK.</param>
        /// <returns>CurrencyInfoResponse</returns>
        CurrencyInfoResponse CurrenciesGet (string iso, bool? obsolete, string language);
    }
  
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class CurrencyInformationApi : ICurrencyInformationApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrencyInformationApi"/> class.
        /// </summary>
        /// <param name="apiClient"> an instance of ApiClient (optional)</param>
        /// <returns></returns>
        public CurrencyInformationApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = Configuration.DefaultApiClient; 
            else
                this.ApiClient = apiClient;
        }
    
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrencyInformationApi"/> class.
        /// </summary>
        /// <returns></returns>
        public CurrencyInformationApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
    
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            this.ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return this.ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}
    
        /// <summary>
        /// Access a list of all available currencies. This endpoint will return a list of all currencies, active and obsolete, available via the XE Currency Data Feed API.  If the obsolete optional parameter is included, then the list will contain both active and obsolete currencies.
        /// </summary>
        /// <param name="iso">OPTIONAL – Comma separated list of ISO 4217 codes. This will limit the data returned to only those currencies that are specified. If this parameter is omitted, this endpoint will return results for all currencies.    It is a prefix match; you can provide it with one, two, or three characters and it will return a list of all the currencies with ISO 4217 codes that match.    A list of acceptable ISO 4217 currency codes can be found here:http://www.xe.com/iso4217.php</param> 
        /// <param name="obsolete">OPTIONAL – If &#39;true&#39; then endpoint will display currencies that are obsolete but for which historical data is available</param> 
        /// <param name="language">OPTIONAL – Default is en. Specified as an RFC-1766-compliant language tag.  Languages available: ar, de, en, es, fr, it, ja, pt, sv, zh-CN, zh-HK.</param> 
        /// <returns>CurrencyInfoResponse</returns>            
        public CurrencyInfoResponse CurrenciesGet (string iso, bool? obsolete, string language)
        {
            
    
            var path = "/currencies";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (iso != null) queryParams.Add("iso", ApiClient.ParameterToString(iso)); // query parameter
 if (obsolete != null) queryParams.Add("obsolete", ApiClient.ParameterToString(obsolete)); // query parameter
 if (language != null) queryParams.Add("language", ApiClient.ParameterToString(language)); // query parameter
                                        
            // authentication setting, if any
            String[] authSettings = new String[] { "basicAuth" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling CurrenciesGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling CurrenciesGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (CurrencyInfoResponse) ApiClient.Deserialize(response.Content, typeof(CurrencyInfoResponse), response.Headers);
        }
    
    }
}
