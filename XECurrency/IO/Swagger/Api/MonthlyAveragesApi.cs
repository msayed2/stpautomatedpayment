using System;
using System.Collections.Generic;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IMonthlyAveragesApi
    {
        /// <summary>
        /// Get monthly average rates for a single base currency and one or more counter currency for the month/year you specify in your API query.  The monthly average endpoint returns monthly average rates for a single base currency and one or more counter currency for the year you specify in your API query. The monthly average rate is calculated by taking the 00:00 UTC Daily rate for each day in the month/year you specify in your query. Months are returned as a numeric value from 1 to 12 where 1 is for January and 12 is for December. If no month is provided, then all months for the given year are returned. 
        /// </summary>
        /// <param name="to">Comma separated list of to currencies based on ISO 4217 codes. This will limit the data returned to only those currencies that are specified.  </param>
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code.  Note if this parameter is omitted, USD is assumed. </param>
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1 is assumed. </param>
        /// <param name="year">OPTIONAL – This parameter specifies the year to calculate average monthly rates. </param>
        /// <param name="month">OPTIONAL – This parameter specifies the month in the given year to return average monthly rates. This is a numeric value from 1 to 12 where 1 is for January and 12 is for December. If no month is provided, then all months for the given year are returned. </param>
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param>
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will include inverse rates.</param>
        /// <returns>MonthlyAverageResponse</returns>
        MonthlyAverageResponse MonthlyAverageGet (string to, string from, decimal? amount, decimal? year, decimal? month, bool? obsolete, bool? inverse);
    }
  
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class MonthlyAveragesApi : IMonthlyAveragesApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MonthlyAveragesApi"/> class.
        /// </summary>
        /// <param name="apiClient"> an instance of ApiClient (optional)</param>
        /// <returns></returns>
        public MonthlyAveragesApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = Configuration.DefaultApiClient; 
            else
                this.ApiClient = apiClient;
        }
    
        /// <summary>
        /// Initializes a new instance of the <see cref="MonthlyAveragesApi"/> class.
        /// </summary>
        /// <returns></returns>
        public MonthlyAveragesApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
    
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            this.ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return this.ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}
    
        /// <summary>
        /// Get monthly average rates for a single base currency and one or more counter currency for the month/year you specify in your API query.  The monthly average endpoint returns monthly average rates for a single base currency and one or more counter currency for the year you specify in your API query. The monthly average rate is calculated by taking the 00:00 UTC Daily rate for each day in the month/year you specify in your query. Months are returned as a numeric value from 1 to 12 where 1 is for January and 12 is for December. If no month is provided, then all months for the given year are returned. 
        /// </summary>
        /// <param name="to">Comma separated list of to currencies based on ISO 4217 codes. This will limit the data returned to only those currencies that are specified.  </param> 
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code.  Note if this parameter is omitted, USD is assumed. </param> 
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1 is assumed. </param> 
        /// <param name="year">OPTIONAL – This parameter specifies the year to calculate average monthly rates. </param> 
        /// <param name="month">OPTIONAL – This parameter specifies the month in the given year to return average monthly rates. This is a numeric value from 1 to 12 where 1 is for January and 12 is for December. If no month is provided, then all months for the given year are returned. </param> 
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param> 
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will include inverse rates.</param> 
        /// <returns>MonthlyAverageResponse</returns>            
        public MonthlyAverageResponse MonthlyAverageGet (string to, string from, decimal? amount, decimal? year, decimal? month, bool? obsolete, bool? inverse)
        {
            
            // verify the required parameter 'to' is set
            if (to == null) throw new ApiException(400, "Missing required parameter 'to' when calling MonthlyAverageGet");
            
    
            var path = "/monthly_average";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (from != null) queryParams.Add("from", ApiClient.ParameterToString(from)); // query parameter
 if (to != null) queryParams.Add("to", ApiClient.ParameterToString(to)); // query parameter
 if (amount != null) queryParams.Add("amount", ApiClient.ParameterToString(amount)); // query parameter
 if (year != null) queryParams.Add("year", ApiClient.ParameterToString(year)); // query parameter
 if (month != null) queryParams.Add("month", ApiClient.ParameterToString(month)); // query parameter
 if (obsolete != null) queryParams.Add("obsolete", ApiClient.ParameterToString(obsolete)); // query parameter
 if (inverse != null) queryParams.Add("inverse", ApiClient.ParameterToString(inverse)); // query parameter
                                        
            // authentication setting, if any
            String[] authSettings = new String[] { "basicAuth" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling MonthlyAverageGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling MonthlyAverageGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (MonthlyAverageResponse) ApiClient.Deserialize(response.Content, typeof(MonthlyAverageResponse), response.Headers);
        }
    
    }
}
