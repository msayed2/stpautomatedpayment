using System;
using System.Collections.Generic;
using RestSharp;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace IO.Swagger.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IConversionsApi
    {
        /// <summary>
        /// Convert from one base currency to one or multiple counter currencies. In the example below, the Convert FROM API query endpoint is to convert from GBP to all available currencies (the asterisk * represents ALL currencies). &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/convert_from.csv/?from&#x3D;GBP&amp;to&#x3D;*&amp;amount&#x3D;1\&quot;&gt;https://xecdapi.xe.com/v1/convert_from.csv/?from&#x3D;GBP&amp;to&#x3D;*&amp;amount&#x3D;1&lt;/a&gt;
        /// </summary>
        /// <param name="to">Comma separated list of to currencies ISO 4217 codes.  This will limit the data returned to only those currencies you are interested in. Use an asterisk * to convert all currencies.  Note: Obsolete currencies are replaced by their successor currency.  </param>
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code.  Note if this parameter is omitted, USD is assumed. </param>
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1 is assumed. </param>
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param>
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will include inverse rates. An inverse rate is a quote for which the base currency and counter currency are switched. An inverse is calculated by dividing one by the exchange rate. Example: If the exchange rate for $1 USD to EUR &#x3D; 0.874852, then the inverse rate would be 1/0.874852 &#x3D; 1.14305, meaning that US$1.14305 would buy 1 euro.</param>
        /// <returns>ConvertFromResponse</returns>
        ConvertFromResponse ConvertFromGet (string to, string from, double? amount, bool? obsolete, bool? inverse,double? decimalPlaces);
        /// <summary>
        /// Convert to a currency amount from one or multiple other currencies. In the example below, the Convert TO API query endpoint is to convert to GBP from all available currencies (the asterisk * represents ALL currencies). &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/convert_to.csv/?to&#x3D;GBP&amp;from&#x3D;*&amp;amount&#x3D;1\&quot;&gt;https://xecdapi.xe.com/v1/convert_to.csv/?to&#x3D;GBP&amp;from&#x3D;*&amp;amount&#x3D;1&lt;/a&gt;
        /// </summary>
        /// <param name="from">Comma separated list of to currencies ISO codes.  This will limit the data returned to only those currencies you are interested in. Use an asterisk * to convert all currencies.  Note: Obsolete currencies are replaced by their successor currency.  </param>
        /// <param name="to">OPTIONAL - Currency you want to convert to ISO code.  Note if this parameter is omitted, USD is assumed. </param>
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1 is assumed. </param>
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param>
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will display the inverse of the converted value. If ‘false‘ then it will not be displayed.</param>
        /// <returns>ConvertToResponse</returns>
        ConvertToResponse ConvertToGet (string from, string to, decimal? amount, bool? obsolete, bool? inverse);
    }
  
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class ConversionsApi : IConversionsApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConversionsApi"/> class.
        /// </summary>
        /// <param name="apiClient"> an instance of ApiClient (optional)</param>
        /// <returns></returns>
        public ConversionsApi(ApiClient apiClient = null)
        {
            if (apiClient == null) // use the default one in Configuration
                this.ApiClient = Configuration.DefaultApiClient; 
            else
                this.ApiClient = apiClient;
        }
    
        /// <summary>
        /// Initializes a new instance of the <see cref="ConversionsApi"/> class.
        /// </summary>
        /// <returns></returns>
        public ConversionsApi(String basePath)
        {
            this.ApiClient = new ApiClient(basePath);
        }
    
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            this.ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return this.ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}
    
        /// <summary>
        /// Convert from one base currency to one or multiple counter currencies. In the example below, the Convert FROM API query endpoint is to convert from GBP to all available currencies (the asterisk * represents ALL currencies). &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/convert_from.csv/?from&#x3D;GBP&amp;to&#x3D;*&amp;amount&#x3D;1\&quot;&gt;https://xecdapi.xe.com/v1/convert_from.csv/?from&#x3D;GBP&amp;to&#x3D;*&amp;amount&#x3D;1&lt;/a&gt;
        /// </summary>
        /// <param name="to">Comma separated list of to currencies ISO 4217 codes.  This will limit the data returned to only those currencies you are interested in. Use an asterisk * to convert all currencies.  Note: Obsolete currencies are replaced by their successor currency.  </param> 
        /// <param name="from">OPTIONAL – Currency you want to convert from ISO code.  Note if this parameter is omitted, USD is assumed. </param> 
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1 is assumed. </param> 
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param> 
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will include inverse rates. An inverse rate is a quote for which the base currency and counter currency are switched. An inverse is calculated by dividing one by the exchange rate. Example: If the exchange rate for $1 USD to EUR &#x3D; 0.874852, then the inverse rate would be 1/0.874852 &#x3D; 1.14305, meaning that US$1.14305 would buy 1 euro.</param> 
        /// <returns>ConvertFromResponse</returns>            
        public ConvertFromResponse ConvertFromGet (string to, string from, double? amount, bool? obsolete, bool? inverse,double? decimalPlaces)
        {
            
            // verify the required parameter 'to' is set
            if (to == null) throw new ApiException(400, "Missing required parameter 'to' when calling ConvertFromGet");
            
    
            var path = "/convert_from";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (from != null) queryParams.Add("from", ApiClient.ParameterToString(from)); // query parameter
 if (to != null) queryParams.Add("to", ApiClient.ParameterToString(to)); // query parameter
 if (amount != null) queryParams.Add("amount", ApiClient.ParameterToString(amount)); // query parameter
 if (obsolete != null) queryParams.Add("obsolete", ApiClient.ParameterToString(obsolete)); // query parameter
 if (inverse != null) queryParams.Add("inverse", ApiClient.ParameterToString(inverse)); // query parameter
if (decimalPlaces != null) queryParams.Add("decimal_places", ApiClient.ParameterToString(decimalPlaces)); // query parameter

            // authentication setting, if any
            String[] authSettings = new String[] { "basicAuth" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ConvertFromGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ConvertFromGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (ConvertFromResponse) ApiClient.Deserialize(response.Content, typeof(ConvertFromResponse), response.Headers);
        }
    
        /// <summary>
        /// Convert to a currency amount from one or multiple other currencies. In the example below, the Convert TO API query endpoint is to convert to GBP from all available currencies (the asterisk * represents ALL currencies). &lt;a href&#x3D;\&quot;https://xecdapi.xe.com/v1/convert_to.csv/?to&#x3D;GBP&amp;from&#x3D;*&amp;amount&#x3D;1\&quot;&gt;https://xecdapi.xe.com/v1/convert_to.csv/?to&#x3D;GBP&amp;from&#x3D;*&amp;amount&#x3D;1&lt;/a&gt;
        /// </summary>
        /// <param name="from">Comma separated list of to currencies ISO codes.  This will limit the data returned to only those currencies you are interested in. Use an asterisk * to convert all currencies.  Note: Obsolete currencies are replaced by their successor currency.  </param> 
        /// <param name="to">OPTIONAL - Currency you want to convert to ISO code.  Note if this parameter is omitted, USD is assumed. </param> 
        /// <param name="amount">OPTIONAL – This parameter can be used to specify the amount you want to convert, if an amount is not specified then 1 is assumed. </param> 
        /// <param name="obsolete">OPTIONAL – If ‘true’ then endpoint will display rates for currencies that are obsolete. If ‘false’ then obsolete currencies are replaced by their successor currency.</param> 
        /// <param name="inverse">OPTIONAL – If ‘true’ then endpoint will display the inverse of the converted value. If ‘false‘ then it will not be displayed.</param> 
        /// <returns>ConvertToResponse</returns>            
        public ConvertToResponse ConvertToGet (string from, string to, decimal? amount, bool? obsolete, bool? inverse)
        {
            
            // verify the required parameter 'from' is set
            if (from == null) throw new ApiException(400, "Missing required parameter 'from' when calling ConvertToGet");
            
    
            var path = "/convert_to";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
             if (from != null) queryParams.Add("from", ApiClient.ParameterToString(from)); // query parameter
 if (to != null) queryParams.Add("to", ApiClient.ParameterToString(to)); // query parameter
 if (amount != null) queryParams.Add("amount", ApiClient.ParameterToString(amount)); // query parameter
 if (obsolete != null) queryParams.Add("obsolete", ApiClient.ParameterToString(obsolete)); // query parameter
 if (inverse != null) queryParams.Add("inverse", ApiClient.ParameterToString(inverse)); // query parameter
                                        
            // authentication setting, if any
            String[] authSettings = new String[] { "basicAuth" };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.GET, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling ConvertToGet: " + response.Content, response.Content);
            else if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling ConvertToGet: " + response.ErrorMessage, response.ErrorMessage);
    
            return (ConvertToResponse) ApiClient.Deserialize(response.Content, typeof(ConvertToResponse), response.Headers);
        }
    
    }
}
