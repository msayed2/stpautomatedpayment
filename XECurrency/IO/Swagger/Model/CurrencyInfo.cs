using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class CurrencyInfo {
    /// <summary>
    /// Gets or Sets Iso
    /// </summary>
    [DataMember(Name="iso", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "iso")]
    public string Iso { get; set; }

    /// <summary>
    /// Gets or Sets CurrencyName
    /// </summary>
    [DataMember(Name="currency_name", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency_name")]
    public string CurrencyName { get; set; }

    /// <summary>
    /// Gets or Sets IsObsolete
    /// </summary>
    [DataMember(Name="is_obsolete", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "is_obsolete")]
    public bool? IsObsolete { get; set; }

    /// <summary>
    /// Gets or Sets SupersededBy
    /// </summary>
    [DataMember(Name="superseded_by", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "superseded_by")]
    public string SupersededBy { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class CurrencyInfo {\n");
      sb.Append("  Iso: ").Append(Iso).Append("\n");
      sb.Append("  CurrencyName: ").Append(CurrencyName).Append("\n");
      sb.Append("  IsObsolete: ").Append(IsObsolete).Append("\n");
      sb.Append("  SupersededBy: ").Append(SupersededBy).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
