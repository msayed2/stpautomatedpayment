using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class AccountInfoResponse {
    /// <summary>
    /// Gets or Sets Id
    /// </summary>
    [DataMember(Name="id", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "id")]
    public string Id { get; set; }

    /// <summary>
    /// Gets or Sets Organization
    /// </summary>
    [DataMember(Name="organization", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "organization")]
    public string Organization { get; set; }

    /// <summary>
    /// Gets or Sets Package
    /// </summary>
    [DataMember(Name="package", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "package")]
    public string Package { get; set; }

    /// <summary>
    /// Gets or Sets ServiceStartTimestamp
    /// </summary>
    [DataMember(Name="service_start_timestamp", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "service_start_timestamp")]
    public string ServiceStartTimestamp { get; set; }

    /// <summary>
    /// Gets or Sets PackageLimitDuration
    /// </summary>
    [DataMember(Name="package_limit_duration", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "package_limit_duration")]
    public string PackageLimitDuration { get; set; }

    /// <summary>
    /// Gets or Sets PackageLimit
    /// </summary>
    [DataMember(Name="package_limit", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "package_limit")]
    public decimal? PackageLimit { get; set; }

    /// <summary>
    /// Gets or Sets PackageLimitRemaining
    /// </summary>
    [DataMember(Name="package_limit_remaining", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "package_limit_remaining")]
    public decimal? PackageLimitRemaining { get; set; }

    /// <summary>
    /// Gets or Sets PackageLimitReset
    /// </summary>
    [DataMember(Name="package_limit_reset", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "package_limit_reset")]
    public string PackageLimitReset { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccountInfoResponse {\n");
      sb.Append("  Id: ").Append(Id).Append("\n");
      sb.Append("  Organization: ").Append(Organization).Append("\n");
      sb.Append("  Package: ").Append(Package).Append("\n");
      sb.Append("  ServiceStartTimestamp: ").Append(ServiceStartTimestamp).Append("\n");
      sb.Append("  PackageLimitDuration: ").Append(PackageLimitDuration).Append("\n");
      sb.Append("  PackageLimit: ").Append(PackageLimit).Append("\n");
      sb.Append("  PackageLimitRemaining: ").Append(PackageLimitRemaining).Append("\n");
      sb.Append("  PackageLimitReset: ").Append(PackageLimitReset).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
