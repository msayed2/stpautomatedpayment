using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Rate {
    /// <summary>
    /// Gets or Sets Quotecurrency
    /// </summary>
    [DataMember(Name="quotecurrency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "quotecurrency")]
    public string Quotecurrency { get; set; }

    /// <summary>
    /// Gets or Sets Mid
    /// </summary>
    [DataMember(Name="mid", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "mid")]
    public decimal? Mid { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Rate {\n");
      sb.Append("  Quotecurrency: ").Append(Quotecurrency).Append("\n");
      sb.Append("  Mid: ").Append(Mid).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
