using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class TOCurrencyISO {
    /// <summary>
    /// Gets or Sets MonthlyAverage
    /// </summary>
    [DataMember(Name="monthlyAverage", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "monthlyAverage")]
    public decimal? MonthlyAverage { get; set; }

    /// <summary>
    /// Gets or Sets Month
    /// </summary>
    [DataMember(Name="month", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "month")]
    public decimal? Month { get; set; }

    /// <summary>
    /// Gets or Sets DaysInMonth
    /// </summary>
    [DataMember(Name="daysInMonth", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "daysInMonth")]
    public decimal? DaysInMonth { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class TOCurrencyISO {\n");
      sb.Append("  MonthlyAverage: ").Append(MonthlyAverage).Append("\n");
      sb.Append("  Month: ").Append(Month).Append("\n");
      sb.Append("  DaysInMonth: ").Append(DaysInMonth).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
